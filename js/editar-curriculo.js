function atualizaCursos(){
	$.get( "/curriculo/pega_cursos_by_user", function( data ) {
	  $('#lista_cursos').html(data);
	});
}

function carregaIdiomas(){
	$.post("/curriculo/cata_idiomas_select",{},function(data, status){
    	$('#select_idiomas').html(data);
    });
}

function atualizaExp(){
	$.post("/curriculo/retorna_exp_by_user",{},function(data, status){
    	$('#lista_experiencias').html(data);
    });
}

function atualizaServicos(){
	$.post("/curriculo/retorna_servicos_by_user",{},function(data, status){
    	$('#lista_servicos').html(data);
		$('.editable-autonomo').editable({
			url: "/curriculo/editable_autonomo",
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        });

		$('.editable-autonomo-data').editable({
			tpl: "<input type='text' id='telefone-autonomo' />",
			url: "/curriculo/editable_autonomo",
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
			$("input#telefone-autonomo").mask("99/99/9999");
	  	});
    });
}

function atualizaIdiomas(){
	$.post("/curriculo/cata_idiomas_by_user",{},function(data, status){
    	$('#lista_idiomas').html(data);
    });
}

function atualizaAreas(){
	$.post("/curriculo/cata_areas_by_user",{},function(data, status){
    	$('#lista_areas').html(data);
    });
}

atualizaAreas();

function deletaArea(area){
	if(confirm("Tem certeza?")){
		$.post("/curriculo/deletaArea",{
			area : area
		},function(data, status){
	    	atualizaAreas();
	    });
	}
}

function deletaIdioma(idioma){
	if(confirm("Tem certeza?")){
		$.post("/curriculo/deletaIdioma",{
			idioma : idioma
		},function(data, status){
	    	$('#lista_idiomas').html(data);
	    	atualizaIdiomas();
	    	carregaIdiomas();
	    });
	}
}
setInterval(atualizaExp, 5000);
setInterval(atualizaIdiomas, 5000);

function deletaCurso(id){
	if(confirm("Tem certeza?")){
		$.get( "/curriculo/deletaCurso/" + id, function(data, status){
			atualizaCursos();
		});
	}
}

function deletaExp(id){
	if(confirm("Tem certeza?")){
		$.post("/curriculo/deletaExp",{
			id : id
		},function(data, status){
	    	atualizaExp();
	    });
	}
}

function deletaServico(id){
	if(confirm("Tem certeza?")){
		$.post("/curriculo/deletaServico",{
			id : id
		},function(data, status){
	    	atualizaServicos();
	    });
	}
}
$(document).ready(function(){
	$("#inicio").mask("99/99/9999");

    $('#botao_cadastrar_servico').click(function(){
    	titulo_servico = $("#titulo_servico").val();
    	descricao_servico = $("#descricao_servico").val();
    	lugares = $("#lugares").val();
    	contato = $("#contato").val();
    	inicio = $("#inicio").val(); //guardando elementos na memória para performance...
    	if(titulo_servico != null && $.trim(titulo_servico) != "" && descricao_servico != null && $.trim(descricao_servico) != ""){
	    	$.post("/curriculo/cadastra_servico",{
		        titulo_servico : titulo_servico,
		        descricao_servico : descricao_servico,			            			        
		        lugares : lugares,			            			        
		        contato : contato,
		        inicio : inicio,
		    },
		    function(data, status){
		    	$("#modal_autonomo").modal('hide');
		    	$("#titulo_servico").val('');
		    	$("#descricao_servico").val('');
		    	$("#lugares").val('');
		    	$("#contato").val('');
		    	$("#inicio").val('');
		    	atualizaServicos();
		    });

		}else{
			alert ("Verifique os campos!");
		}
    });

	atualizaExp();
	atualizaIdiomas();
	carregaIdiomas();

	$.get( "/curriculo/pega_cursos_for_user", function( data ) {
	  $('#id_curso').html(data);
	});
	atualizaCursos();
	setInterval(atualizaCursos, 5000);
	$('#adicionarCurso').click(function(){
		curso = document.getElementById('id_curso').value;
		onde = document.getElementById('instituicao').value;
		if(curso != null && curso != "" && onde != null && onde != ""){
			curso_concluido = document.getElementById('curso_concluido').checked;
			instituicao = document.getElementById('instituicao').value;
			if(curso_concluido == true){
				concluido = 1;
			}else{
				concluido = 0;
			}

			$.post("/curriculo/cadastraCurso",{
				curso : curso,
				concluido : concluido,
				instituicao : instituicao
			},function(data, status){
		    	$('#instituicao').val("");
			  	$('#result_cursos').html(data);
		    });


			$.get( "/curriculo/pega_cursos_for_user", function( data ) {
			  $('#id_curso').html(data);
			});

			$('#modal_add_cursos').modal('hide');
		}else{
			alert ("Informe todos os dados!");
		}
		atualizaCursos();

	});

	$('#botao_cadastrar_idioma').click(function(){
    	idioma = $('#select_idiomas').val();


    	if(idioma != null && idioma != ""){
	    	$.post("/curriculo/cadastra_idioma_usuario",{
	    		idioma : idioma
	    	},
		    function(data, status){
		    	$('#modal_add_idiomas').modal('hide');
		    	carregaIdiomas();
		    	atualizaIdiomas();
		    });
		}else{
			alert ("Informe um idioma da lista!");
		}
    });

	$('#botao_cadastrar_area').click(function(){
    	area = $('#nome_nova_area').val();
    	if(area != null && area != ""){
	    	$.post("/curriculo/cadastra_nova_area_usuario",{
	    		area : area
	    	},
		    function(data, status){
		    	$('#modal_nova_area').modal('hide');
		    	atualizaAreas();
		    });
		}else{
			alert ("Informe uma área");
		}
    });

    $('#cadastrarArea').click(function(){
    	area = $('#id_area').val();
    	if(area != null && area != ""){
	    	$.post("/curriculo/cadastra_area_usuario",{
	    		area : area
	    	},
		    function(data, status){
		    	$('#modal_add_area').modal('hide');
		    	atualizaAreas();
		    });
		}else{
			alert ("Informe uma área da lista!");
		}
    });

    //Call .imgPicker()
    $('#avatarModal').imgPicker({
        url: '/js/imagepicker/server/upload_avatar.php',
        // swf: '/js/imagepicker/assets/webcam.swf',
        aspectRatio: 1,
        deleteComplete: function() {
            // Restore default avatar
            $('#avatar').attr('src', 'http://www.gravatar.com/avatar/0?d=mm&s=150');
            // Hide modal
            this.modal('hide');
        },
        cropSuccess: function(image) {
	        $.post("/curriculo/troca_imagem",{
				imagem: image.url,
			},function(data, status){
	    		console.log(data);
		    });
            // Set #avatar src
            $('#avatar').attr('src', image.versions.avatar.url);
            // Hide modal
            this.modal('hide');
        }
    });

    $('#botao_cadastrar_curso').click(function(){
    	curso = $('#nome_novo_curso').val();

    	curso_concluido = document.getElementById('curso_concluido_modal').checked;
		instituicao = document.getElementById('instituicao_modal').value;
		if(curso_concluido == true){
			concluido = 1;
		}else{
			concluido = 0;
		}

    	if(curso != null && curso != "" && instituicao != null && instituicao != ""){
	    	//$.get( "/curriculo/cadastra_novo_curso/" + curso + "/" + concluido + "/" + instituicao, function( data ) {});
	    	$.post("/curriculo/cadastra_novo_curso",{
		        nome_curso: curso,
		        concluido: concluido,
		        instituicao: instituicao
		    },
		    function(data, status){});
	    	$("#modal_curso").modal('hide');
	    	$("#nome_novo_curso").val('');
	    	$("#instituicao_modal").val('');
	    	atualizaCursos();
		}else{
			alert ("Informe os dados do curso!");
		}
    });
    
    $('#botao_cadastrar_exp').click(function(){
    	descricao_exp = $('#exp_descricao').val();
    	funcao_exp = $('#exp_funcao').val();
    	inicio_exp = $('#exp_inicio').val();
    	final_exp = $('#exp_final').val();
    	empresa = $('#empresa').val();
    	endereco = $('#exp_endereco').val();
    	telefone = $('#exp_telefone').val();
    	cidade = $('#exp_cidade').val();


    	if(descricao_exp != null && descricao_exp != "" && funcao_exp != null && funcao_exp != "" && inicio_exp != null && inicio_exp != "" && empresa != null && empresa != ""){
	    	$.post("/curriculo/cadastra_experiencia",{
		        descricao : descricao_exp,
		        funcao : funcao_exp,
		        inicio : inicio_exp,
		        final_ : final_exp,
		        empresa : empresa,
		        endereco : endereco,
		        telefone : telefone,
		        cidade : cidade
		    },
		    function(data, status){});
	    	$("#modal_experiencias").modal('hide');
	    	descricao_exp = $('#exp_descricao').val('');
	    	funcao_exp = $('#exp_funcao').val('');
	    	inicio_exp = $('#exp_inicio').val('');
	    	final_exp = $('#exp_final').val('');
	    	empresa = $('#empresa').val('');

		}else{
			alert ("Verifique os campos!");
		}
    });

	//X-Editable
	$.fn.editable.defaults.mode = 'popup';
	$.fn.editable.defaults.url = '/Curriculo/editable';
	$.fn.editableform.buttons  = '<button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="fa fa-check"></i></button>' +
	'<button type="button" class="btn btn-default btn-sm editable-cancel"><i class="fa fa-ban"></i></button>';

	$('.editable').editable({
        ajaxOptions: {
            dataType: 'json'
        },
        emptytext: 'Não informado',
        success: function(response, newValue) {
            if(!response) {
                return "Erro desconhecido";
            }

            if(response.success == false) {
                return response.msg;
            }
        }
    });

    

	// SELECT2 PARA CIDADES
  	$('#cidade').editable({
	        select2: {
	            theme: "bootstrap",
	            placeholder: 'Selecione a cidade',
	        minimumInputLength: 3,
	        minimumResultsForSearch: 0,
			language: {
				inputTooShort: function(args) {
				  // args.minimum is the minimum required length
				  // args.input is the user-typed text
				  return "Digite mais...";
				},
				inputTooLong: function(args) {
				  // args.maximum is the maximum allowed length
				  // args.input is the user-typed text
				  return "Excesso de caracteres";
				},
				errorLoading: function() {
				  return "Carregando cidades...";
				},
				loadingMore: function() {
				  return "Carregando mais cidades";
				},
				noResults: function() {
				  return "Nenhuma cidade encontrada";
				},
				searching: function() {
				  return "Procurando cidades...";
				},
				maximumSelected: function(args) {
				  // args.maximum is the maximum number of items the user may select
				  return "Erro ao carregar resultados";
				}
			},
	        ajax: {
	            url: "/pesquisa/lista_cidades",
	            dataType: "json",
	            type: "post",
	            data: function (params) {

	                var queryParameters = {
	                    term: params.term
	                }
	                return queryParameters;
	            },
	            processResults: function (data) {
	                return {
	                    results: $.map(data, function (item) {
	                        return {
	                            text: item.nome+" - "+item.estado,
	                            id: item.id
	                        }
	                    })
	                };
	            }
	        }
	        },
	        tpl: '<select style="width:150px;">',
	        type: 'select2',
	        success: function(response, newValue) {
                var editable = $(this).data('editable');
                var option = editable.input.$input.find('option[value="VALUE"]'.replace('VALUE',newValue));
                var newText = option.text();    
                $(this).attr('data-text', newText);
                $(this).attr('data-value', newValue);
                $(this).html(newText);
	        },
	        display: function(value, sourceData ) {
	        	return false;
	        }
	    });
	// FIM SELECT2 PARA CIDADES

	//SELECT2 CADASTRAR EXP
	$("#exp_cidade").select2({
		placeholder: "Selecione uma cidade",
		theme: "bootstrap",
        minimumInputLength: 3,
        minimumResultsForSearch: 0,
		language: {
			inputTooShort: function(args) {
			  // args.minimum is the minimum required length
			  // args.input is the user-typed text
			  return "Digite mais...";
			},
			inputTooLong: function(args) {
			  // args.maximum is the maximum allowed length
			  // args.input is the user-typed text
			  return "Excesso de caracteres";
			},
			errorLoading: function() {
			  return "Carregando cidades...";
			},
			loadingMore: function() {
			  return "Carregando mais cidades";
			},
			noResults: function() {
			  return "Nenhuma cidade encontrada";
			},
			searching: function() {
			  return "Procurando cidades...";
			},
			maximumSelected: function(args) {
			  // args.maximum is the maximum number of items the user may select
			  return "Erro ao carregar resultados";
			}
		},
        ajax: {
            url: "/pesquisa/lista_cidades",
            dataType: "json",
            type: "post",
            data: function (params) {

                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome+" - "+item.estado,
                            id: item.id
                        }
                    })
                };
            }
        }
	});
	//FIM SELECT2 CADASTRAR EXP


    $('.editable-cep').editable({
    	tpl:'   <input type="text" id ="cep" class="form-control input-sm" style="padding-right: 24px;">',
        ajaxOptions: {
            dataType: 'json'
        },
        emptytext: 'Não informado',
        success: function(response, newValue) {
            if(!response) {
                return "Erro desconhecido";
            }

            if(response.success == false) {
                return response.msg;
            }
        }
    }).on('shown',function(){
		$("input#cep").mask("99999-999");
  	});

	

	$.fn.bdatepicker.dates['de'] = {
		days: ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado", "Domingo"],
		daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
		daysMin: ["Do", "Se", "Ter", "Qa", "Qi", "Se", "Sá", "Do"],
		months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
		today: "Hoje",
		clear: "Limpar",
		weekStart: 1
	};

	$('#dt_nascimento').editable({
        format: 'yyyy-mm-dd',    
		viewformat: 'dd/mm/yyyy',
		mode: "popup",
		datepicker: {
		    language: 'de'
		  },
        validate: function(value) {
            if($.trim(value) == '') return 'Campo requerido!';
        },
        emptytext: 'Não informado',
        ajaxOptions: {
            dataType: 'json'
        },
        success: function(response, newValue) {
            if(!response) {
                return "Erro";
            }

             if(response.success === false) {
                return response.msg;
            }
        }
	});

	$('#campo_telefone').editable({
		tpl:'   <input type="text" id ="telefone" class="form-control input-sm" style="padding-right: 24px;">',
        ajaxOptions: {
            dataType: 'json'
        },
        emptytext: 'Não informado',
        success: function(response, newValue) {
            if(!response) {
                return "Erro";
            }

             if(response.success === false) {
                return response.msg;
            }
        }
	}).on('shown',function(){
		$("input#telefone").mask("(99) 9999-9999?9");
  	});


	$('#campo_celular').editable({
		tpl:'   <input type="text" id ="celular" class="form-control input-sm dd" style="padding-right: 24px;">',
        ajaxOptions: {
            dataType: 'json'
        },
        emptytext: 'Não informado',
        success: function(response, newValue) {
            if(!response) {
                return "Erro";
            }

             if(response.success === false) {
                return response.msg;
            }
        }
	}).on('shown',function(){
		$("input#celular").mask("(99) 9999-9999?9");
  	});

	
	atualizaServicos();
});