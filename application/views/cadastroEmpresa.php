<html>
	<head>
		<title>Currículo Online - Cadastre sua empresa agora mesmo!</title>
		<?php $this->load->view('inc/headBasico.php')?>
		<meta name='description' content="Registre-se sua empresa no Enviar Currículo, receba muitos currículos e entre em contato com pessoas que estão procurando emprego, tudo totalmente grátis!"/>
		<script src="/js/mascara.js"></script>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php')?>
			<?php $this->load->view('inc/menuSuperior.php')?>
		</div>
		
		<script>
		jQuery(function($){
   			$("#cnpj").mask("99.999.999/9999-99");
			});
		</script>
		
		<div class="panel panel-default col-md-8 col-sm-12 col-xs-12">
		<div class='panel-body'>
		
		<?php
			echo form_open('cadastroEmpresa');
			echo "<center><h2>Cadastrar-se</h2></center>";
			echo validation_errors('<div class="alert alert-danger">', '</div>');
			echo "<div class='form-group'>";
			echo form_label('Nome da empresa:');
			echo form_input(array('name'=>'nome', 'class'=>'form-control'), set_value('nome'), 'autofocus');
			echo "</div>";
			echo "<div class='form-group'>";
			echo form_label('CNPJ:');
			echo form_input(array('name'=>'cnpj', 'class'=>'form-control', 'id'=>'cnpj', 'placeholder'=>"Para fins de validação de falsidade."), set_value('cnpj'));
			echo "</div>";
			echo "<div class='form-group'>";
			echo form_label('E-mail:');
			echo form_input(array('name'=>'email', 'type'=>'email', 'class'=>'form-control'), set_value('email'));
			echo "</div>";
			echo "<div class='form-group'>";
			echo "<div class='form-group'>";
			echo form_label('Cidade:');
			?>
			<select id='cidade' class='form-control' name='cidade'>
				<option>Selecione</option>
			</select>
			</div>

			<?php
			echo "<div class='form-group'>";
			echo form_label('Nome de usuário:');
			echo form_input(array('name'=>'login', 'class'=>'form-control'), set_value('login'));
			echo "</div>";
			
			echo "<div class='form-group'>";			
			echo form_label('Senha:');
			echo form_input(array('name'=>'senha', 'class'=>'form-control', 'type'=>'password'), set_value('senha'));
			echo "</div>";

			echo "<div class='form-group'>";		
			echo form_label('Confirmar a senha:');
			echo form_input(array('name'=>'senha2', 'class'=>'form-control', 'type'=>'password'), set_value('senha2'));
			echo "</div>";
			
			echo "<div class='form-group'>";		
			echo form_submit(array('name'=>'cadastrar', 'class'=>'btn btn-primary pull-right'), 'Cadastrar');
			echo "</div>";
			
			echo form_close();
		?>
			</div>
		</div>
	</div>
		
		
		<?php $this->load->view('inc/anuncioDireita.php')?>
		<?php $this->load->view('inc/anuncioMeio.php')?>
		
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2_bootstrap.min.css">
		<script type="text/javascript" src='/js/select2/dist/js/select2.full.min.js'></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$("#cidade").select2({
				theme: "bootstrap",
		        minimumInputLength: 3,
		        minimumResultsForSearch: 0,
				language: {
					inputTooShort: function(args) {
					  // args.minimum is the minimum required length
					  // args.input is the user-typed text
					  return "Digite mais...";
					},
					inputTooLong: function(args) {
					  // args.maximum is the maximum allowed length
					  // args.input is the user-typed text
					  return "Excesso de caracteres";
					},
					errorLoading: function() {
					  return "Carregando cidades...";
					},
					loadingMore: function() {
					  return "Carregando mais cidades";
					},
					noResults: function() {
					  return "Nenhuma cidade encontrada";
					},
					searching: function() {
					  return "Procurando cidades...";
					},
					maximumSelected: function(args) {
					  // args.maximum is the maximum number of items the user may select
					  return "Erro ao carregar resultados";
					}
				},
		        ajax: {
		            url: "/pesquisa/lista_cidades",
		            dataType: "json",
		            type: "post",
		            data: function (params) {

		                var queryParameters = {
		                    term: params.term
		                }
		                return queryParameters;
		            },
		            processResults: function (data) {
		                return {
		                    results: $.map(data, function (item) {
		                        return {
		                            text: item.nome+" - "+item.estado,
		                            id: item.id
		                        }
		                    })
		                };
		            }
		        }
			});
		});
		</script>
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php')?>
		
	</body>
</html>