<html>
	<head>
		<title>Enviar Currículo - Confirmação</title>
		<?php include 'inc/headBasico.inc.php';?>
	</head>
	<body style="min-width: 540px; max-width: 95%; margin: 0 auto;">
		<div class="row topo">
			<?php include 'inc/topo.inc.php';?>
			<?php include 'inc/menuSuperior.inc.php';?>
		</div>
		
		<div class="panel panel-default col-md-8 col-sm-12 col-xs-12">
		<div class="panel-body">

		<?php
						
			$criarlogin		= $_GET['criarlogin'];

				function is_utf8( $string ){

				return preg_match( '%^(?:
					 [\x09\x0A\x0D\x20-\x7E]
					| [\xC2-\xDF][\x80-\xBF]
					| \xE0[\xA0-\xBF][\x80-\xBF]
					| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}
					| \xED[\x80-\x9F][\x80-\xBF]
					| \xF0[\x90-\xBF][\x80-\xBF]{2}
					| [\xF1-\xF3][\x80-\xBF]{3}
					| \xF4[\x80-\x8F][\x80-\xBF]{2}
					)*$%xs',
					$string
				);
			}

			function removeAccents( $string ){
				$utf8 = is_utf8( $string );

				if ( !$utf8 ) $string = utf8_encode( $string );

				$ret = preg_replace(
					array(
						//Maiúsculos
						'/\xc3[\x80-\x85]/',
						'/\xc3\x87/',
						'/\xc3[\x88-\x8b]/',
						'/\xc3[\x8c-\x8f]/',
						'/\xc3([\x92-\x96]|\x98)/',
						'/\xc3[\x99-\x9c]/',

						//Minúsculos
						'/\xc3[\xa0-\xa5]/',
						'/\xc3\xa7/',
						'/\xc3[\xa8-\xab]/',
						'/\xc3[\xac-\xaf]/',
						'/\xc3([\xb2-\xb6]|\xb8)/',
						'/\xc3[\xb9-\xbc]/',
					),
					str_split( 'ACEIOUaceiou' , 1 ),
					$string
				);

				return $ret;
			}


			if (!( preg_match( '/^[\w\n\s]+$/i' , removeAccents( $criarlogin ) ) )){
				echo "<div class='alert alert-danger'><h2>&Eacute; permitido apenas letras e n&uacute;meros no campo nome de usu&aacute;rio.</h2>
					<button class='btn btn-danger' onClick='JavaScript: window.history.back();>Voltar</button>
				</div>";
				}
			elseif ((strlen($criarlogin)) > 20 || (strlen($criarlogin)) < 6){
				echo "<div class='alert alert-danger'><h2>O nome de usuário deve conter de 6 a 20 caracteres</h2>
					<button class='btn btn-danger' onClick='JavaScript: window.history.back();>Voltar</button>
				</div>";
				}

			elseif ($criarlogin == "" || $criarlogin == " "){ 
				echo "<div class='alert alert-danger'><h2>O campo login de usu&aacute;rio precisa ser preenchido!</h2>
					<button class='btn btn-danger' onClick='JavaScript: window.history.back();>Voltar</button>
				</div>";
			}

			else{


				include "inc/conecta.php";
			
				$sql=mysqli_query($conexao, "UPDATE usuarios SET confirmado=1 WHERE login = '$criarlogin'");
							

				if (!$sql)
				{
					echo "Ocorreu algum problema. Por favor entre em contato conosco: <b>contato@icurriculumvitae.com.br</b>";
				}
				else
				{
					echo "<center><h2>Cadastro realizado com sucesso!</h2><center>";
				}
				
			}	
		?>

		</div>
		</div>

		<?php include 'inc/anuncioDireita.inc.php';?>
		<?php include 'inc/anuncioMeio.inc.php';?>
				
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		
		<div style='clear: both'></div>
		<?php include 'inc/anuncioBaixo.inc.php';?>
		
	</body>
</html>