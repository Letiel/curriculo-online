<html>
	<head>
		<title>Cadastrar Empresa - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
	</head>
	<body>
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Cadastrar Empresa</h2></center>
				<?php
					echo form_open_multipart('adm/cadastrar_empresa');
					echo validation_errors();
					echo $this->upload->display_errors();
					
					echo form_label('Nome:');
					echo form_input(array('name'=>'nome', 'class'=>'form-control'), set_value('nome'));

					echo form_label('CNPJ:');
					echo form_input(array('name'=>'cnpj', 'class'=>'form-control'), set_value('cnpj'));

					echo form_label('Website:');
					echo form_input(array('name'=>'site', 'class'=>'form-control'), set_value('site'));

					echo form_label('E-mail:');
					echo form_input(array('name'=>'email', 'class'=>'form-control'), set_value('email'));

					echo form_label('Endereço:');
					echo form_input(array('name'=>'endereco', 'class'=>'form-control'), set_value('endereco'));

					echo form_label('Bairro:');
					echo form_input(array('name'=>'bairro', 'class'=>'form-control'), set_value('bairro'));

					echo form_label('Estado:');
			        echo "<select name='estado' class='form-control' id='selectEstado'><option value=''>Selecione...</option>";
			        foreach($estados as $estado){
						if($estado->id == $empresa->idEstado)
								$selected = 'selected';
							else
								$selected = '';
						echo "<option $selected value='".$estado->id."'>".$estado->nome."</option>";
					}
			        echo "</select>";

					echo form_label('Cidade:');
			        echo "<select name='cidade' class='form-control' id='selectCidade'><option value=''>Selecione...</option>";
			        foreach($cidades as $cidade){
						if($cidade->id == $empresa->idCidade)
								$selected = 'selected';
							else
								$selected = '';
						echo "<option $selected value='".$cidade->id."'>".$cidade->nome."</option>";
					}
			        echo "</select>";

			        echo form_label('CEP:');
					echo form_input(array('name'=>'cep', 'class'=>'form-control', 'id'=>'cep'), set_value('cep'));

					echo form_label('Foto:');
					echo "<div class='input-group'>
			                <span class='input-group-btn'>
			                    <span class='btn btn-primary btn-file'>
			                        Enviar foto <input type='file' name='foto'>
			                    </span>
			                </span>
			                <input type='text' class='form-control' readonly>
			            </div>";

			        echo form_label('Texto de apresentação:');
			        echo "<textarea name='textempresa' class='form-control'></textarea>";


					echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary', 'value'=>'Atualizar'));

					echo form_close();
				?>
			</div>
			<script>
				$(document).on('change', '.btn-file :file', function() {
				  var input = $(this),
				      numFiles = input.get(0).files ? input.get(0).files.length : 1,
				      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				  input.trigger('fileselect', [numFiles, label]);
				});

				$(document).ready( function() {
				    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
				        
				        var input = $(this).parents('.input-group').find(':text'),
				            log = numFiles > 1 ? numFiles + ' files selected' : label;
				        
				        if( input.length ) {
				            input.val(log);
				        } else {
				            if( log ) alert(log);
				        }
				        
				    });
				});
			</script>
		</div>
	</body>
</html>