<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');?>
<html>
	<head>
		<title>Enviar Currículo - Recupendo a senha</title>
		<?php $this->load->view('inc/headBasico');?>

	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo');?>
			<?php $this->load->view('inc/menuSuperior');?>
		</div>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<form style='margin: 1%;' action='<?php echo base_url(); ?>Recuperar_senha' method="post">
			<?php echo validation_errors();
			echo $this->session->flashdata('confirmado');
			if(isset($erro) && $erro == TRUE && $codigo_erro == 1){
				echo "<div class='alert alert-danger'>O login ou e-mail não consta no sistema, verifique seus dados!</div>";
			}
			?>
			  <div class="form-group"><h3 align="center">Recuperar Senha</h3>
				<label for="inputLogin">E-mail da conta:</label>
				<input type="text" class="form-control" name='login' id="inputLogin" placeholder="E-mail">
			  </div>
			  <div class="form-group">
				<button type="submit" class="btn btn-primary">Recuperar Senha</button>
			  </div>
		  </form>
		  		
		</div>
		
		<?php $this->load->view('inc/anuncioDireita');?>
		<?php $this->load->view('inc/anuncioMeio');?>
		
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo');?>
	</body>
</html>