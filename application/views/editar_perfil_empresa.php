<?php $empresa = $empresa[0]; ?>
<html>
	<head>
		<title><?php echo $empresa->nome ?> - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="<?php echo "Veja os dados de $empresa->nome de $empresa->cidade, $empresa->uf. Cadastre-se, veja vagas e envie seu curriculo para empresas de sua região." ?>" />
		<meta property="og:image" content="<?php echo $empresa->imagem?>"/>
		<meta property="og:title" content="<?php echo $empresa->nome?>"/>
		<meta property="og:description" content="<?php echo $empresa->textempresa?>"/>

		<!-- JavaScript -->
		<script src="/js/imagepicker/assets/js/jquery.Jcrop.min.js"></script>
		<script src="/js/imagepicker/assets/js/jquery.imgpicker.js"></script>
		<link rel="stylesheet" href="/js/imagepicker/assets/css/imgpicker.css">
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		<?php $this->load->view('inc/anuncioDireita.php');?>

		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<script>
			function envia_curriculo(id){
				$("#resultado_curriculo").html("");
				$.post("<?php echo base_url(); ?>Empresa/envia_curriculo",{
					empresa : id,
				},function(data, status){
					$("#resultado_curriculo").html(data);
				});
			}
		</script>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12' style="padding: 20px 50px 15px 50px">
			<p class="text-info"><b>Dica!</b> Você pode editar seu perfil clicando sobre o item que deseja alterar!</p>
			<div id='curriculo'>
				<div id='resultado_curriculo'></div>
				<?php
					// if($empresa->recebe_email && !$this->session->userdata('logadoEmpresa')){
					// 	if($this->session->userdata('logado')){
					// 		echo "<button class='btn btn-primary bt-sm pull-right' onclick='envia_curriculo($empresa->id)'>Enviar Currículo</button>";
					// 	}else{
					// 		echo "<button id='makeLogin' class='btn btn-primary bt-sm pull-right' data-toggle='modal' data-toggle='tooltip' data-placement='left' data-target='.bs-example-modal-sm' title='É necessário logar no sistema.'>Enviar Currículo</button>";
					// 	}
					// }
				?>
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-5" style="margin: 10px 50px 10px 25px">
					<?php
					echo "<div id='avatar-container'><img class='img-responsive' id='avatar' src='".$empresa->imagem."' alt='$empresa->nome' /><button type='button' class='btn btn-default edit-avatar btn-block btn-sm' data-ip-modal='#avatarModal' title='Editar imagem'>Trocar</button></div>"; 
					echo "</div>";
					echo "<h1 class='azul'><a href='#' data-name='nome' class='editable'>".$empresa->nome."</a></h1><br/>";

					echo "<h4 class='azul'><strong>Website: </strong> <a href='#' data-name='site' class='editable' >".$empresa->site."</a><br/><br/></h4>";
				
					echo "<h3 class='quebra-separador'>Sobre a empresa: </h3><div class='separador'></div><h4 class='azul'><a href='#' class='editable' data-type='textarea' data-name='textempresa'>".nl2br($empresa->textempresa)."</a></h4>";
					//$this->load->view("inc/anuncioMeioResultados");
										
					echo "<h3 class='quebra-separador'>Localização: </h3><div class='separador'></div>";

					echo "<h4 class='azul'><strong>Endereço: </strong><a href='#' data-name='endereco' class='editable'>".$empresa->endereco."</a></h4>";


					echo "<h4 class='azul'><strong>Bairro: </strong><a href='#' data-name='bairro' class='editable'>".$empresa->bairro."</a></h4>";

					echo "<h4 class='azul'><strong>Complemento: </strong><a href='#' data-name='complemento' class='editable'>".$empresa->complemento."</a></h4>";
				
					echo "<h4 class='azul'><strong>Cidade: </strong><a href='#' data-name='cidade' class='editable-cidade' data-value='$empresa->idCidade' data-type='select2'>".$empresa->cidade.", ".$empresa->uf."</a></h4>";

					echo "<h4 class='azul'><strong>CEP: </strong><a href='#' class='editable-cep' data-name='cep'>".$empresa->cep."</a><br/><br/></h4>";
					
				?>

				<div style='margin: 25px 0; width: 100%; overflow: hidden;'></div>

				<!-- Modal CROP -->
				<div class="ip-modal" id="avatarModal">
				  <div class="ip-modal-dialog">
				    <div class="ip-modal-content">
				      <div class="ip-modal-header">
				        <a class="ip-close" title="Close">&times;</a>
				        <h4 class="ip-modal-title">Alterar imagem</h4>
				      </div>
				      <div class="ip-modal-body">
				        <div class="btn btn-primary ip-upload">
				            Carregar Foto <input type="file" name="file" class="ip-file">
				        </div>
				        <!-- <button type="button" class="btn btn-primary ip-webcam">Webcam</button> -->
				        <!-- <button type="button" class="btn btn-info ip-edit">Edit</button> -->
				        <!-- <button type="button" class="btn btn-danger ip-delete">Delete</button> -->
				        <div class="alert ip-alert"></div>
				        <div class="ip-info">
				        	Para recortar a imagem, arraste o mouse na região abaixo depois clique em "Salvar"
				        </div>
				          <div class="ip-preview"></div>
				        <div class="ip-rotate">
				          <button type="button" class="btn btn-default ip-rotate-ccw">
				            <i class="icon-ccw"></i>
				          </button>
				          <button type="button" class="btn btn-default ip-rotate-cw">
				            <i class="icon-cw"></i>
				          </button>
				        </div>
				        <div class="ip-progress">
				          <div class="text">Enviando</div>
				          <div class="progress progress-striped active">
				            <div class="progress-bar"></div>
				          </div>
				        </div>
				      </div>
				      <div class="ip-modal-footer">
				        <div class="ip-actions">
				          <button type="button" class="btn btn-success ip-save">Salvar</button>
				          <button type="button" class="btn btn-primary ip-capture">Capturar</button>
				          <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
				        </div>
				        <button type="button" class="btn btn-default ip-close">Fechar</button>
				      </div>
				    </div>
				  </div>
				</div>

				<script src="/js/mascara.js"></script>
				<!-- X-Editable -->
				<link href="/css/bootstrap-editable.css" rel="stylesheet">
				<script src="/js/bootstrap-editable.js"></script>

				<script type="text/javascript" src='/js/select2/dist/js/select2.min.js'></script>
				<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2.min.css">
				<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2_bootstrap.min.css">
				<script type="text/javascript" src='/js/select2/dist/js/select2.full.min.js'></script>
				<script type="text/javascript">
				$(document).ready(function(){
				    //Call .imgPicker()
				    $('#avatarModal').imgPicker({
				        url: '/js/imagepicker/server/upload_avatar_empresa.php',
				        aspectRatio: 1,
				        deleteComplete: function() {
				            // Restore default avatar
				            $('#avatar').attr('src', 'http://www.gravatar.com/avatar/0?d=mm&s=150');
				            // Hide modal
				            this.modal('hide');
				        },
				        cropSuccess: function(image) {
		        	        $.post("/empresa/troca_imagem",{
		        				imagem: image.url,
		        			},function(data, status){
		    		    		console.log(data);
		        		    });
				            // Set #avatar src
				            $('#avatar').attr('src', image.versions.avatar.url);
				            // Hide modal
				            this.modal('hide');
				        }
				    });

					//X-Editable
					$.fn.editable.defaults.pk = <?php echo $empresa->id ?>;
					$.fn.editable.defaults.mode = 'inline';
					$.fn.editable.defaults.url = '/Empresa/editable';
					$.fn.editableform.buttons  = '<button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="fa fa-check"></i></button>' +
					'<button type="button" class="btn btn-default btn-sm editable-cancel"><i class="fa fa-ban"></i></button>';

					$('.editable').editable({
		                ajaxOptions: {
		                    dataType: 'json'
		                },
		                emptytext: 'Não informado',
		                success: function(response, newValue) {
		                    if(!response) {
		                        return "Erro desconhecido";
		                    }

		                    if(response.success == false) {
		                        return response.msg;
		                    }
		                }
		            });

					$('.editable-cep').editable({
						tpl:'   <input type="text" id ="cep" class="form-control input-sm" style="padding-right: 24px;">',
		                ajaxOptions: {
		                    dataType: 'json'
		                },
		                emptytext: 'Não informado',
		                success: function(response, newValue) {
		                    if(!response) {
		                        return "Erro desconhecido";
		                    }

		                    if(response.success == false) {
		                        return response.msg;
		                    }
		                }
				  	}).on('shown',function(){
						$("input#cep").mask("99999-999");
				  	});


				  	$('.editable-cidade').editable({
			  	        select2: {
			  	            theme: "bootstrap",
			  	            placeholder: 'Selecione a cidade',
					        minimumInputLength: 3,
					        minimumResultsForSearch: 0,
							language: {
								inputTooShort: function(args) {
								  // args.minimum is the minimum required length
								  // args.input is the user-typed text
								  return "Digite mais...";
								},
								inputTooLong: function(args) {
								  // args.maximum is the maximum allowed length
								  // args.input is the user-typed text
								  return "Excesso de caracteres";
								},
								errorLoading: function() {
								  return "Carregando cidades...";
								},
								loadingMore: function() {
								  return "Carregando mais cidades";
								},
								noResults: function() {
								  return "Nenhuma cidade encontrada";
								},
								searching: function() {
								  return "Procurando cidades...";
								},
								maximumSelected: function(args) {
								  // args.maximum is the maximum number of items the user may select
								  return "Erro ao carregar resultados";
								}
							},
					        ajax: {
					            url: "/pesquisa/lista_cidades",
					            dataType: "json",
					            type: "post",
					            data: function (params) {

					                var queryParameters = {
					                    term: params.term
					                }
					                return queryParameters;
					            },
					            processResults: function (data) {
					                return {
					                    results: $.map(data, function (item) {
					                        return {
					                            text: item.nome+" - "+item.estado,
					                            id: item.id
					                        }
					                    })
					                };
					            }
					        }
			  	        },
			  	        tpl: '<select style="width:150px;">',
			  	        type: 'select2',
			  	        success: function(response, newValue) {
		  	                var editable = $(this).data('editable');
		  	                var option = editable.input.$input.find('option[value="VALUE"]'.replace('VALUE',newValue));
		  	                var newText = option.text();    
		  	                $(this).attr('data-text', newText);
		  	                $(this).attr('data-value', newValue);
		  	                $(this).html(newText);
			  	        },
			  	        display: function(value, sourceData ) {
			  	        	return false;
			  	        }
			  	    });
				});
				</script>
			</div>
		</div>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>