<html>
	<head>
		<title>Nova Vaga Especial - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<script src="//cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>
	</head>
	<body>
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Cadastrar Vaga como Administrador</h2></center>
				<?php
					echo form_open_multipart('adm/cadastrar_vaga');
					echo validation_errors();
					
					echo form_label('Título:');
					echo form_input(array('name'=>'titulo', 'class'=>'form-control', 'placeholder'=>'Título que vai aparecer '), set_value('titulo'));

					echo form_label('Nome da Empresa:');
					echo form_input(array('name'=>'nome_empresa', 'class'=>'form-control'), set_value('nome_empresa'));

					echo form_label('E-mail da Vaga:');
					echo form_input(array('name'=>'email', 'class'=>'form-control'), set_value('email'));

					echo form_label('Cidade:');
			        echo "<select name='cidade' class='form-control' id='selectCidade'><option value=''>Selecione...</option>";
			        foreach($cidades as $cidade){
						if($cidade->id == $empresa->idCidade)
								$selected = 'selected';
							else
								$selected = '';
						echo "<option $selected value='".$cidade->id."'>".$cidade->nome.", ".$cidade->estado."</option>";
					}
			        echo "</select>";

			        echo form_label('Descrição:');
			        echo "<textarea name='descricao' class='form-control ckeditor'>".set_value('descricao')."</textarea>";

	        		echo form_label('Foto:');
	        		echo "<div class='input-group'>
	                        <span class='input-group-btn'>
	                            <span class='btn btn-primary btn-file'>
	                                Enviar foto <input type='file' name='foto'>
	                            </span>
	                        </span>
	                        <input type='text' class='form-control' readonly>
	                    </div>";


	                echo "<br />";
					echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary form-control', 'value'=>'Cadastrar'));

					echo form_close();
				?>
			</div>
			<script type="text/javascript">
				$(document).ready(function(){
					$(".ckeditor").ckeditor();
				});
			</script>

			<script>
				$(document).on('change', '.btn-file :file', function() {
				  var input = $(this),
				      numFiles = input.get(0).files ? input.get(0).files.length : 1,
				      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				  input.trigger('fileselect', [numFiles, label]);
				});

				$(document).ready( function() {
				    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
				        
				        var input = $(this).parents('.input-group').find(':text'),
				            log = numFiles > 1 ? numFiles + ' files selected' : label;
				        
				        if( input.length ) {
				            input.val(log);
				        } else {
				            if( log ) alert(log);
				        }
				        
				    });
				});
			</script>
		</div>
	</body>
</html>