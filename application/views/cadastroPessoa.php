<html>
	<head>
		<title>Currículo Online - Cadastre seu currículo agora mesmo!</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Registre-se agora no Currículo Online e mande seu currículo para várias empresas de sua cidade, tudo totalmente grátis!"/>
		<script src="/js/mascara.js"></script>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		<script>
		$(document).ready(function(){
			$("#nasc").mask("99/99/9999");
		});
		</script>
		
		<div class="panel panel-default col-md-8 col-sm-12 col-xs-12">
			<div class="panel-body">
			<?php
				echo form_open('CadastroPessoa');
				echo "<center><h2>Cadastrar-se</h2></center>";
				echo validation_errors('<div class="alert alert-danger">', '</div>');
				
				echo "<div class='form-group'>";
				echo form_label('Nome:');
				echo form_input(array('name'=>'nome', 'class'=>'form-control'), set_value('nome'), 'autofocus');
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Sobrenome:');
				echo form_input(array('name'=>'sobrenome', 'class'=>'form-control'), set_value('sobrenome'));
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Sexo:');
				echo "<select name='sexo' class='form-control'>
					<option value=''>Selecione</option>
					<option value='Feminino'>Feminino</option>
					<option value='Masculino'>Masculino</option>";
				echo "</select>";
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Data de nascimento:');
				echo form_input(array('name'=>'dt_nascimento', 'class'=>'form-control', 'id'=>'nasc'), set_value('dt_nascimento'));
				echo "</div>";
				/*echo form_label('CPF:');
				echo form_input(array('name'=>'cpf', 'class'=>'form-control', 'id'=>'cpf'), set_value('cpf'));*/
				
				// echo "<div class='form-group'>";
				// echo form_label('Estado: ');
			?>
			<!-- <select id='selectEstado' class='form-control'>
				<option>Selecione um estado</option>
				<?php 
				foreach($estados as $estado){
					echo "<option value='".$estado->id."'>".$estado->nome."</option>";
				} 
				?>
			</select> -->
			<?php
			// echo "</div>";

			echo "<div class='form-group'>";
			echo form_label('Cidade:');
			?>
			<select id='selectCidade' class='form-control' name='cidade'></select>
			<?php
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Nome de usuário:');
				echo form_input(array('name'=>'login', 'class'=>'form-control'), set_value('login'));
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('E-mail:');
				echo form_input(array('name'=>'email', 'type'=>'email', 'class'=>'form-control'), set_value('email'));
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Senha:');
				echo form_input(array('name'=>'senha', 'class'=>'form-control', 'type'=>'password'), set_value('senha'));
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Confirmar a senha:');
				echo form_input(array('name'=>'senha2', 'class'=>'form-control',  'type'=>'password'), set_value('senha2'));
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_submit(array('name'=>'cadastrar', 'class'=>'btn btn-primary pull-right'), 'Cadastrar');
				echo "</div>";

				echo form_close();
			?>
				<!--<form name="login" action="cadastro-pessoa.php" method="post"> <center><h2>Criar Curr&iacute;culo</h2></center><br />
					<input name="urlSite" type="hidden" value="http://icurriculumvitae.com.br" />
					<div>
						<label>Nome Completo: </label>
						<input class="form-control" name="nome" type="text" required pattern="^[À-úa-zA-Z ]{1,40}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'É permitido apenas letras e ter no máximo 40 caracteres.' : '')" />
					</div>
				
					<div>
						<label>Nome de usu&aacute;rio: </label>
						<input class="form-control" name="criarlogin" type="text" required pattern="^[À-úa-zA-Z0-9 ]{6,20}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'É permitido letras, números e ter de 6 a 20 caracteres' : '')"/>
					</div>

					<div>
						<label>Digite o seu e-mail: </label>
						<input class="form-control" name="inseriremail" type="email" required pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'E-mail inválido' : '')" />
					</div>

					<div>
						<label>Senha: </label>
						<input class="form-control" name="criarsenha" id="txtSenha" type="password" required pattern="^.{6,20}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'A senha deve ter de 6 a 20 caracteres' : '')" />
					</div>
					<div>
						<label>Confirmar senha: </label>
						<input class="form-control" name="confirmarsenha" type="password" oninput="validaSenha(this)"/>
					</div>
					
					<br />
					<center><input class="btn btn-primary" id="enviar" type="submit" name="acao" value="Enviar" /></center>
				</form>-->
			</div>


		</div>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
				
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>

		<script type="text/javascript" src='/js/select2/dist/js/select2.min.js'></script>
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2_bootstrap.min.css">
		<script type="text/javascript" src='/js/select2/dist/js/select2.full.min.js'></script>

		<script type="text/javascript">
		$(document).ready(function(){
			$("#selectCidade").select2({
				theme: "bootstrap",
		        minimumInputLength: 3,
		        minimumResultsForSearch: 0,
				language: {
					inputTooShort: function(args) {
					  // args.minimum is the minimum required length
					  // args.input is the user-typed text
					  return "Digite mais...";
					},
					inputTooLong: function(args) {
					  // args.maximum is the maximum allowed length
					  // args.input is the user-typed text
					  return "Excesso de caracteres";
					},
					errorLoading: function() {
					  return "Carregando cidades...";
					},
					loadingMore: function() {
					  return "Carregando mais cidades";
					},
					noResults: function() {
					  return "Nenhuma cidade encontrada";
					},
					searching: function() {
					  return "Procurando cidades...";
					},
					maximumSelected: function(args) {
					  // args.maximum is the maximum number of items the user may select
					  return "Erro ao carregar resultados";
					}
				},
		        ajax: {
		            url: "/pesquisa/lista_cidades",
		            dataType: "json",
		            type: "post",
		            data: function (params) {

		                var queryParameters = {
		                    term: params.term
		                }
		                return queryParameters;
		            },
		            processResults: function (data) {
		                return {
		                    results: $.map(data, function (item) {
		                        return {
		                            text: item.nome+" - "+item.estado,
		                            id: item.id
		                        }
		                    })
		                };
		            }
		        }
			});
		});
		</script>
		
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>