<?php if($total_vagas > 0){ ?>
<div class="panel panel-default col-md-8 col-sm-12 col-xs-12">
	<div class="panel-body" id='painelDireitaBaixo'>
		<?php 
			echo utf8_encode("<center><h2>Vagas de Emprego</h2></center>");
			foreach($vagas as $vaga){
				$what = array( '�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','�','�','@', '.' );

				$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-', '-' );
						    

				if($vaga->especial == 1) {
					$uri = str_replace($what, $by, utf8_decode($vaga->titulo_especial));
					if($vaga->imagem_especial != NULL && $vaga->imagem_especial != "")
						$imagem = "/img/thumbs/vagas/$vaga->imagem_especial";
					else
						$imagem = "/img/img_padrao_vaga.png";
					echo "<div class='col-xs-12 col-sm-6 item-lista' style='margin: 1% 0%; border-bottom: 2px solid #D9D9F3; overflow: hidden; padding-bottom: 2%'>
						<a href='/Vaga/ver/$vaga->id/$uri'>
					      <img class='col-xs-6 col-sm-4 img-thumbnail pull-left' src='$imagem' alt='Vaga de $vaga->titulo_especial.'>
					      <div class='col-xs-6 col-sm-8 pull-left'>
						      <h4 style='float: left;'>$vaga->titulo_especial</h4>
						      <h5 style='clear: left;'>$vaga->empresa em $vaga->cidade_especial_nome, $vaga->uf_estado_especial</h5>
						  </div>
					    </a></div>";
				} else {
					$uri = str_replace($what, $by, utf8_decode($vaga->cargo));
					if($vaga->imagem != NULL && $vaga->imagem != "" && $vaga->imagem != "http://www.icurriculumvitae.com.br/img/avatar-empresa.png")
						$imagem = "$vaga->imagem";
					else
						$imagem = "/img/img_padrao_vaga.png";
					echo "<div class='col-xs-12 col-sm-6 item-lista' style='margin: 1% 0%; border-bottom: 2px solid #D9D9F3; overflow: hidden; padding-bottom: 2%'>
						<a href='/Vaga/ver/$vaga->id/$uri'>
					      <img class='col-xs-6 col-sm-4 img-thumbnail pull-left' src='$imagem' alt='Vaga de $vaga->empresa.'>
					      <div class='col-xs-6 col-sm-8 pull-left'>
						      <h4 style='float: left;'>$vaga->cargo</h4>
						      <h5 style='clear: left;'>$vaga->empresa em $vaga->cidade, $vaga->uf</h5>
						  </div>
					    </a></div>";
				}

			}
		?>
		<a href='/vagas'><button class='btn btn-primary col-sm-6 col-sm-offset-3'>Ver mais vagas</button></a>
	</div>
</div>
<?php } ?>