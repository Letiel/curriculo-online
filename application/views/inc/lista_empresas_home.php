<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
	<div class="panel-body">
	<center><h2 style='text-align: center'>Empresas para enviar currículo</h2></center>
<?php
	foreach($empresas as $empresa){

		$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@' );

		$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
		    
		$uri = str_replace($what, $by, $empresa->nome);

		$lugar = "";

		if($empresa->cidade != NULL && $empresa->cidade != ""){
			$lugar.=$empresa->cidade;
		}

		if($empresa->uf != NULL && $empresa->uf != ""){
			$lugar.= ", ".$empresa->uf;
		}

		echo "<div class='col-xs-12 col-sm-6 item-lista'>
		<a href='".base_url()."empresa/ver/$empresa->id/$uri'>
	      <img class='img-thumbnail col-xs-6 col-sm-4 pull-left' src='$empresa->imagem' alt='Visualizar perfil de $empresa->nome'>
	      <div class='col-xs-6 col-sm-8 pull-left'>
		      <h4 style='float: left;'>$empresa->nome</h4>
		      <h5 style='clear: left;'>em $lugar</h5>
		  </div>
	    </a></div>";
	}
?>
	<a href='/empresas/listar'><button class='btn btn-primary col-sm-6 col-sm-offset-3'>Ver mais empresas</span></button></a>
	</div>
</div>