<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
	<div class="panel-body">
	<center><h2 style='text-align: center'>Últimos currículos cadastrados</h2></center>
<?php
	$i=0;
	foreach($curriculos as $curriculo){
		$nome = $curriculo->nome;
		$nome .= " ".$curriculo->sobrenome;

		$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@' );

		$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
		    
		$uri = str_replace($what, $by, $nome);

		echo "<div class='col-xs-12 col-sm-6 item-lista'>
		<a href='".base_url()."curriculo/ver/$curriculo->id/$uri'>
	      <img class='img-thumbnail col-xs-6 col-sm-4' alt='Ver currículo de $curriculo->nome $curriculo->sobrenome' src='$curriculo->foto' alt='Visualizar perfil de $curriculo->nome'>
	      <div class='col-xs-6 col-sm-8 pull-left'>
		      <h4 style='float: left;'>$curriculo->nome $curriculo->sobrenome</h4>
		      <h5 style='clear: left;'>em $curriculo->cidade, $curriculo->uf</h5>
		  </div>
	    </a></div>";
	    if($i == 1){
	    	$this->load->view("inc/anuncioMeioResultados");
	    }
	    $i++;
	}
?>
	<a href='/pessoas/lista'><button class='btn btn-primary col-sm-6 col-sm-offset-3'>Ver mais currículos</span></button></a>
	</div>
</div>