<meta  charset='utf-8' />
<meta http-equiv="content-language" content="PT-br"/>
<link rel='shortcut icon' type='image/x-icon' href='/img/favicon.png' />
<meta name="keywords" content="emprego, empregos, vagas de emprego, criar curriculum, anunciar vagas de emprego gratis, busca de currículos, cadastro curriculo gratis, site de curriculo, cadastro de currículo gratuito"/>
<?php //echo link_tag('css/bootstrap.min.css'); ?>
<?php //echo link_tag('css/geral.css'); ?>
<link async rel="stylesheet" href='/css/bootstrap.min.css' />
<link async rel="stylesheet" href='/css/geral.css' />
<link rel="stylesheet" href="/css/font-awesome.min.css">

<script src="/js/jquery-2.1.4.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/geral.js"></script>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<meta name="robots" content="index" />
<style>
	@media screen and (min-width: 992px){
		.panel.col-md-8{
			width: 74.233337%;
		}
	}
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60604860-1', 'auto');
  ga('send', 'pageview');

</script>
