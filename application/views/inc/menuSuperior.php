﻿<?php $msg = $this->session->flashdata('mensagem'); ?>
<nav id="menuSuperior"	 class="navbar navbar-default  col-md-12 col-sm-12 col-xs-12">
  <div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
	<?php
		if($this->session->userdata('logado')){
  			?>
  			<a href='/Logout' style='margin: 8px 0 0 0;' class="btn btn-primary col-xs-8 visible-xs" id="deslogar">Logout</a>
  			<?php
  		}else{
	  		if($this->session->userdata('logadoEmpresa')){
	  			?>
	  			<a href='/LogoutEmpresa' style='margin: 8px 0 0 0;' class="btn btn-primary col-xs-8 visible-xs" id="deslogar">Logout</a>
	  			<?php
	  		}else{
	  			?>
	  			<div class="dropdown col-xs-8 navbar-right visible-xs" style='padding: 0;'>
				  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style='margin: 8px 0 0 0;' class="btn btn-primary col-xs-12 navbar-right visible-xs">
				    Login
				    <span class=""></span>
				  </button>
				  <ul class="dropdown-menu" aria-labelledby="dLabel">
				    <li>
				    	<a id="makeLogin" style="cursor: pointer;" data-toggle="modal" data-toggle='tooltip' data-placement="left" title="Logar no sistema" data-target=".bs-example-modal-sm">Pessoa</a>
					</li>
					<li>
						<a id="makeLoginEmpresa" style="cursor:pointer;" data-toggle="modal" data-toggle='tooltip' data-placement="left" title="Logar no sistema" data-target=".bs-empresa-modal-sm">Empresa</a>
					</li>
				  </ul>
				</div>
		<?php
		}
	} ?>
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	</div>
	<script>
		$(document).ready(function () {
	        $('a[href="' + window.location.href + '"]').parent().addClass('active');
	    });
	</script>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	  <ul class="nav navbar-nav">
		<li><a class='item-menu <?php if($pagina == "home") echo "atual" ?>' href="/">Início</a></li>
		<li><a class='item-menu <?php if($pagina == "empresas") echo "atual" ?>' href="/empresas/listar">Empresas</a></li>
		<li><a class='item-menu <?php if($pagina == "vagas") echo "atual" ?>' href="/vagas">Vagas</a></li>
		<li><a class='item-menu <?php if($pagina == "curriculos") echo "atual" ?>' href="/pessoas/lista">Currículos</a></li>
		<li><a class='item-menu <?php if($pagina == "pesquisa") echo "atual" ?>' href='/pesquisa' title='Pesquisar'>Pesquisar</a></li>
		<?php
	  		$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@' );
			$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
	  		if($this->session->userdata('logado')){?>
	  			<?php $perfil = $this->session->userdata('id');
	  			$perfil .= "/".$this->session->userdata('nome');
	  			$perfil .= "-".$this->session->userdata('sobrenome');						    
				$perfil = str_replace($what, $by, $perfil);

	  			?>
	  			<li class='visible-xs <?php if($pagina == "perfil") echo "atual" ?>'><a href="/curriculo/ver/<?php echo $perfil ?>" class='item-menu' title='Atualizar seu currículo'>Atualizar seu currículo</a></li>
	  			<!-- <li class='visible-xs'><a href="/curriculo/ver/<?php echo $perfil ?>" class='item-menu' title="Ver seu currículo">Ver seu currículo</a></li> -->
				<!-- <li class='visible-xs'><a href="/Empresas/listar" class='item-menu' title='Procurar empresas'>Procurar empresas</a></li> -->
				<!-- <li class='visible-xs'><a href="/vagas" class='item-menu' title='Procurar vagas'>Procurar vagas</a></li> -->
				<li class='visible-xs <?php if($pagina == "mensagens") echo "atual" ?>'><a href="/Mensagens" class='item-menu' title='Ver mensagens'>Ver mensagens <?php if ($msg > 0) echo "<i class='badge'>$msg</i>" ?></a></li>
				<li class='visible-xs <?php if($pagina == "configuracoes") echo "atual" ?>'><a href="/curriculo/configuracoes" class='item-menu' title='Conta'>Conta</a></li>
	  	<?php }else{

	  			if($this->session->userdata('logadoEmpresa')){
		  			$perfil_emp = $this->session->userdata('idEmpresa');
		  			$perfil_emp .= "/".$this->session->userdata('nomeEmpresa');
		  			$perfil_emp .= "-".$this->session->userdata('nomeCidade');
						    
					$perfil_emp = str_replace($what, $by, $perfil_emp);
	  				?>
	  				<li class='visible-xs item-menu <?php if($pagina == "suasVagas") echo "atual" ?>'><a class='item-menu' href="/suasvagas" title='Suas Vagas'>Suas Vagas</a></li>
					<!-- <li class='visible-xs'><a href="/Empresa/curriculos_enviados" title='Currículos recebidos'>Currículos recebidos</a></li> -->
					<li class='visible-xs item-menu <?php if($pagina == "mensagens") echo "atual" ?>'><a class='item-menu' href="/Mensagens" title='Ver mensagens'>Ver mensagens <?php if ($msg > 0) echo "<span class='badge'>$msg</span>" ?></a></li>
					<li class='visible-xs item-menu <?php if($pagina == "empresa") echo "atual" ?>'><a class='item-menu' href="/Empresa" title='Editar perfil'>Editar perfil</a></li>
					<!-- <li class='visible-xs'><a href="/Empresa/ver/<?php echo $perfil_emp ?>" title='Visualizar perfil'>Visualizar perfil</a></li> -->
					<li class='visible-xs item-menu <?php if($pagina == "configuracoes") echo "atual" ?>'><a class='item-menu' href="/Empresa/configuracoes" title='Conta'>Conta</a></li>
	  				<?php
	  			}else{
	  		?>
			<li class='visible-xs <?php if($pagina == "CadastroPessoa") echo "atual" ?>'><a class='item-menu' title='Cadastre seu currículo' href='/CadastroPessoa'>Cadastre seu currículo</a></li>
			<li class='visible-xs <?php if($pagina == "CadastroEmpresa") echo "atual" ?>'><a class='item-menu' title='Cadastrar sua empresa' href='/CadastroEmpresa'>Cadastre sua Empresa</a></li>
		<?php }} ?>
	  <?php
	  		if($this->session->userdata('logado')){
	  			?>
	  			<li class='visible-xs'><a href='/Logout' id="deslogar" class='item-menu' title='Logout'>Logout</a></li>
	  			<?php
	  		}else{
		  		if($this->session->userdata('logadoEmpresa')){
		  			?>
		  			<li class='visible-xs'><a href='/LogoutEmpresa' class="navbar-right hidden-xs" class='item-menu' id="deslogar" title='Logout'>Logout</a></li>
		  			<?php
		  		}
		} ?>
	  </ul>
	  <!-- <form class="navbar-form navbar-right" role="search" action='/Pesquisa/empresas' method='post'>
		<div class="form-group">
		  <input type="text" class="form-control" name='campo_pesquisa_filtro' placeholder="Pesquisar">
		</div>
		<button type="submit" class="btn btn-default">Ir</button>
	  </form> -->
	  <div class='botoesLogin navbar-right <?php if($this->session->userdata('logado')) echo "logado"; else if($this->session->userdata('logadoEmpresa')) echo "logadoEmpresa"; ?>'>
	  	


	  <?php
	  		if($this->session->userdata('logado')){?>
	  			<?php $perfil = $this->session->userdata('id');
	  			$perfil .= "/".$this->session->userdata('nome');
	  			$perfil .= "-".$this->session->userdata('sobrenome');						    
				$perfil = str_replace($what, $by, $perfil);

	  			?>
	  			<a class='espacado hidden-xs <?php if($pagina == "perfil") echo "atual" ?>' href="/curriculo/ver/<?php echo $perfil ?>" title='Atualizar seu currículo' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-pencil-square-o'></span></a>
	  			<!-- <a class='hidden-xs' href="/curriculo/ver/<?php echo $perfil ?>" data-toggle="tooltip" data-placement="bottom" title="Ver seu currículo"><span class='fa fa-binoculars'></span></a> -->
				<!-- <a class='espacado hidden-xs' href="/Empresas/listar" title='Procurar empresas' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-search'></span></a> -->
				<!-- <a class='espacado hidden-xs' href="/vagas" title='Procurar vagas' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-map-pin'></span></a> -->
				<a class='espacado hidden-xs <?php if($pagina == "mensagens") echo "atual" ?>' href="/Mensagens/ver" data-toggle="tooltip" data-placement="bottom" title='Ver mensagens'><span class='fa fa-weixin'></span><?php if ($msg > 0) echo "<i class='badge'>$msg</i>" ?></a>
				<a class='espacado hidden-xs <?php if($pagina == "configuracoes") echo "atual" ?>' href="/curriculo/configuracoes" title='Conta' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-cog'></span></a>
	  	<?php }else{

	  			if($this->session->userdata('logadoEmpresa')){
		  			$perfil_emp = $this->session->userdata('idEmpresa');
		  			$perfil_emp .= "/".$this->session->userdata('nomeEmpresa');
		  			$perfil_emp .= "-".$this->session->userdata('nomeCidade');
						    
					$perfil_emp = str_replace($what, $by, $perfil_emp);
	  				?>
	  				<a class='espacado hidden-xs <?php if($pagina == "suasVagas") echo "atual" ?>' href="/suasvagas" title='Suas Vagas' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-list-ul'></span></a>
					<!-- <a class='espacado hidden-xs' href="/Empresa/curriculos_enviados" title='Currículos recebidos' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-list'></span></a> -->
					<a class='espacado hidden-xs <?php if($pagina == "mensagens") echo "atual" ?>' href="/Mensagens" title='Ver mensagens' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-weixin'></span><?php if ($msg > 0) echo "<i class='badge'>$msg</i>" ?></a>
					<a class='espacado hidden-xs <?php if($pagina == "empresa") echo "atual" ?>' href="/Empresa" title='Editar perfil' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-pencil-square-o'></span></a>
					<!-- <a class='espacado hidden-xs' href="/Empresa/ver/<?php echo $perfil_emp ?>" title='Visualizar perfil' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-binoculars'></span></a> -->
					<a class='espacado hidden-xs <?php if($pagina == "configuracoes") echo "atual" ?>' href="/Empresa/configuracoes" title='Conta' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-cog'></span></a>
	  				<?php
	  			}else{
	  		?>
			<a class='espacado hidden-xs <?php if($pagina == "CadastroPessoa") echo "atual" ?>' title='Cadastre seu currículo' data-toggle="tooltip" data-placement="bottom" href='/CadastroPessoa'><span class='fa fa-user'></span></a>
			<a class='espacado hidden-xs <?php if($pagina == "CadastroEmpresa") echo "atual" ?>' title='Cadastrar sua empresa' data-toggle="tooltip" data-placement="bottom" href='/CadastroEmpresa'><span class='fa fa-briefcase'></span></a>
		<?php }} ?>
	  <?php
	  		if($this->session->userdata('logado')){
	  			?>
	  			<a class='espacado hidden-xs' href='/Logout' id="deslogar" title='Logout' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-sign-out'></span></a>
	  			<?php
	  		}else{
		  		if($this->session->userdata('logadoEmpresa')){
		  			?>
		  			<a class='espacado hidden-xs' href='/LogoutEmpresa' class="navbar-right hidden-xs" id="deslogar" title='Logout' data-toggle="tooltip" data-placement="bottom"><span class='fa fa-sign-out'></span></a>
		  			<?php
		  		}else{
		  			?>
		  			<div class="espacado dropdown hidden-xs" title='Login' data-toggle="tooltip" data-placement="right">
		  			   <a class="dropdown-toggle" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					      <span class="fa fa-sign-in"></span>
					   </a>
					   <ul class="dropdown-menu" aria-labelledby="dLabel">
					     <li>
					    	<a id="makeLogin" style="cursor: pointer;" data-toggle="modal" data-toggle='tooltip' data-placement="left" title="Logar no sistema" data-target=".bs-example-modal-sm">Pessoa</a>
						 </li>
						 <li role="separator" class="divider"></li>
						 <li>
							<a id="makeLoginEmpresa" style="cursor:pointer;" data-toggle="modal" data-toggle='tooltip' data-placement="left" title="Logar no sistema" data-target=".bs-empresa-modal-sm">Empresa</a>
						 </li>
					   </ul>
					</div>


		  			<!--<a class="link navbar-right" id="makeLoginEmpresa" style="margin-top: 15px; cursor: pointer;" data-toggle="modal" data-toggle='tooltip' data-placement="left" title="Logar no sistema" data-target=".bs-empresa-modal-sm">Login Empresa</a>
					<a class="link navbar-right" id="makeLogin" style="margin-top: 15px; margin-right: 15px; cursor: pointer;" data-toggle="modal" data-toggle='tooltip' data-placement="left" title="Logar no sistema" data-target=".bs-example-modal-sm">Login Pessoa</a>-->
			<?php
			}
		} ?>
		</div><!-- /.botoesLogin -->
	</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php echo $this->session->flashdata('cadastroVaga'); ?>
		<div class="modal fade bs-example-modal-sm" id='modalLogin' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-sm">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Login</h4>
			  </div>
			  <form style='margin: 1%;' action='/Login' method="post">
				  <div class="form-group">
					<label for="inputLogin">Login ou E-mail</label>
					<input type="text" class="form-control" name='login' id="inputLogin" placeholder="Login ou E-mail">
				  </div>
				  <div class="form-group">
					<label for="inputSenha">Senha</label>
					<input type="password" name='senha' class="form-control" id="inputSenha" placeholder="********">
				  </div>
				  <input type='hidden' name='pagina' style='display: none;' id='pagina' />
					<script>
						$("#pagina").val(window.location.href);
					</script>
				  <div class="modal-footer">
				  	<div class='pull-left'>
					<a href='/CadastroPessoa' class="link pull-left">Registrar currículo</a><br />
					<a href='/CadastroEmpresa' class="link pull-left">Registrar Empresa</a></div>
					<button type="submit" class="btn btn-primary pull-right">Entrar</button>
				  </div>
			</div>
		  </div>
		</div>
	  </form>

	  <div class="modal fade bs-empresa-modal-sm" id='modalLogin' tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-sm">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Login Empresarial</h4>
			  </div>
			  <form style='margin: 1%;' action='/LoginEmpresa' method="post">
				  <div class="form-group">
					<label for="inputLogin">Login ou E-mail</label>
					<input type="text" class="form-control" name='login' id="inputLogin" placeholder="Login ou E-mail">
				  </div>
				  <div class="form-group">
					<label for="inputSenha">Senha</label>
					<input type="password" name='senha' class="form-control" id="inputSenha" placeholder="********">
				  </div>
				  <div class="modal-footer">
				  	<div class='pull-left'>
					<a href='/CadastroPessoa' class="link pull-left">Registrar currículo</a><br />
					<a href='/CadastroEmpresa' class="link pull-left">Registrar Empresa</a></div>
					<button type="submit" class="btn btn-primary pull-right">Entrar</button>
				  </div>
			</div>
		  </div>
		</div>
	  </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->