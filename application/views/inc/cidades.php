<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
	<div class='panel-body'>
		<form class='form-inline'>
			<div class='form-group select pull-left;'>
				<select id='selectEstado' class='form-control' style='width: 100%;'>
					<option>Selecione o estado</option>
					<?php
						foreach ($estados as $ponteiro){
							echo "<option value=".$ponteiro->id.">".$ponteiro->nome."</option>";
						}
					?>
				</select>
			</div>
			<div class='form-group select pull-right;'>
				<select id='selectCidade' class='form-control' style='width: 100%;'>
					<option>Selecione a cidade</option>
				</select>
			</div>
		</form>
	</div>
</div>