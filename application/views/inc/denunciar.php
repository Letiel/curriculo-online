<div class="pull-right" style='margin: 8px 0 0 0'>
  <!-- Button trigger modal -->
  <a href='#' data-toggle="modal" data-target="#modal_denuncia">
    Denunciar
  </a>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_denuncia" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Denunciar publicação</h3>
      </div>
      <form action='#' id='form_denunciar' method='post'>
        <div class="modal-body">
          <div id="resultado_denuncia"></div>
            <div class='form-group'>
              <label for='email'>Informe seu e-mail:</label>
              <input type='email' class='form-control' name='email_denuncia' id='email_denuncia' required />
            </div>

            <div class='form-group'>
              <label for='email'>Informe o motivo da denúncia:</label>
              <input type='text' class='form-control' name='motivo_denuncia' id='motivo_denuncia' required />
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-warning" id='submit'>Denunciar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#form_denunciar").submit(function(){
      $.post("/denuncia", {
        email: $("#email_denuncia").val(),
        motivo: $("#motivo_denuncia").val(),
        url: window.location.href
      }, function(result){
          $("#email_denuncia").attr("disabled", true);
          $("#motivo_denuncia").attr("disabled", true);
          $("#submit").attr("disabled", true);
          $("#resultado_denuncia").html(result);
      });
      event.preventDefault();
    });
  });
</script>