<html>
	<head>
		<title>Pesquisa - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Faça uma busca personalizada das pessoas de sua região que estão em busca de um emprego." />
		<style>
		.result{
			overflow: hidden;
		}
		</style>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<form class='form-inline' name='form_pesquisa' style='margin: 0 auto;' action='' method='post' >
					<div class='form-group'>
						<input class='form-control' name='campo_pesquisa_filtro' placeholder='Pesquisa' />
					</div>
					<div class='btn-group' role='group'>
						<a class='btn btn-default' onclick= "pesquisa('<?php echo base_url(); ?>Pesquisa/vagas')">Vagas</a>
						<a class='btn btn-default' onclick= "pesquisa('<?php echo base_url(); ?>Pesquisa/empresas')">Empresas</a>
						<a class='btn btn-default' onclick= "pesquisa('<?php echo base_url(); ?>Pesquisa/curriculos')">Curriculos</a>
					</div>
					<script>
						function pesquisa(pag){
							document.form_pesquisa.action= pag;
   							document.form_pesquisa.submit();
						}
					</script>
				</form>
				<h4>Sua pesquisa: <b><?php echo $campo_pesquisa_filtro; ?></b> em <b><?php echo $pagina; ?></b></h4>
				<div class=''>
					<?php
						$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º' );
						$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
						if(isset($vagas))
							foreach($vagas as $vaga){
								$uri = str_replace($what, $by, $vaga->cargo);

								echo "<div class='col-md-12 result' style='clear: both; margin: 1% 0%; border-bottom: 2px solid #D9D9F3; padding-bottom: 2%'>
									<a href='".base_url()."vaga/ver/$vaga->id/$uri'>
								      <img class='img-thumbnail' src='$vaga->imagem' alt='Visualizar vaga' style='width: 20%; float: left;'>
								      <div style='float: left' class='col-md-9'>
									      <h4 style='float: left;'>$vaga->cargo</h4>
									      <h5 style='clear: left;'>$vaga->empresa em $vaga->cidade, $vaga->uf</h5>
									  </div>
								    </a></div>";
							}
						else
							if(isset($empresas))
								foreach($empresas as $empresa){
									$uri = str_replace($what, $by, $empresa->nome);
									echo "<div class='col-md-12 result' style='clear: both; margin: 1% 0%; border-bottom: 2px solid #D9D9F3; padding-bottom: 2%'>
									<a href='".base_url()."empresa/ver/$empresa->id/$uri'>
								      <img class='img-thumbnail' src='$empresa->imagem' alt='Visualizar empresa' style='width: 20%; float: left;'>
								      <div style='float: left' class='col-md-9'>
									      <h4 style='float: left;'>$empresa->nome</h4>
									      <h5 style='clear: left;'>$empresa->nome em $empresa->cidade, $empresa->uf</h5>
									  </div>
								    </a></div>";
								}
							else
								if(isset($curriculos))
									foreach($curriculos as $curriculo){
										$nome = $curriculo->nome . " " . $curriculo->sobrenome;
										$uri = str_replace($what, $by, $nome);
										echo "<div class='col-md-12 result' style='clear: both; margin: 1% 0%; border-bottom: 2px solid #D9D9F3; padding-bottom: 2%'>
										<a href='".base_url()."curriculo/ver/$curriculo->id/$uri'>
									      <img class='img-thumbnail' src='$curriculo->foto' alt='Visualizar perfil de $curriculo->nome $curriculo->sobrenome' style='width: 20%; float: left;'>
									      <div style='float: left' class='col-md-9'>
										      <h4 style='float: left;'>$nome</h4>
										      <h5 style='clear: left;'>$curriculo->cidade, $curriculo->uf</h5>
										  </div>
									    </a></div>";
									}
						echo $paginacao;
					?>
				</div>
			</div>
		</div>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>