<html>
	<head>
		<title>Currículo Online - Currículos</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Crie seu currículo totalmente grátis e envie para empresas de sua região. Procurar emprego sem sair de casa com seu Curriculum Vitae." />
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<?php
				echo form_open('CriarVaga');
				echo "<center><h2>Criar uma vaga</h2></center>";
				echo validation_errors('<div class="alert alert-danger">', '</div>');
				echo $this->session->flashdata('cadastroVaga');
				
				echo "<div class='form-group'>";
				echo form_label('Área:');

				?>
				<select name='area' class='form-control' id='select_areas'>
					<option value=''>Selecione...</option>
					<?php
						foreach($areas as $area){
							echo "<option value='$area->id'>$area->descricao</option>";
						}
					?>
				</select>
				<?php echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Cargo:');
				echo form_input(array('name'=>'cargo', 'class'=>'form-control'), set_value('cargo'));
				echo "</div>";

				echo "<div class='form-group'>";
				echo form_label('Descrição:');
				echo "<textarea name='descricao' class='form-control'>".set_value('descricao')."</textarea>";
				echo "</div>";
				?>

				<a href="#myModal" role="button" class="link" id='abre_modal' data-toggle="modal">Sua área não está na lista?</a>
					<!-- Modal HTML -->
					<div id="modal_area" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Cadastrar nova área</h4>
					            </div>
					            <div class="modal-body">
					            <? echo form_label('Nome da área:'); ?>
					            <input type='text' id='nome_area' placeholder='Descreva o nome da área' class='form-control' />
					            </div>
					            <div class="modal-footer">
					                <button type="button" id='botao_cadastrar_area' class="btn btn-primary" onclick='return false'>Cadastrar</button>
					            </div>
					        </div>
					    </div>
					</div>
					<script type="text/javascript">
					function atualizaAreas(){
						$.post("<?php echo base_url(); ?>CriarVaga/atualiza_areas",{},function(data, status){
							$('#select_areas').html(data);
						});
					}
					$(document).ready(function(){
					    $("#abre_modal").click(function(){
					        $("#modal_area").modal('show');
					    });

					    $('#botao_cadastrar_area').click(function(){
					    	area = $('#nome_area').val();

					    	if(area != null && area != ""){
						    	$.post("<?php echo base_url(); ?>CriarVaga/cadastra_nova_area",{
							        nome_area: area,
							    },function(data, status){});
						    	$("#modal_area").modal('hide');
						    	$("#nome_area").val('');
						    	atualizaAreas();
							}else{
								alert ("Informe o nome da área!");
							}
					    });
					});
					</script>




					<!-- ------------------------------------------------------------------------- -->

					<a href="#myModal" role="button" class="link" id='modal_areas_empresa' data-toggle="modal">Ver suas vagas criadas</a>
					<!-- Modal HTML -->
					<div id="modal_areas" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Suas vagas</h4>
					            </div>
					            <div class="modal-body">
					            	<?php
					            		foreach($vagas_empresa as $vaga_empresa){
					            			echo "<div class='alert alert-warning alert-dismissible' role='alert'>
											  <button type='button' onclick='delete_area($vaga_empresa->id)' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
											  <strong>$vaga_empresa->cargo</strong><br />$vaga_empresa->descricao
											</div>";
					            		}
					            	?>
					            </div>
					        </div>
					    </div>
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
						    $("#modal_areas_empresa").click(function(){
						        $("#modal_areas").modal('show');
						    });
						});

						function delete_area(id){
							$.post("<?php echo base_url(); ?>CriarVaga/deleta_vaga",{
								id : id,
							},function(data, status){});
						}
					</script>

					<!-- ------------------------------------------------------------------------- -->

				<?php

				echo form_submit(array('name'=>'cadastrar', 'class'=>'btn btn-primary pull-right'), 'Criar vaga');
				
				echo form_close();
			?>
			</div>
		</div>
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>