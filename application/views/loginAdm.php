<html>
	<head>
		<title>System Adm</title>
		<?php $this->load->view('inc/headBasico.php');?>
	</head>
	<body>
		<div class='panel panel-default col-md-6' style='  margin: 5% auto 0 auto; float: none; padding: 2% 10%;'>
			<h2 style='text-align: center;'>Login</h2>
			<div class='panel-body'>
				<?php
					echo "<form action='login' method='post' style='overflow: hidden;'>";
					
					echo validation_errors();
					echo $this->session->flashdata('Erro');

					echo form_label('Login:');
					echo form_input(array('name'=>'login', 'class'=>'form-control'), set_value('login'));

					echo form_label('Senha:');
					echo form_input(array('name'=>'senha', 'class'=>'form-control', 'type'=>'password'), set_value('senha'));

					echo form_submit(array('name'=>'entrar', 'class'=>'btn btn-primary pull-right', 'value'=>'Entrar', 'style'=>'margin: 1% 0 0 0; overflow: hidden;'));
					echo "</form>";
				?>
			</div>
		</div>
	</body>
</html>