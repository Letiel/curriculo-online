<html>
	<head>
		<title>Currículo Online - Veja currículos de pessoas que estão em busca de emprego</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Faça uma busca personalizada das pessoas de sua região que estão em busca de um emprego." />
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<center><h2 style='text-align: center'>Empresas</h2></center>
			<div class='panel-body'>
				<?php
					$i = 0;
					foreach($empresas as $empresa){
						$nome = $empresa->nome;

						$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@' );

						$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
						    
						$uri = str_replace($what, $by, $nome);
						
						?>
							<div class='row col-sm-6 col-xs-12 item-lista'>
							    <a href="/empresa/ver/<?php echo $empresa->id?>/<?php echo $uri ?>">
								    <!-- <div class="thumbnail col-sm-12"> -->
								      <img class='img-thumbnail col-xs-6 col-sm-4 pull-left' src="<?php echo $empresa->imagem ?>" alt="Visualizar perfil de <?php echo $empresa->nome ?>">
								    <!-- </div> -->
							    	<div class="col-sm-8">
									  <h4><?php echo $empresa->nome ?></h4>
									  <?php
									  if ($empresa->cidade != NULL && $empresa->uf != ''){
											echo  "</h5>Em $empresa->cidade, $empresa->uf</h5>";
										}
									  ?>
									</div>
								</a>
							</div>
						<?php
						// if($i == 2)
						// 	$this->load->view("inc/anuncioMeioResultados");
						// $i++;
					}
					echo $paginacao;
				?>
			</div>
		</div>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>