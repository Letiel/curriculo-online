<?php $vaga = $vaga[0]; ?>
<html>
	<head>
		<title><?php echo ($vaga->especial == 1) ? $vaga->titulo_especial : $vaga->cargo ?> - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content='<?php echo ($vaga->especial ? "$vaga->titulo_especial | Veja outras vagas em $vaga->nome_cidade_especial - $vaga->uf_estado_especial | Aproveite para fazer seu cadastro e procurar vagas de emprego em sua cidade!" : "Veja essa vaga de $vaga->cargo em EnviarCurriculo.net. Aproveite para fazer seu cadastro e procurar vagas de emprego em sua cidade!")?>' />
		<meta property="og:image" content="<?php echo ($vaga->especial ? base_url()."img/thumbs/vagas/$vaga->imagem_especial" : base_url()."img/thumbs/vagas/$vaga->imagem") ?>"/>
		<meta property="og:title" content='<?php echo ($vaga->especial ? "$vaga->titulo_especial" : "$vaga->cargo") ?>'/>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
			<?php
				if (!$this->session->userdata('logadoEmpresa')){
					if(!$candidatura && $this->session->userdata('logado')){
						?><div id='cont_cddt'><button class='btn btn-primary btn-sm pull-right' value='<?php echo $vaga->id_vaga ?>' id='cddt'>Candidatar-se à vaga</button></div><?php
					}else if(!$candidatura && !$this->session->userdata('logado')){
						?>
						<div id='cont_cddt'>
							<button id='makeLogin' class='btn btn-primary bt-sm pull-right' data-toggle='modal' data-toggle='tooltip' data-placement='left' data-target='.bs-example-modal-sm' title='É necessário logar no sistema.'>Candidatar-se à vaga</button>
						</div>
						<?php 
					}else{
						?>
						<div class='alert alert-success'><h3 style='margin: 0px; text-align: center;'>Você é um  candidato a esta vaga!</h3><center><small>O link do seu currículo foi enviado para a empresa! Aguarde contato.</small></center></div>
						<?php
					}
				}
			?>


			<?php 
			if($vaga->especial == 1) {
				if($vaga->imagem_especial != NULL && $vaga->imagem_especial != "")
					$imagem = "/img/thumbs/vagas/$vaga->imagem_especial";
				else
					$imagem = "/img/img_padrao_vaga.png";
				echo "<div class='col-lg-3 col-md-4 col-sm-4 col-xs-5' style='margin: 10px 50px 10px 25px'>";
				echo "<img src='$imagem' class='img-responsive' />"; 
				echo "</div>";
				echo "<h1 style='font-size: 20px;'> $vaga->titulo_especial </h1>";
				echo"Em <b>$vaga->nome_empresa_especial</b>";

				echo "<h5>Em <b>" . $vaga->nome_cidade_especial . " - " . $vaga->uf_estado_especial . "</b></h5>";
				echo "<h4>".$vaga->descricao_especial."</h4>";

				echo "</div>";
			} else {
				if($vaga->imagem != NULL && $vaga->imagem != "" && $vaga->imagem != "http://www.icurriculumvitae.com.br/img/avatar-empresa.png")
					$imagem = "$vaga->imagem";
				else
					$imagem = "/img/img_padrao_vaga.png";
				echo "<div class='col-lg-3 col-md-4 col-sm-4 col-xs-5' style='margin: 10px 50px 10px 25px'>";
				echo "<img src='$imagem' class='img-responsive' />"; 
				echo "</div>";
				echo "<h1>".$vaga->cargo."</h1>";
				echo "<h4>".$vaga->descricao."</h4>";
				$nome = $vaga->nome;

				$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@' );

				$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
				    
				$uri = str_replace($what, $by, $nome);

				echo" <a href='".base_url()."empresa/ver/".$vaga->id_empresa."/$uri' target='_blank'> <h5> $vaga->nome </h5>
				</a>";

				echo "<h5>Em <b>" . $vaga->cidade . " - " . $vaga->uf . "</b></h5>";
				echo "</div>";
			}
			?>
				<?php $this->load->view("inc/denunciar.php"); ?>
			</div>
		</div>
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		<script>
		$(document).ready(function(){
			$("#cddt").click(function(){
				$.post("<?php echo base_url(); ?>vaga/candidatar",{
					vaga : $("#cddt").val(),
				},function(data, status){
                    if(data != null && data != ""){
                        alert(data);
                    }else{
                        $("#cddt").removeClass("btn-primary");
                        $("#cddt").addClass("btn-success");
                        $("#cddt").remove();
                        $("<div class='alert alert-success'><h3 style='margin: 0px; text-align: center;'>Você se candidatou a esta vaga!</h3></div>").appendTo('#cont_cddt');
                        $("#cddt").addClass("disabled");
                        $("#cddt").html("Candidato");
                    }
                });
			});
		});
		</script>
	</body>
</html>