<html>
	<head>
		<title>Suas Vagas - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Crie seu currículo totalmente grátis e envie para empresas de sua região. Procurar emprego sem sair de casa com seu currículo." />
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center><h2>Vagas de Emprego</h2></center>
				<a href="#modal_criar_vaga" class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 btn btn-primary' role="button" id='abre_modal' data-toggle="modal">Criar</a>
				<?php
					foreach($vagas as $vaga){
					?>
					<div class='col-xs-12'>
						<div class="separador"></div>
						<h4 class='azul quebra-separador'>
							<a style='font-size: 20px' href='#' class='editable' data-mode='inline' data-pk='<?php echo $vaga->id ?>' data-name='cargo'><?php echo $vaga->cargo ?></a>
							<a href="#modal_altera_vaga_<?php echo $vaga->id ?>" data-toggle="modal" class="badge"><?php echo $vaga->area ?></a>
						</h4>
						<h4 class='azul' style='float: left;'>
							<a href='#' style='font-size: 16px' class='editable' data-mode='popup' data-pk='<?php echo $vaga->id ?>' data-type='textarea' data-name='descricao'><?php echo $vaga->descricao ?></a>
						</h4>
						<div class="btn-group pull-right">
							<button class="deleta_vaga btn btn-<?php echo $vaga->desativado ? "warning" : "info"?> btn-sm" value='<?php echo $vaga->id ?>'><?php echo $vaga->desativado ? "Ativar" : "Desativar" ?></button>
							<a href='/curriculos_enviados/<?php echo $vaga->id ?>' class="btn btn-success btn-sm">
							Currículos
							<span class='badge'><?php echo $vaga->total ?></span>
							</a>
						</div>
				    </div>
				    <div id="modal_altera_vaga_<?php echo $vaga->id ?>" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content">
				                <div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h4 class="modal-title">Escolha uma área</h4>
				                </div>
				                <!-- <form id='cadastro_vaga' action='' method='post'> -->
				    	            <div class="modal-body">
				    	            	<div class="result_altera_area_<?php echo $vaga->id ?>"></div>
				    		            <div class="form-group">
				    		            	<?php echo form_label("Área") ?>
				    		            	<select id='nova_area_<?php echo $vaga->id ?>' name='area' class="form-control area">
				    		            		<option value=''>Selecione uma área</option>
				    		            		<?php
				    		            		foreach($areas as $area){
				    		            			echo "<option value='$area->id'>$area->descricao</option>";
				    		            		}
				    		            		?>
				    		            	</select>
				    		            	<a href="#modal_area" role="button" class="link" data-toggle="modal">Sua área não está na lista?</a>
				    		            </div>
				    		            <input type='hidden' id='id_vaga_alterar_<?php echo $vaga->id ?>' value='<?php echo $vaga->id ?>' />
				    	            </div>
				    	            <div class="modal-footer">
				    	                <button type="submit" id='botao_alterar_area' value='<?php echo $vaga->id ?>' class="btn btn-primary botao_alterar_area">Alterar</button>
				    	            </div>
				    	        <!-- </form> -->
				            </div>
				        </div>
				    </div>

					<?php
					}
				?>

				<link href="/css/bootstrap-editable.css" rel="stylesheet">
				<script src="/js/bootstrap-editable.js"></script>
				<script type="text/javascript">
					//X-Editable

					$('.editable').editable({
						url: '/suasvagas/editable',
						buttons: '<button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="fa fa-check"></i></button>' +
					'<button type="button" class="btn btn-default btn-sm editable-cancel"><i class="fa fa-ban"></i></button>',
		                ajaxOptions: {
		                    dataType: 'json'
		                },
		                emptytext: 'Não informado',
		                success: function(response, newValue) {
		                    if(!response) {
		                        return "Erro desconhecido";
		                    }

		                    if(response.success == false) {
		                        return response.msg;
		                    }
		                }
		            });
				</script>

				<!-- <a href="#modal" role="button" class="link" data-toggle="modal">Sua área não está na lista?</a> -->
					<!-- Modal HTML -->
					<div id="modal_criar_vaga" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Cadastrar nova vaga</h4>
					            </div>
					            <form id='cadastro_vaga' action='/CriarVaga' method='post'>
						            <div class="modal-body">
						            	<div id="result"></div>
							            <div class="form-group">
							            	<?php echo form_label("Área") ?>
							            	<select id='area' name='area' class="form-control area">
							            		<option value=''>Selecione uma área</option>
							            		<?php
							            		foreach($areas as $area){
							            			echo "<option value='$area->id'>$area->descricao</option>";
							            		}
							            		?>
							            	</select>
							            	<a href="#modal_area" role="button" class="link" data-toggle="modal">Sua área não está na lista?</a>
							            </div>
						            	<div class="form-group">
								            <?php echo form_label('Cargo'); ?>
								            <input type='text' name='cargo' id='cargo' placeholder='Descreva o nome da área' class='form-control' />
								        </div>
								        <div class="form-group">
								        	<?php echo form_label("Descrição da vaga") ?>
								        	<textarea id='descricao' class="form-control" name='descricao' placeholder='Descreva a vaga'></textarea>
								        </div>
						            </div>
						            <div class="modal-footer">
						                <button type="submit" id='botao_cadastrar_vaga' class="btn btn-primary">Cadastrar</button>
						            </div>
						        </form>
					        </div>
					    </div>
					</div>
					<!-- Modal HTML -->
					<div id="modal_area" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Cadastrar nova área</h4>
					            </div>
					            <div class="modal-body">
					            <div id="result_cadastra_area"></div>
						            <div class="form-group">
							            <? echo form_label('Nome da área:'); ?>
							            <input type='text' id='nome_area' placeholder='Descreva o nome da área' class='form-control' />
						            </div>
					            </div>
					            <div class="modal-footer">
					                <button type="button" id='botao_cadastrar_area' class="btn btn-primary" onclick='return false'>Cadastrar</button>
					            </div>
					        </div>
					    </div>
					</div>
					<script type="text/javascript">
					function atualizaAreas(){
						$.post("/CriarVaga/atualiza_areas",{},function(data, status){
							$('.area').html(data);
						});
					}
					$(document).ready(function(){

						$('.deleta_vaga').click(function(){
					    	if(confirm("Tem certeza?")){
						    	$.post("/Suasvagas/deleta_vaga",{
							        id: $(this).val(),
							    },function(data, status){
							    	console.log(result);
							    	location.reload();
							    });
							}
					    });

					    $('#botao_cadastrar_area').click(function(){
					    	area = $('#nome_area').val();

					    	$("#result_cadastra_area").html("");
					    	if(area != null && area != ""){
						    	$.post("/CriarVaga/cadastra_nova_area",{
							        nome_area: area,
							    },function(data, status){
							    	if(data == "sucesso"){
								    	$("#modal_area").modal('hide');
								    	$("#nome_area").val('');
								    	atualizaAreas();
								    } else {
								    	$("#result_cadastra_area").html(data);
								    }
							    });
							}else{
								alert ("Informe o nome da área!");
							}
					    });

					    $('.botao_alterar_area').click(function(){
					    	id_vaga = $(this).val();
					    	nova_area = $('#nova_area_'+id_vaga).val();

					    	$(".result_altera_area_"+id_vaga).html("");
					    	if(id_vaga != null && id_vaga != "" && nova_area != null && nova_area != ""){
						    	$.post("/CriarVaga/alterar_area_vaga",{
							        id_vaga: id_vaga,
							        nova_area: nova_area,
							    },function(data, status){
							    	if(data == "sucesso"){
								    	$("#modal_altera_vaga_"+id_vaga).modal('hide');
								    	location.reload();
								    } else {
								    	$(".result_altera_area_"+id_vaga).html(data);
								    }
								    console.log(data);
							    });
							}else{
								alert ("Informe todos os dados!");
							}
					    });
					});
					</script>



					<!-- <a href="#myModal" role="button" class="link" id='modal_areas_empresa' data-toggle="modal">Ver suas vagas criadas</a> -->
					<!-- Modal HTML -->
					<div id="modal_areas" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Suas vagas</h4>
					            </div>
					            <div class="modal-body">
					            	<?php
					            		foreach($vagas_empresa as $vaga_empresa){
					            			echo "<div class='alert alert-warning alert-dismissible' role='alert'>
											  <button type='button' onclick='delete_area($vaga_empresa->id)' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
											  <strong>$vaga_empresa->cargo</strong><br />$vaga_empresa->descricao
											</div>";
					            		}
					            	?>
					            </div>
					        </div>
					    </div>
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
							$("#cadastro_vaga").submit(function(){
								$.post("/CriarVaga",{
									area : $("#area").val(),
									descricao : $("#descricao").val(),
									cargo : $("#cargo").val(),
								},function(data, status){
									if(data == "sucesso")
										location.reload();
									else
										$("#result").html(data);
								});
								return false;
							});

						    $("#modal_areas_empresa").click(function(){
						        $("#modal_areas").modal('show');
						    });
						});

						function delete_area(id){
							$.post("/CriarVaga/deleta_vaga",{
								id : id,
							},function(data, status){});
						}
					</script>
			</div>
		</div>
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>