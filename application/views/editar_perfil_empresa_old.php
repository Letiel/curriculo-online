<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
	<head>
		<title>Editar empresa - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<style>
			.separa{
				float: left;
				margin-left: 2%;
				font-weight: bold;
				border-left: 1px solid #CCC;
				padding-left: 1%;
			}

			.aumenta{
				overflow: hidden;
  				height: auto;
			}
		</style>

		<script src="<?php echo base_url();?>js/mascara.js"></script>
	</head>
	<body>
		<script>
		jQuery(function($){
   			$("#cep").mask("99999-999");
			});

		$(document).ready(function(){
			$(".form-control").blur(function(){
				//console.log($(this).attr("name") + "\n" + $(this).val());
				$.post("<?php echo base_url(); ?>empresa/update_especifico",{
					campo: $(this).attr("name"),
					value: $(this).val()
				},function(data, status){
			    	console.log(data);
			    });
			});
		});
		</script>

		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Editar Empresa</h2></center>
				<?php
					echo form_open_multipart('Empresa');
					$empresa = $empresa[0];
					echo validation_errors();
					echo $this->upload->display_errors();
					
					echo form_label('Nome:');
					echo form_input(array('name'=>'nome', 'class'=>'form-control'), set_value('nome', $empresa->nome));

					// echo form_label('CNPJ:');
					// echo form_input(array('name'=>'cnpj', 'class'=>'form-control', 'disabled'=>'disabled'), set_value('cnpj', $empresa->cnpj));

					echo form_label('Website:');
					echo form_input(array('name'=>'site', 'class'=>'form-control'), set_value('site', $empresa->site));

					echo form_label('E-mail:');
					echo form_input(array('name'=>'email', 'class'=>'form-control'), set_value('email', $empresa->email));

					echo form_label('Endereço:');
					echo form_input(array('name'=>'endereco', 'class'=>'form-control'), set_value('endereco', $empresa->endereco));

					echo form_label('Bairro:');
					echo form_input(array('name'=>'bairro', 'class'=>'form-control'), set_value('bairro', $empresa->bairro));

					echo form_label('Cidade:');
			        echo "<select name='cidade' class='form-control' id='selectCidade'><option value=''>Selecione...</option>";
			        foreach($cidades as $cidade){
						if($cidade->id == $empresa->idCidade)
								$selected = 'selected';
							else
								$selected = '';
						echo "<option $selected value='".$cidade->id."'>".$cidade->nome."</option>";
					}
			        echo "</select>";

					echo form_label('Estado:');
			        echo "<select name='estado' class='form-control' id='selectEstado'><option value=''>Selecione...</option>";
			        foreach($estados as $estado){
						if($estado->id == $empresa->idEstado)
								$selected = 'selected';
							else
								$selected = '';
						echo "<option $selected value='".$estado->id."'>".$estado->nome."</option>";
					}
			        echo "</select>";

			        echo form_label('CEP:');
					echo form_input(array('name'=>'cep', 'class'=>'form-control', 'id'=>'cep'), set_value('cep', $empresa->cep));

			        echo form_label('Texto de apresentação:');
			        echo "<textarea name='textempresa' class='form-control'>".$empresa->textempresa."</textarea>";

			        echo form_label('Foto:');
					echo "<div class='input-group'>
			                <span class='input-group-btn'>
			                    <span class='btn btn-primary btn-file'>
			                        Enviar foto <input type='file' name='foto'>
			                    </span>
			                </span>
			                <input type='text' class='form-control' readonly>
			            </div>";

					echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary pull-right', 'value'=>'Atualizar', 'style'=>'margin: 10px 0 0 0'));

					echo form_close();
				?>
			</div>
			<script>
				$(document).on('change', '.btn-file :file', function() {
				  var input = $(this),
				      numFiles = input.get(0).files ? input.get(0).files.length : 1,
				      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				  input.trigger('fileselect', [numFiles, label]);
				});

				$(document).ready( function() {
				    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
				        
				        var input = $(this).parents('.input-group').find(':text'),
				            log = numFiles > 1 ? numFiles + ' files selected' : label;
				        
				        if( input.length ) {
				            input.val(log);
				        } else {
				            if( log ) alert(log);
				        }
				        
				    });
				});
			</script>
		</div>
		
	</body>
</html>