<html>
	<head>
		<title>Pesquisa - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Faça uma busca personalizada das pessoas de sua região que estão em busca de um emprego." />
		<style>
		.result{
			overflow: hidden;
		}
		</style>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<form style='overflow: hidden;' class='col-sm-12 form-inline' name='form_pesquisa' action='/pesquisa' method='post' id='formulario'>
					<div class="col-sm-12">
						<div class='form-group col-sm-6' style='overflow: hidden;'>
							<input style='width: 100%;' class='form-control col-xs-12' id='pesquisa' name='campo_pesquisa_filtro' placeholder='Pesquisa' value='<?php echo set_value("campo_pesquisa_filtro") ?>' />
						</div>
						<div class="form-group col-sm-6">
							<select name='cidade' class="form-control col-sm-12" style='width: 100%;' id='cidade'>
								<option value=''>Selecione a cidade</option>
								<?php
									if($cidade != NULL){
										echo "<option value='$cidade->id' selected='selected'>$cidade->nome</option>";
									}
								?>
							</select>
						</div>
					</div>
					<center>
						<div class="form-group">
							<label class="radio-inline" for='vagas'>
							  <input type="radio" name="tipo" id="vagas" value="vagas" <?php echo set_radio('tipo', 'vagas', true); ?>> Vagas
							</label>
							<label class="radio-inline" for='usuarios'>
							  <input type="radio" name="tipo" id="usuarios" value="usuarios" <?php echo set_radio('tipo', 'usuarios'); ?>> Currículos
							</label>
							<label class="radio-inline" for='empresas'>
							  <input type="radio" name="tipo" id="empresas" value="empresas" <?php echo set_radio('tipo', 'empresas'); ?>> Empresas
							</label>
						</div>
					</center>
					<div class="form-group col-xs-12">
						<button class="btn btn-default col-xs-6 col-xs-offset-3">Pesquisar</button>
					</div>
				</form>
				<hr style='clear: both;' />
				<div class='col-sm-12'>
					<?php
						$cidade = (isset($cidade->nome) ? "em $cidade->nome" : "");
						echo ($tipo == "usuarios" ? "<h4>Currículos $cidade</h4>" : "<h4>".ucfirst($tipo)." ".$cidade)."</h4>";
						$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@', '.' );

						$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-', '-' );
						echo (sizeof($resultados) == 0 ? "<h5>Nenhum resultado encontrado.</h5>" : "");
						switch ($tipo){
							case 'vagas':
								foreach($resultados as $vaga){
									if($vaga->especial == 1) {
										$uri = str_replace($what, $by, $vaga->titulo_especial);
										if($vaga->imagem_especial != NULL && $vaga->imagem_especial != "")
											$imagem = "/img/thumbs/vagas/$vaga->imagem_especial";
										else
											$imagem = "/img/img_padrao_vaga.png";
										echo "<div class='col-xs-12 col-sm-6 item-lista' style='margin: 1% 0%; border-bottom: 2px solid #D9D9F3; overflow: hidden; padding-bottom: 2%'>
											<a href='/Vaga/ver/$vaga->id/$uri'>
										      <img class='col-xs-6 col-sm-4 img-thumbnail pull-left' src='$imagem' alt='Vaga de $vaga->titulo_especial.'>
										      <div class='col-xs-6 col-sm-8 pull-left'>
											      <h4 style='float: left;'>$vaga->titulo_especial</h4>
											      <h5 style='clear: left;'>$vaga->empresa em $vaga->cidade_especial_nome, $vaga->uf_estado_especial</h5>
											  </div>
										    </a></div>";
									} else {
										$uri = str_replace($what, $by, $vaga->cargo);
										if($vaga->imagem != NULL && $vaga->imagem != "")
											$imagem = "$vaga->imagem";
										else
											$imagem = "/img/thumbs/vagas/img_padrao_vaga.png";
										echo "<div class='col-xs-12 col-sm-6 item-lista' style='margin: 1% 0%; border-bottom: 2px solid #D9D9F3; overflow: hidden; padding-bottom: 2%'>
											<a href='/Vaga/ver/$vaga->id/$uri'>
										      <img class='col-xs-6 col-sm-4 img-thumbnail pull-left' src='$imagem' alt='Vaga de $vaga->empresa.'>
										      <div class='col-xs-6 col-sm-8 pull-left'>
											      <h4 style='float: left;'>$vaga->cargo</h4>
											      <h5 style='clear: left;'>$vaga->nome_empresa em $vaga->cidade, $vaga->uf</h5>
											  </div>
										    </a></div>";
									}
								}
								break;
							case 'usuarios':
								foreach($resultados as $curriculo){
									$nome = $curriculo->nome;
									$nome .= " ".$curriculo->sobrenome;

									    
									$uri = str_replace($what, $by, $nome);

									echo "<div class='col-xs-12 col-sm-6 item-lista'>
									<a href='/curriculo/ver/$curriculo->id/$uri'>
								      <img class='img-thumbnail col-xs-6 col-sm-4' alt='Ver currículo de $curriculo->nome $curriculo->sobrenome' src='$curriculo->foto' alt='Visualizar perfil de $curriculo->nome'>
								      <div class='col-xs-6 col-sm-8 pull-left'>
									      <h4 style='float: left;'>$curriculo->nome $curriculo->sobrenome</h4>
									      <h5 style='clear: left;'>em $curriculo->nome_cidade, $curriculo->uf</h5>
									  </div>
								    </a></div>";
								}
								break;
							case 'empresas':
								foreach($resultados as $empresa){
									$uri = str_replace($what, $by, $empresa->nome);

									$lugar = "";

									if($empresa->nome_cidade != NULL && $empresa->nome_cidade != ""){
										$lugar.=$empresa->nome_cidade;
									}

									if($empresa->uf != NULL && $empresa->uf != ""){
										$lugar.= ", ".$empresa->uf;
									}

									echo "<div class='col-xs-12 col-sm-6 item-lista'>
									<a href='".base_url()."empresa/ver/$empresa->id/$uri'>
								      <img class='img-thumbnail col-xs-6 col-sm-4 pull-left' src='$empresa->imagem' alt='Visualizar perfil de $empresa->nome'>
								      <div class='col-xs-6 col-sm-8 pull-left'>
									      <h4 style='float: left;'>$empresa->nome</h4>
									      <h5 style='clear: left;'>em $lugar</h5>
									  </div>
								    </a></div>";
								}
								break;
							default:
								echo "<center><h3>Faça sua pesquisa</h3></center>";
						}
						echo $paginacao;
					?>
				</div>
			</div>
		</div>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2_bootstrap.min.css">
		<script type="text/javascript" src='/js/select2/dist/js/select2.full.min.js'></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".num_pag, .proxima, .ultima, .primeira, .anterior").click(function(){
				    pag = $(this).children().attr("data-ci-pagination-page");
				    $("#formulario").attr("action","/pesquisa/"+pag);
				    $("#formulario").submit();
				    return false;
				});

				// $("#cidade").select2();
				$("#cidade").select2({
					theme: "bootstrap",
			        minimumInputLength: 3,
			        minimumResultsForSearch: 0,
					language: {
						inputTooShort: function(args) {
						  // args.minimum is the minimum required length
						  // args.input is the user-typed text
						  return "Digite mais...";
						},
						inputTooLong: function(args) {
						  // args.maximum is the maximum allowed length
						  // args.input is the user-typed text
						  return "Excesso de caracteres";
						},
						errorLoading: function() {
						  return "Carregando cidades...";
						},
						loadingMore: function() {
						  return "Carregando mais cidades";
						},
						noResults: function() {
						  return "Nenhuma cidade encontrada";
						},
						searching: function() {
						  return "Procurando cidades...";
						},
						maximumSelected: function(args) {
						  // args.maximum is the maximum number of items the user may select
						  return "Erro ao carregar resultados";
						}
					},
			        ajax: {
			            url: "/pesquisa/lista_cidades",
			            dataType: "json",
			            type: "post",
			            data: function (params) {

			                var queryParameters = {
			                    term: params.term
			                }
			                return queryParameters;
			            },
			            processResults: function (data) {
			                return {
			                    results: $.map(data, function (item) {
			                        return {
			                            text: item.nome+" - "+item.estado,
			                            id: item.id
			                        }
			                    })
			                };
			            }
			        }
				});
			});
		</script>
		
	</body>
</html>