<html>
	<head>
		<title>VAGAS - Enviar Currículo - Veja as vagas criadas pelas empresas de sua região</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Veja as vagas de emprego e ache seu emprego perfeito! Veja vagas de emprego em sua cidade e em sua áre de atuação e saia do desemprego." />
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<center><h2>Vagas de emprego</h2></center>
			<div class='panel-body'>
				<?php
					// $i = 0;
					foreach($vagas as $vaga){

						$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@' );

						$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
						    
						$uri = str_replace($what, $by, $vaga->cargo);

						if($vaga->especial){
						?>
							<div class="row col-sm-6 col-xs-12 item-lista">
								<a href="/Vaga/ver/<?php echo $vaga->id . "/" . $uri ?>">
									<!-- <div class="col-xs-6 col-md-3"> -->
										<img class="img-thumbnail col-xs-6 col-sm-4 pull-left" src="<?php echo ($vaga->imagem_especial != "" ? "/img/thumbs/vagas/$vaga->imagem_especial" : "/img/img_padrao_vaga.png") ?>" alt="<?php echo $vaga->titulo_especial ?>">
									<!-- </div> -->
									<div class="col-md-8">
										<h4><?php echo $vaga->titulo_especial ?></h4>
										<?php
											$contador = strlen($vaga->descricao_especial);
											$descricao = substr($vaga->descricao_especial, 0, strrpos(substr($vaga->descricao_especial, 0, 100), ' '));
										?>
										<h5><?php echo ($contador >= 200 ? $descricao."..." : $descricao) ?></h5>
									</div>
							  	</a>
							</div>
						<?php
						}else{
						?>
							<div class="row col-sm-6 col-xs-12 item-lista">
								<a href="<?php echo base_url(); ?>Vaga/ver/<?php echo $vaga->id . "/" . $uri ?>">
								  <!-- <div class="col-xs-6 col-md-2"> -->
								      <img src="<?php echo $vaga->imagem ?>" alt="<?php echo $vaga->cargo ?>" class="img-thumbnail col-xs-6 col-sm-4 pull-left">
								  <!-- </div> -->
								  <div class="col-md-8">
									  <h4><?php echo $vaga->cargo ?></h4>
										<?php
										$contador = strlen($vaga->descricao);
										$descricao = substr($vaga->descricao, 0, strrpos(substr($vaga->descricao, 0, 100), ' '));
										  if ( $contador >= 200 ) {      
										      $pontos = "...";
										  }
										  else{
										    $pontos = "";
										  }

										  ?><h5><?php echo $descricao.$pontos ?></h5>
									  </div>
								  
							  	</a>
							</div>
					<?php
						}
						// if($i%2 == 1)
						// 	echo "<hr />";
						// $i++;
					}
					echo $paginacao;
				?>
			</div>
		</div>
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
	</body>
</html>