<?php $pessoa = $pessoa[0]; ?>
<html>
	<head>
		<title><?php echo $pessoa->nome. " ".$pessoa->sobrenome ?> - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content='<?php echo "Visualize o perfil de $pessoa->nome $pessoa->sobrenome de $pessoa->cidade, $pessoa->uf em curriculoonline.net.br. Aproveite para fazer seu cadastro e procurar vagas de emprego em sua cidade!"?>' />
		<meta property="og:image" content="<?php echo$pessoa->foto?>"/>
		<meta property="og:title" content="<?php echo$pessoa->nome?>"/>
		<style>
			.quebra-separador{
			    color: #326C99;
			    font-size: 18px;
			    font-weight: normal;
			    margin-top: 17px !important;
			    padding-bottom: 0;
			    padding-top: 10px;
			    clear: both;
			}

			.separador{
			    border-bottom: 1px solid;
			    color: #A6C0D9;
			    display: inline-block;
			    padding: 2px;
			    width: 100%;
			}

			.separador-menor{
				border-bottom: 1px solid;
			    color: #A6C0D9;
			    display: inline-block;
			    padding: 2px;
			    width: 80%;
			    margin: 10px 10% 0 10%;
			}

			.azul{
				color: #069;
			}

			.panel-curriculo{
				padding: 20px 4% 15px 4%;
			}

			#curriculo{
			    padding: 10px 20px;
				box-shadow: 1px 1px 1px 2px #AAA;
			}

			#curriculo img{
				border: 1px solid #DDD;
			}

			#curriculo h1{
				font-size: 19px;
			}

			#curriculo h4{
				font-size: 13px;
			}
		</style>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12 panel-curriculo'>
			<div id='curriculo'>
				<h1 class="azul"><?php echo $pessoa->nome." ".$pessoa->sobrenome ?></h1>
				<h3 class="quebra-separador">Serviços que realizo</h3>
				<?php foreach($servicos as $servico){ ?>
				<h4 class="azul"><?php echo $servico->titulo_servico ?></h4>
				<?php } ?>
			</div><!-- /#curriculo -->

			

			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-553d8a231f4f63dc" async="async"></script>
			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<div class="addthis_sharing_toolbox" style="float: right; clear: both;width:200px;"></div>
		</div>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>