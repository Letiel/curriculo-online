<?php defined('BASEPATH') OR exit('No direct script access allowed');
$pessoa = $pessoa[0];
?>
<html>
	<head>
		<title>Editar seu Curriculo - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<!-- <script src="/js/jquery-ui.min.js"></script>
		<script src='/js/jquery.picture.cut.js'></script> -->

		<!-- CSS -->
		<link rel="stylesheet" href="/js/imagepicker/assets/css/bootstrap.css">
		<link rel="stylesheet" href="/js/imagepicker/assets/css/imgpicker.css">

		<!-- JavaScript -->
		<!-- // <script src="/js/imagepicker/assets/js/jquery-1.11.0.min.js"></script> -->
		<script src="/js/imagepicker/assets/js/jquery.Jcrop.min.js"></script>
		<script src="/js/imagepicker/assets/js/jquery.imgpicker.js"></script>

		<style>
			.select2-dropdown {
			  z-index: 9001 !important;
			}
			.separa{
				float: left;
				margin-left: 2%;
				font-weight: bold;
				border-left: 1px solid #CCC;
				padding-left: 1%;
			}

			.aumenta{
				overflow: hidden;
  				height: auto;
			}

			.quebra-separador{
			    color: #326C99;
			    font-size: 18px;
			    font-weight: normal;
			    margin-top: 20px !important;
			    padding-bottom: 0;
			    padding-top: 0px;
			    clear: both;
			    margin: 0;
			}

			.separador{
			    border-bottom: 1px solid;
			    color: #A6C0D9;
			    display: inline-block;
			    padding: 2px;
			    width: 100%;
			    margin-bottom: 15px;
			}

			.separador-menor{
				border-bottom: 1px solid;
			    color: #A6C0D9;
			    display: inline-block;
			    padding: 2px;
			    width: 80%;
			    margin: 10px 10% 0 10%;
			}

			.azul{
				color: #069;
			}

			.panel-curriculo{
				padding: 20px 4% 15px 4%;
			}

			#curriculo{
			    padding: 10px 20px;
				box-shadow: 1px 1px 1px 2px #AAA;
			}

			#curriculo img{
				width: 100%;
			}

			#curriculo h1{
				font-size: 19px;
			}

			#curriculo h4{
				font-size: 13px;
			}

			.adicionar{
				background: none;
			    width: 10%;
			    font-size: 25px;
			    border: none;
			    border-radius: 2px;
			    color: #76B3FF;
			    text-shadow: 0px 0px 5px #76B3FF;
			    outline: none;
			}

			#container_image{
				background-size: 100%;
			}

			.ui-front{
				top: 0px !important;
			}

			.select2-container--bootstrap{
				width: 100% !important;
			}
		</style>

	</head>
	<body>

		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Meu Curr&iacute;culo</h2></center>
				<p class="text-info"><b>Dica!</b> Você pode editar seu perfil clicando sobre o item que deseja alterar!</p>
				<div id='curriculo'>
				<?php
					$date = new DateTime( $pessoa->dt_nascimento );
					$interval = $date->diff( new DateTime() );

					$idade = $interval->format( '%Y' );
					?>
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-5" style='max-width: 200px; margin: 0px 0px 10px 0px'>				
						<div id='avatar-container'>
							<img src='<?php echo $pessoa->foto ?>' id='avatar' />
							<button type='button' class='btn btn-default edit-avatar btn-block btn-sm' data-ip-modal='#avatarModal' title='Editar imagem'>Trocar</button>
						</div>
					</div>
					<h1 class='azul'>
						<a href='#' class='editable' data-type='text' data-pk='<?php echo $pessoa->id ?>' data-title='Nome' data-name='nome'>
							<?php echo $pessoa->nome ?>
						</a>
						<a href='#' id='sobrenome' class='editable' data-type='text' data-pk='".$pessoa->id."' data-title='Sobrenome'>
							<?php echo $pessoa->sobrenome ?>
						</a>
					</h1>
					
					<h4 class='azul'>
						<strong>Sexo: </strong>
						<a href='#' id='sexo' data-name='sexo' data-type='select' data-pk='<?php echo $pessoa->id ?>' data-title='Selecione o sexo'>
							<?php echo $pessoa->sexo ?>
						</a>
					</h4>

					<h4 class='azul'>
						<strong>Idade: </strong>
						<?php echo $idade ?> anos [<a href='#' id='dt_nascimento' data-name='dt_nascimento' data-type='date' data-pk='<?php echo $pessoa->id ?>' data-title='Data de nascimento'><?php echo date("d/m/Y", strtotime($pessoa->dt_nascimento)) ?></a>]
					</h4>
					
					<h4 class='azul'>
						<strong>Escolaridade: </strong>
						<a href='#' id='escolaridade' data-name='escolaridade' data-type='select' data-pk='<?php echo $pessoa->id ?>' data-title='Escolaridade'>
							<?php echo $pessoa->escolaridade ?>
						</a>
					</h4>

					<h4 class='azul'>
						<strong>Estado Civil: </strong>
						<a href='#' id='estado_civil' data-name='estado_civil' data-type='select' data-pk='<?php echo $pessoa->id ?>' data-title='Estado Civil'>
							<?php echo $pessoa->estado_civil ?>
						</a>
					</h4>

					<h4 class='azul'>
						<strong>Filhos: </strong>
						<a href='#' class='editable' id='filhos' data-type='text' data-pk='<?php echo $pessoa->id ?>' data-title='Filhos' data-name='filhos'>
						<?php echo $pessoa->filhos ?>
						</a>
					
					<h3 class='quebra-separador'>Endereço</h3>
					
					<div class='separador'></div>
					
					<h4 class='azul'>
						<strong>Endereço: </strong>
						<a href='#' class='editable' id='endereco' data-type='text' data-pk='<?php echo $pessoa->id ?>' data-title='Endereço' data-name='endereco'>
						<?php echo $pessoa->endereco ?>
						</a>
					</h4>
					
					
					
					<h4 class='azul'>
						<strong>Bairro: </strong>
						<a href='#' class='editable' data-type='text' id='bairro' data-pk='<?php echo $pessoa->id ?>' data-title='Bairro' data-name='bairro'>
						<?php echo $pessoa->bairro ?>
						</a>
					</h4>

					<h4 class='azul'>
						<strong>Complemento: </strong>
						<a href='#' class='editable' data-type='text' id='complemento' data-pk='<?php echo $pessoa->id ?>' data-title='Complemento' data-name='complemento'>
							<?php echo $pessoa->complemento ?>
						</a>
					</h4>

					<h4 class='azul'>
						<strong>Cidade: </strong>
						<a href='#' id='cidade' data-name='cidade' data-pk='<?php echo $pessoa->id ?>' data-title='Cidade' data-value='<?php echo $pessoa->idCidade ?>'>
							<?php echo $pessoa->cidade." - ".$pessoa->uf ?>
						</a>
					</h4>

					<h4 class='azul'>
						<strong>CEP: </strong>
						<a href='#' class='editable-cep' id='cep' data-type='text' data-pk='<?php echo $pessoa->id ?>' data-title='CEP' data-name='cep'>
							<?php echo $pessoa->cep ?>
						</a>
					</h4>
										
					<h4 class='azul'>
						<strong>Telefone: </strong>
						<a href='#' id='campo_telefone' data-type='text' data-pk='<?php echo $pessoa->id ?>' data-title='Telefone' data-name='telefone'>
							<?php echo $pessoa->telefone ?>
						</a>
					</h4>
					
					<h4 class='azul'>
						<strong>Celular: </strong>
						<a href='#' id='campo_celular' data-type='text' data-pk='<?php echo $pessoa->id ?>' data-title='Celular' data-name='celular'>
							<?php echo $pessoa->celular ?>
						</a>
					</h4>

					<!-- <h4 class='azul'>
						<strong>Facebook: </strong>
						<a href='".$pessoa->link_facebook."' target='_blank' class='link' >
							<?php echo $pessoa->link_facebook ?>
						</a><br/><br/>
					</h4> -->

					<h3 class='quebra-separador'>
						Cursos <button type='button' title='Adicionar novo curso' class='btn btn-info pull-right' data-toggle='modal' data-target='#modal_add_cursos'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span></button>
					</h3>
					<div class='separador'></div>
					<div id='result_cursos'></div>

					<div id='lista_cursos'>
					<?php 
					foreach($cursos as $curso){
						?>
						<h4 class='azul item'><?php echo $curso->nome." - ".$curso->instituicao ?> </h4>
						<?php
					}
					?>
					</div>

					<h3 class='quebra-separador'>
						Áreas de atuação <button type='button' title='Adicionar nova área' class='btn btn-info pull-right' data-toggle='modal' data-target='#modal_add_area'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span></button>
					</h3>
					<div class='separador'></div>
					<div id='result_area'></div>

					<div id='lista_areas'>
					<?php 
					foreach($areas_usuario as $area){
					?>
						<h4 class="azul item"><?php echo $area->descricao ?></h4>
					<?php
					}
					?>
					</div>

					<h3 class='quebra-separador'>
						Idioma(s) <button type='button' title='Adicionar novo idioma' class='btn btn-info pull-right' data-toggle='modal' data-target='#modal_add_idiomas'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span></button>
					</h3>
					<div class='separador'></div>
					<div id='lista_idiomas'>
						<?php
						foreach($idiomas as $idioma){
							?>
							<h4 class='azul'><?php echo $idioma->nome ?></h4>
							<?php
						}
						?>
					</div>

					<h3 class='quebra-separador'>
						Experiências <button type='button' title='Adicionar nova experiência' class='btn btn-info pull-right' data-toggle='modal' data-target='#modal_experiencias'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span></button>
					</h3>
					<div class='separador'></div>
					<div id='lista_experiencias'>
						<?php
						$i = 0;
						foreach($exp as $experiencia){
							if($i > 0)
								echo "<div class='separador-menor'></div>";
							$inicio = date("d/m/Y", strtotime($experiencia->inicio));
							if($experiencia->final != '0000-00-00')
								$final = date("d/m/Y", strtotime($experiencia->final));
							else
								$final = "agora";
						?>
							<h4 class='azul'><strong>Empresa: </strong><?php echo $experiencia->empresa ?></h4>
							<h4 class='azul'><strong>Função: </strong><?php echo $experiencia->funcao ?></h4>
							<h4 class='azul'><strong>Descrição: </strong><?php echo $experiencia->descricao ?></h4>
							<h4 class='azul'><strong>Período: </strong> De <?php echo $inicio." até ".$final ?></h4>
							<?php
							$i++;
						}?>
					</div>

					<h3 class='quebra-separador'>
						Serviços Autônomos <button type='button' title='Adicionar serviço' class='btn btn-info pull-right' data-toggle='modal' data-target='#modal_autonomo'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span></button>
					</h3>
					<div class='separador'></div>
					<div id='lista_servicos'>
						<?php
						$i = 0;
						foreach($servicos as $servico){
							if($i > 0)
								echo "<div class='separador-menor'></div>";
							?>
							<h4 class='azul'>
								<strong>
									<a href='#' data-pk='<?php echo $servico->id ?>' data-name='titulo_servico' class='editable-autonomo'>
										<?php echo $servico->titulo_servico ?>
									</a>
								</strong>
							</h4>
							<h4 class='azul'>
								<a href='#' data-pk='<?php echo $servico->id ?>' data-name='descricao_servico' data-type='textarea' class='editable-autonomo'>
									<?php echo $servico->descricao_servico ?>
								</a>
							</h4>
							<h4 class='azul'>
								<strong>Contato: </strong>
								<a href='#' data-pk='<?php echo $servico->id ?>' data-name='contato' class='editable-autonomo'>
									<?php echo $servico->contato ?>
								</a>
							</h4>
							<h4 class='azul'>
								<strong>Lugares onde atende: </strong>
								<a href='#' data-pk='<?php echo $servico->id ?>' data-name='lugares' class='editable-autonomo'>
									<?php echo $servico->lugares ?>
								</a>
							</h4>
							<h4 class='azul'>
								<strong>Trabalha desde: </strong>
								<a href='#' data-pk='<?php echo $servico->id ?>' data-name='inicio' class='editable-autonomo-data'>
									<?php echo date("d/m/Y", strtotime($servico->inicio)) ?>
								</a>
							</h4>
							<?php
							$i++;
						}
						?>
					</div>


					<h3 class='quebra-separador'>Outros</h3>
					<div class='separador'></div>
					<h4 class='azul'>
						<strong>Disponibilidade para Viajar: </strong>
						<a href='#' id='viajar' data-url='/Curriculo/editable' data-type='select' data-name='disp_viajar' data-pk='".$pessoa->id."' data-title='Disponibilidade para viajar'></a>
					</h4>

					<h4 class='azul'>
						<strong>Deficiente: </strong>
						<a href='#' id='deficiente' data-url='/Curriculo/editable' data-type='select' data-name='deficiente' data-pk='".$pessoa->id."' data-title='Deficiente'></a>
					</h4>

					<h4 class='azul'>
						<strong>Veículos próprios: </strong>
						<a href="#" id="veiculos" data-type="checklist" data-url='/Curriculo/editable_veiculos' data-pk="<?php echo $pessoa->id; ?>" data-title="Veículos"></a>
					</h4>
						
					
					<h4 class='azul'>
						<strong>Carteira de habilitação:</strong>
						<a href="#" id="habilitacao" data-type="checklist" data-url='/Curriculo/editable_habilitacao' data-pk="<?php echo $pessoa->id; ?>" data-title="Habilitação"></a>
					</h4>
					<h3 class="quebra-separador">Informações adicionais <small>Estas informações não aparecerão para ninguém além de você.</small></h3>
					<div class="separador"></div>
					<?php
						if($pessoa->recebe_email_massa_cidade) $recebe_mensagens = "Receber"; else $recebe_mensagens = "Não receber";
					?>
					<h4 class='azul'>
						<strong>Receber e-mails em massa? </strong>
						<a href='#' id='recebe_email_massa_cidade' data-name='recebe_email_massa_cidade' data-type='select' data-pk='<?php echo $pessoa->id ?>' data-title='Receber e-mails em massa?'>
							<?php echo $recebe_mensagens ?>
						</a>
					</h4>
			</div><!-- /#curriculo -->
		
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>

		<!-- Modal para curso não cadastrado -->
		<div id="modal_curso" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Cadastrar novo curso</h4>
		            </div>
		            <div class="modal-body">
		                <div class='form-group'>
		            	<?php echo form_label('Nome do curso'); ?>
		                <input type='text' id='nome_novo_curso' placeholder='Nome do curso...' class='form-control' />
						</div>
		                <div class='form-group'>
		                <?php echo form_label('Instituição'); ?>
		                <?php echo form_input(array('id'=>'instituicao_modal', 'class'=>'form-control', 'placeholder'=>'Instituição...'));
						echo '</div>';
		                echo "<div class='form-group'>";
						echo "<input style='margin-right: 3px;' type='checkbox' id='curso_concluido_modal' />";
		                echo "<label>Concluído</label>";
						echo '</div>';
		                ?>
		            </div>
		            <div class="modal-footer">
		                <button type="button" id='botao_cadastrar_curso' class="btn btn-primary" onclick='return false'>Cadastrar</button>
		            </div>
		        </div>
		    </div>
		</div>

		<!-- Modal para add cursos -->
		<div class="modal fade" id="modal_add_cursos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Cadastrar Curso</h4>
		      </div>
		      <div class="modal-body">
		        <div class='form-group'>
		        	<label for="recipient-name" class="control-label">Selecione um Curso:</label>
            		<select class='form-control' name='curso' id='id_curso'></select>
		        </div>
		        <div class='form-group'>
		        	<label for="recipient-name" class="control-label">Instituição</label>
            		<input type="text" class='form-control' id='instituicao'>
		        </div>
		        <div class='form-group'>
		        	<input type="checkbox" id="curso_concluido">
		        	<label> Concluído</label>
		        </div>
		      </div>
		      <div class="modal-footer">
		      	<a href="#" data-toggle='modal' data-dismiss="modal" data-target='#modal_curso'>Seu curso não está na lista?</a>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-success" id="adicionarCurso" onclick="return false">Adicionar Curso</button>
		      </div>
		    </div>
		  </div>
		</div>

		<!-- Modal para area não cadastrada -->
		<div id="modal_nova_area" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Cadastrar nova área</h4>
		            </div>
		            <div class="modal-body">
		                <div class='form-group'>
			            	<label>Nome da área</label>
			                <input type='text' id='nome_nova_area' placeholder='Nome da área...' class='form-control' />
						</div>
					</div>
		            <div class="modal-footer">
		                <button type="button" id='botao_cadastrar_area' class="btn btn-primary">Cadastrar</button>
		            </div>
		        </div>
		    </div>
	    </div>

		<!-- Modal para add areas -->
		<div class="modal fade" id="modal_add_area" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Cadastrar Área</h4>
		      </div>
		      <div class="modal-body">
		        <div class='form-group'>
		        	<label for="recipient-name" class="control-label">Selecione uma área:</label>
            		<select class='form-control' name='curso' id='id_area'>
            			<option value=''>Selecione</option>
            			<?php 
            			foreach($all_areas as $area) {
            				echo "<option value='$area->id'>$area->descricao</option>";
            			}
            			?>
            		</select>
		        </div>
		      </div>
		      <div class="modal-footer">
		      	<a href="#" data-toggle='modal' data-dismiss="modal" data-target='#modal_nova_area'>Sua área não está na lista?</a>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-success" id="cadastrarArea">Adicionar Área</button>
		      </div>
		    </div>
		  </div>
		</div>

		<!-- Modal para add idiomas -->
		<div class="modal fade" id="modal_add_idiomas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Adicionar idioma</h4>
		      </div>
		      <div class="modal-body">
		        <div class='form-group'>
		        	<select class='form-control' id='select_idiomas'></select>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-success" id="botao_cadastrar_idioma" onclick="return false">Adicionar idioma</button>
		      </div>
		    </div>
		  </div>
		</div>

		<!-- Modal para add Experiências -->
		<div id="modal_experiencias" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Adicionar experiência ao currículo</h4>
		            </div>
		            <div class="modal-body">
		            	<div class='form-group'>
			            	<?php echo form_label('Empresa'); ?>
			            	<input type='text' id='empresa' class='form-control' placeholder='Nome da empresa...' />
		            	</div>
		            	<div class='form-group'>
	            			<?php echo form_label('Cidade'); ?>
			            	<select id='exp_cidade' class='form-control col-md-12'></select>
			            </div>
			            <div class='form-group'>
	            			<?php echo form_label('Endereço'); ?>
			            	<input type='text' id='exp_endereco' class='form-control' placeholder='Endereço da empresa' />
			            </div>
			            <div class='form-group'>
	            			<?php echo form_label('Telefone'); ?>
			            	<input type='text' id='exp_telefone' class='form-control' placeholder='Telefone da empresa' />
			            </div>
		            	<div class='form-group'>
	            			<?php echo form_label('Função'); ?>
			            	<input type='text' id='exp_funcao' class='form-control' placeholder='Descreva a função...' />
			            </div>
			            <div class='form-group'>
			            	<?php echo form_label('Início'); ?>
			            	<input type='text' id='exp_inicio' class='form-control' />
			            </div>
			            <div class='form-group'>
			            	<?php echo form_label('Final'); ?>
			            	<input type='text' id='exp_final' class='form-control' placeholder='Caso ainda esteja na função deixe em branco'/>
			            </div>
			            <div class='form-group'>
			            	<?php echo form_label('Descrição'); ?>
			                <textarea id='exp_descricao' placeholder='Escreva uma pequena descrição...' class='form-control'></textarea>
			            </div>
		            </div>
		            <div class="modal-footer">
	                	<button type="button" id='botao_cadastrar_exp' class="btn btn-primary" onclick='return false'>Adicionar</button>
		            </div>
		        </div>
		    </div>
		</div>

			<!-- Modal para add Trabalhos Autônomos -->
			<div id="modal_autonomo" class="modal fade">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <h4 class="modal-title">Adicionar Serviço</h4>
			            </div>
			            <div class="modal-body">
			            	<div class='form-group'>
				            	<?php echo form_label('Título'); ?>
				            	<input type='text' id='titulo_servico' class='form-control' placeholder='O que você faz?' />
			            	</div>
			            	<div class='form-group'>
		            			<?php echo form_label('Descrição'); ?>
		            			<textarea class='form-control' id='descricao_servico' name='descricao_servico' placeholder='Descreva seu trabalho aqui.'></textarea>				            	
				            </div>
				            <div class='form-group'>
		            			<?php echo form_label('Lugares onde atende'); ?>
				            	<input type='text' id='lugares' class='form-control' placeholder='Cidade, região, etc' />
				            </div>
				            <div class='form-group'>
		            			<?php echo form_label('Contato'); ?>
				            	<input type='text' id='contato' class='form-control' placeholder='E-mail, telefone, etc.' />
				            </div>
				            <div class='form-group'>
		            			<?php echo form_label('Início'); ?>
				            	<input type='text' id='inicio' class='form-control' placeholder='Informe desde quando você faz isso.' />
				            </div>
			            </div>
			            <div class="modal-footer">
		                	<button type="button" id='botao_cadastrar_servico' class="btn btn-primary" onclick='return false'>Adicionar</button>
			            </div>
			        </div>
			    </div>
			</div>



		<!-- Modal -->
		<div class="ip-modal" id="avatarModal">
		  <div class="ip-modal-dialog">
		    <div class="ip-modal-content">
		      <div class="ip-modal-header">
		        <a class="ip-close" title="Close">&times;</a>
		        <h4 class="ip-modal-title">Alterar imagem</h4>
		      </div>
		      <div class="ip-modal-body">
		        <div class="btn btn-primary ip-upload">
		            Carregar Foto <input type="file" name="file" class="ip-file">
		        </div>
		        <!-- <button type="button" class="btn btn-primary ip-webcam">Webcam</button> -->
		        <!-- <button type="button" class="btn btn-info ip-edit">Edit</button> -->
		        <!-- <button type="button" class="btn btn-danger ip-delete">Delete</button> -->
		        <div class="alert ip-alert"></div>
		        <div class="ip-info">
		        	Para recortar a imagem, arraste o mouse na região abaixo depois clique em "Salvar"
		        </div>
		          <div class="ip-preview"></div>
		        <div class="ip-rotate">
		          <button type="button" class="btn btn-default ip-rotate-ccw">
		            <i class="icon-ccw"></i>
		          </button>
		          <button type="button" class="btn btn-default ip-rotate-cw">
		            <i class="icon-cw"></i>
		          </button>
		        </div>
		        <div class="ip-progress">
		          <div class="text">Enviando</div>
		          <div class="progress progress-striped active">
		            <div class="progress-bar"></div>
		          </div>
		        </div>
		      </div>
		      <div class="ip-modal-footer">
		        <div class="ip-actions">
		          <button type="button" class="btn btn-success ip-save">Salvar</button>
		          <button type="button" class="btn btn-primary ip-capture">Capturar</button>
		          <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
		        </div>
		        <button type="button" class="btn btn-default ip-close">Fechar</button>
		      </div>
		    </div>
		  </div>
		</div>

		<script src="/js/mascara.js"></script>
		<!-- X-Editable -->
		<link href="/css/bootstrap-editable.css" rel="stylesheet">
		<script src="/js/bootstrap-editable.js"></script>

		<!-- SELECT2 -->
		<script type="text/javascript" src='/js/select2/dist/js/select2.min.js'></script>
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2.min.css">
		<link rel="stylesheet" type="text/css" href="/js/select2/dist/css/select2_bootstrap.min.css">
		<script type="text/javascript" src='/js/select2/dist/js/select2.full.min.js'></script>

		<script type="text/javascript" src='/js/editar-curriculo.js'></script>
		<script type='text/javascript'>
			$(document).ready(function(){
				$('#sexo').editable({
			        value: "<?php echo $pessoa->sexo ?>",
			        source: [
			        	{value: "Masculino", text: 'Masculino'},
			            {value: "Feminino", text: 'Feminino'},
			        ],
			        emptytext: 'Não informado',
			        validate: function(value) {
			            if($.trim(value) == '') return 'Campo requerido!';
			        },
			        ajaxOptions: {
			            dataType: 'json'
			        },
			        success: function(response, newValue) {
			            if(!response) {
			                return "Erro desconhecido";
			            }

			             if(response.success === false) {
			                return response.msg;
			            }
			        }
				});

				$('#escolaridade').editable({
			        value: "<?php echo $pessoa->idEscolaridade ?>",
			        source: [
			        	<?php 
			        		foreach($lista_escolaridades as $esc){
			        			echo "{value: '$esc->id', text: '$esc->descricao'},";
			        	}?>
			        ],
			        ajaxOptions: {
			            dataType: 'json'
			        },
			        emptytext: 'Não informado',
			        success: function(response, newValue) {
			            if(!response) {
			                return "Erro desconhecido";
			            }

			             if(response.success === false) {
			                return response.msg;
			            }
			        }
				});

				$('#estado_civil').editable({
			        value: "<?php echo $pessoa->Estado_civil ?>",
			        source: [
			        	<?php 
			        		foreach($lista_estados_civis as $estado){
			        			echo "{value: '$estado->id', text: '$estado->descricao'},";
			        	}?>
			        ],
			        ajaxOptions: {
			            dataType: 'json'
			        },
			        emptytext: 'Não informado',
			        success: function(response, newValue) {
			            if(!response) {
			                return "Erro desconhecido";
			            }

			             if(response.success === false) {
			                return response.msg;
			            }
			        }
				});

			    $('#viajar').editable({
			        value: [<?php echo $pessoa->disp_viajar ?>],
			        source: [
			              {value: 0, text: 'Não'},
			              {value: 1, text: 'Sim'},
			           ],
						emptytext: 'Não informado',
						validate: function(value) {
						    if($.trim(value) == '') return 'Campo requerido!';
						},
						ajaxOptions: {
						    dataType: 'json'
						},
						success: function(response, newValue) {
						    if(!response) {
						        return "Erro desconhecido";
						    }

						     if(response.success === false) {
						        return response.msg;
						    }
						}
			    });

			    $('#deficiente').editable({
			        value: [<?php echo $pessoa->deficiente ?>],
			        source: [
			              {value: 0, text: 'Não'},
			              {value: 1, text: 'Sim'},
			           ],
						emptytext: 'Não informado',
						validate: function(value) {
						    if($.trim(value) == '') return 'Campo requerido!';
						},
						ajaxOptions: {
						    dataType: 'json'
						},
						success: function(response, newValue) {
						    if(!response) {
						        return "Não foi possível realizar a edição. Estamos trabalhando para resolver o problema!";
						    }

						     if(response.success === false) {
						        return response.msg;
						    }
						}
			    });

			    $('#veiculos').editable({
			        value: [<?php
			        	if($pessoa->carro == 1)
			        		echo "'carro',";
			        	if($pessoa->moto == 1)
			        		echo "'moto',";
			        	if($pessoa->caminhao == 1)
			        		echo "'caminhao',";
			        	if($pessoa->outro_veiculo == 1)
			        		echo "'outro_veiculo',";
			        ?>],
			        emptytext: 'Não informado',
			        source: [
			          {value: 'carro', text: 'Carro'},
			          {value: 'moto', text: 'Moto'},
			          {value: 'caminhao', text: 'Caminhão'},
			          {value: 'outro_veiculo', text: 'Outro Veículo'}
			       	],
			       	ajaxOptions: {
					    dataType: 'json'
					},
					success: function(response, newValue) {
					    if(!response) {
					        return "Erro desconhecido";
					    }

					     if(response.success === false) {
					        return response.msg;
					    }
					},
					display: function(value, sourceData) {
					    //display checklist as comma-separated values
					    var html = [],
					      checked = $.fn.editableutils.itemsByValue(value, sourceData);

					    if (checked.length) {
					      $.each(checked, function(i, v) {
					        html.push($.fn.editableutils.escape(v.text));
					      });
					      $(this).html(html.join(', '));
					    } else {
					      $(this).empty();
					    }
					  }
			    });
				
			    $('#habilitacao').editable({
			        value: [<?php
			        	if($pessoa->hab_a == 1)
			        		echo "'hab_a',";
			        	if($pessoa->hab_b == 1)
			        		echo "'hab_b',";
			        	if($pessoa->hab_c == 1)
			        		echo "'hab_c',";
			        	if($pessoa->hab_d == 1)
			        		echo "'hab_d',";
			        	if($pessoa->hab_e == 1)
			        		echo "'hab_e',";
			        ?>],
			        emptytext: 'Não informado',
			        source: [
			          {value: 'hab_a', text: 'A'},
			          {value: 'hab_b', text: 'B'},
			          {value: 'hab_c', text: 'C'},
			          {value: 'hab_d', text: 'D'},
			          {value: 'hab_e', text: 'E'}
			       	],
			       	ajaxOptions: {
					    dataType: 'json'
					},
					success: function(response, newValue) {
					    if(!response) {
					        return "Erro desconhecido";
					    }

					     if(response.success === false) {
					        return response.msg;
					    }
					},
					display: function(value, sourceData) {
					    //display checklist as comma-separated values
					    var html = [],
					      checked = $.fn.editableutils.itemsByValue(value, sourceData);

					    if (checked.length) {
					      $.each(checked, function(i, v) {
					        html.push($.fn.editableutils.escape(v.text));
					      });
					      $(this).html(html.join(', '));
					    } else {
					      $(this).empty();
					    }
					  }
			    });

				$('#recebe_email_massa_cidade').editable({
				    value: "<?php echo $pessoa->recebe_email_massa_cidade ?>",
				    source: [
				    	{value: 1, text: 'Receber'},
				    	{value: 0, text: 'Não receber'}
				    ],
				    ajaxOptions: {
				        dataType: 'json'
				    },
				    emptytext: 'Não informado',
				    success: function(response, newValue) {
				        if(!response) {
				            return "Erro desconhecido";
				        }

				         if(response.success === false) {
				            return response.msg;
				        }
				    }
				});
			});
		</script>
	</body>
</html>