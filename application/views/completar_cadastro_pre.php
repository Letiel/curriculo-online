<html>
	<head>
		<title>Currículo Online - Cadastre sua empresa agora mesmo!</title>
		<?php $this->load->view('inc/headBasico.php')?>
		<meta name='description' content="Registre-se sua empresa no Currículo Online, receba muitos currículos e entre em contato com pessoas que estão procurando emprego, tudo totalmente grátis!"/>
		<script src="<?php echo base_url();?>js/mascara.js"></script>
	</head>
	<body style="min-width: 540px; max-width: 95%; margin: 0 auto;">
		<div class="row topo">
			<?php $this->load->view('inc/topo.php')?>
			<?php $this->load->view('inc/menuSuperior.php')?>
		</div>
		
		<script>
			jQuery(function($){
	   			$("#cnpj").mask("99.999.999/9999-99");
			});
		</script>
		
		<div class="panel panel-default col-md-8 col-sm-12 col-xs-12">
		<div class='panel-body'>
		
		<?php
			echo validation_errors('<div class="alert alert-danger">', '</div>');
			echo "<center><h2>Cadastrar Login</h2></center>";

			echo form_open();

			echo form_label('Nome de usuário:');
			echo form_input(array('name'=>'login', 'class'=>'form-control'), set_value('login'));
			
			echo form_label('Senha:');
			echo form_input(array('name'=>'senha', 'class'=>'form-control', 'type'=>'password'), set_value('senha'));
			
			echo form_label('Confirmar a senha:');
			echo form_input(array('name'=>'senha2', 'class'=>'form-control', 'type'=>'password'), set_value('senha2'));

			echo form_hidden('codigo', "$codigo");
			
			echo "<div class='form-group'>";
			echo form_submit(array('name'=>'cadastrar', 'class'=>'btn btn-primary pull-right', 'style'=>"margin-top: 1%;"), 'Cadastrar');
			echo "</div>";

			echo form_close();
		?>
		
				
			</div>
					

		</div>
		
		
		<?php $this->load->view('inc/anuncioDireita.php')?>
		<?php $this->load->view('inc/anuncioMeio.php')?>
				
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php')?>
		
	</body>
</html>