<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');?>
<html>
	<head>
		<title>Login - Currículo Online</title>
		<?php $this->load->view('inc/headBasico');?>

	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo');?>
			<?php $this->load->view('inc/menuSuperior');?>
		</div>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<form style='margin: 1%;' action='<?php echo base_url(); ?>Login' method="post">
			<?php echo validation_errors();
			if(isset($erro) && $erro == TRUE && $codigo_erro == 1){
				echo "<div class='alert alert-danger' style='text-align: center;'>Usuário ou senha incorretos, verifique seus dados!<br />Certifique-se de que confirmou seu e-mail!</div>";
			}
			echo $this->session->flashdata('confirmado');
			?>
			  <div class="form-group">
				<label for="inputLogin">Login ou E-mail</label>
				<input type="text" class="form-control" name='login' id="inputLogin" placeholder="Login ou E-mail">
			  </div>
			  <div class="form-group">
				<label for="inputSenha">Senha</label>
				<input type="password" name='senha' class="form-control" id="inputSenha" placeholder="********">
			  </div>
			  <div class="form-group">
				<button type="submit" class="btn btn-primary">Entrar</button>
			  </div>
		  </form>
		  	<div align="center"> <a href="<?php echo base_url(); ?>recuperar_senha" class="link" > Recuperar Senha </a> </div>	
		</div>
		
		<?php $this->load->view('inc/anuncioDireita');?>
		<?php $this->load->view('inc/anuncioMeio');?>
		
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo');?>
	</body>
</html>