<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
	<head>
		<title>Alterar senha - Enviar Currículo</title>
		<?php $this->load->view('inc/headBasico.php');?>
	</head>
	<body>


		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Configurações</h2></center>
				<?php
					echo $this->session->flashdata('sucesso');
				?>

				<div class="btn-group col-xs-12">
					<a href='#alterar_login' role="button" data-toggle="modal" class='btn btn-primary btn-block'>Alterar login</a>
					<a href='#alterar_senha' role="button" data-toggle="modal" class='btn btn-primary btn-block'>Alterar senha</a>
				</div>

				<!-- Button HTML (to Trigger Modal) -->
				<div class="row">
					<div class="btn-group pull-right" style='margin: 10px 0 0 0'>
						<a href="#excluir" role="button" class="link text-danger" data-toggle="modal">Excluir Conta</a>
					</div>
				</div>
				 
				<!-- Modal HTML -->
				<div id="excluir" class="modal fade">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                <h4 class="modal-title">Tem certeza que deseja excluir sua conta?</h4>
				            </div>
				            <div class="modal-body">
				                <p>Você perderá o acesso à sua conta.</p>
				                <p class="text-warning"><small>Para voltar a utilizar o sistema, será necessário criar uma nova conta!</small></p>
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				                <a href='<?php echo base_url(); ?>empresa/excluir_conta' class="btn btn-primary">Excluir Conta</a>
				            </div>
				        </div>
				    </div>
				</div>

				<div id="alterar_login" class="modal fade">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                <h4 class="modal-title">Alterar login</h4>
				                <div id="resultado_troca_login"></div>
				            </div>
				            <form id='form_alterar_login'>

					            <div class="modal-body">
					                <label>Login</label>
					                <input type='text' class='form-control' name='login' id='novo_login' placeholder='Informe o novo login' />
					            </div>
					            <div class="modal-footer">
					                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					                <button id='alterar_login' type="submit" class="btn btn-primary">Alterar</button>
					            </div>
				            </form>
				        </div>
				    </div>
				</div>

				<div id="alterar_senha" class="modal fade">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                <h4 class="modal-title">Alterar senha</h4>
				                <div id="resultado_troca_senha"></div>
				            </div>
				            <div class="modal-body">
				            	<form id='form_alterar_senha'>
					                <div class='form-group'>
						                <label>Senha antiga:</label>
						                <input id='senhaAntiga' name='senhaAntiga' class='form-control' placeholder='******' type='password' />
					                </div>

					                <div class='form-group'>
						                <label>Nova senha:</label>
						                <input id='senha' name='senha' placeholder='*****' type='password' class="form-control"/>
					                </div>

					                <div class='form-group'>
						                <label>Confirmação de senha:</label>
						                <input id='conf_senha' name='conf_senha' class="form-control" placeholder='*****' type='password'/>
					                </div>
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				                <button id='alterar_senha' type="submit" class="btn btn-primary">Alterar</button>
				            </div>
				            </form>
				        </div>
				    </div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#form_alterar_login").submit(function(){
					$.post("/Empresa/config_altera_login", {
						login : $("#novo_login").val(),
					}, function(result){
				        $("#resultado_troca_login").html(result);
				    });
					return false;
				});

				$("#form_alterar_senha").submit(function(){
					$.post("/Empresa/config_altera_senha", {
						senhaAntiga : $("#senhaAntiga").val(),
						senha : $("#senha").val(),
						conf_senha : $("#conf_senha").val(),
					}, function(result){
				        $("#resultado_troca_senha").html(result);
				    });
					return false;
				});
			});
		</script>
	</body>
</html>