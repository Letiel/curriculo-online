<?php $pessoa = $pessoa[0]; ?>
<html>
	<head>
		<title><?php echo $pessoa->nome. " ".$pessoa->sobrenome ?> - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content='<?php echo "Visualize o perfil de $pessoa->nome $pessoa->sobrenome de $pessoa->cidade, $pessoa->uf em curriculoonline.net.br. Aproveite para fazer seu cadastro e procurar vagas de emprego em sua cidade!"?>' />
		<meta property="og:image" content="<?php echo$pessoa->foto?>"/>
		<meta property="og:title" content="<?php echo$pessoa->nome?>"/>
		<style>
			.quebra-separador{
			    color: #326C99;
			    font-size: 18px;
			    font-weight: normal;
			    margin-top: 17px !important;
			    padding-bottom: 0;
			    padding-top: 10px;
			    clear: both;
			}

			.separador{
			    border-bottom: 1px solid;
			    color: #A6C0D9;
			    display: inline-block;
			    padding: 2px;
			    width: 100%;
			}

			.separador-menor{
				border-bottom: 1px solid;
			    color: #A6C0D9;
			    display: inline-block;
			    padding: 2px;
			    width: 80%;
			    margin: 10px 10% 0 10%;
			}

			.azul{
				color: #069;
			}

			.panel-curriculo{
				padding: 20px 4% 15px 4%;
			}

			#curriculo{
			    padding: 10px 20px;
				box-shadow: 1px 1px 1px 2px #AAA;
			}

			#curriculo img{
				border: 1px solid #DDD;
			}

			#curriculo h1{
				font-size: 19px;
			}

			#curriculo h4{
				font-size: 13px;
			}
		</style>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12 panel-curriculo'>
			<div id='curriculo'>
				<?php
					if($this->session->userdata("logadoEmpresa")){
						$id = $pessoa->id;
						$nome = $pessoa->nome;
						$nome .= "-".$pessoa->sobrenome;
						//echo "<a href='/Mensagem/empresa/$id/$nome' class='btn btn-success pull-right'>Enviar mensagem</a>";
						?>

						<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#enviarMensagem">
						  Enviar Mensagem
						</button>
						<!-- Modal -->
						<div class="modal fade" id="enviarMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						    	<?php
						    	echo form_open('Mensagem/empresa');
						    	?>
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="myModalLabel">Enviar mensagem para <?php echo $pessoa->nome." ".$pessoa->sobrenome ?></h4>
						      </div>
						      <div class="modal-body">
						        <?php
					        		
					        		echo validation_errors();
					                
					                echo "<input type='hidden' name='usuario' value='$pessoa->id' />";
					                echo "<div class='form-group'>";
					                echo form_label('Mensagem:');
					                echo "<textarea name='mensagem' class='form-control' placeholder='Enviar mensagem para $pessoa->nome $pessoa->sobrenome'></textarea>";
					                echo "</div>";

					        		// echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary', 'value'=>'Enviar'));

						        ?>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						        <button type="submit" class="btn btn-primary">Enviar Mensagem</button>
						      </div>
						      <?php echo form_close(); ?>
						    </div>
						  </div>
						</div>
						<?php
					}
					//$date = $pessoa->dt_nascimento;
					$date = new DateTime( $pessoa->dt_nascimento );
					$interval = $date->diff( new DateTime() );

					$idade = $interval->format( '%Y' );
					?><div class="col-lg-3 col-md-4 col-sm-4 col-xs-5" style="margin: 10px 50px 10px 25px">				<?php
						echo "<img src='".$pessoa->foto."' class='img-responsive' />"; 
					echo "</div>";
					echo "<h1 class='azul'>".$pessoa->nome." ".$pessoa->sobrenome."</h1>";
					echo "<h4 class='azul'><strong>Sexo: </strong>".$pessoa->sexo."</h4>";

					if($idade<100 && $idade > 0){
						echo "<h4 class='azul'><strong>Idade: </strong> $idade anos</h4>";
					}

					if ($pessoa->escolaridade != NULL && $pessoa->escolaridade != ''){
						echo "<h4 class='azul'><strong>Escolaridade: </strong>".$pessoa->escolaridade."</h4>";
					}

					if ($pessoa->estado_civil != NULL && $pessoa->estado_civil != ''){
						echo "<h4 class='azul'><strong>Estado Civil: </strong>".$pessoa->estado_civil."</h4>";
					}

					if ($pessoa->filhos > 0){
						echo "<h4 class='azul'><strong>Filhos: </strong>".$pessoa->filhos."</h4>";
					}

					echo "<h3 class='quebra-separador'>Endereço</h3>";
					echo "<div class='separador'></div>";
					//-------------------
					if ($pessoa->endereco != NULL && $pessoa->endereco != ''){
						echo "<h4 class='azul'><strong>Endereço: </strong>".$pessoa->endereco."</h4>";
					}

					if ($pessoa->bairro != NULL && $pessoa->bairro != ''){
						echo "<h4 class='azul'><strong>Bairro: </strong>".$pessoa->bairro."</h4>";
					}

					if ($pessoa->complemento != NULL && $pessoa->complemento != ''){
						echo "<h4 class='azul'><strong>Complemento: </strong>".$pessoa->complemento."</h4>";
					}

					echo "<h4 class='azul'><strong>Cidade: </strong>".$pessoa->cidade.", ".$pessoa->uf."</h4>";


					if ($pessoa->cep != NULL && $pessoa->cep != ''){
						echo "<h4 class='azul'><strong>CEP: </strong>".$pessoa->cep."</h4>";
					}
					//-------------------
					
					if ($pessoa->telefone != NULL && $pessoa->telefone != ''){
						echo "<h4 class='azul'><strong>Telefone: </strong>".$pessoa->telefone."</h4>";
					}
					if ($pessoa->celular != NULL && $pessoa->celular != ''){
						echo "<h4 class='azul'><strong>Celular: </strong>".$pessoa->celular."</h4>";
					}

					if ($pessoa->link_facebook != NULL && $pessoa->link_facebook != ''){
						echo "<h4 class='azul'><strong>Facebook: </strong> <a href='".$pessoa->link_facebook."' target='_blank' class='link' >".$pessoa->link_facebook."</a><br/><br/></h4>";
					}
					//echo "<div class='separador'></div>";
					if ($cursos != NULL){

						echo "<h3 class='quebra-separador'>Cursos</h3>";
						echo "<div class='separador'></div>";

						foreach($cursos as $curso){
							echo "<h4 class='azul item'>$curso->nome - $curso->instituicao </h4>";
						}

						//echo "<div class='separador'></div>";

					}

					if($areas_usuario != NULL){
						?>
						<h3 class='quebra-separador'>Áreas de atuação</h3>
						<div class='separador'></div>
						<?php
						foreach($areas_usuario as $area){
						?>
							<h4 class="azul item"><?php echo $area->descricao ?></h4>
						<?php
						}
					}

					if ($idiomas != NULL){

						echo "<h3 class='quebra-separador'>Idioma(s)</h3>";
						echo "<div class='separador'></div>";

						foreach($idiomas as $idioma){
							echo "<h4 class='azul'>$idioma->nome</h3>";
						}
						//echo "<div class='separador'></div>";
					}


					if ($exp != NULL){
						echo "<h3 class='quebra-separador'>Experiências</h3>";
						echo "<div class='separador'></div>";
						$i = 0;
						foreach($exp as $experiencia){
							if($i > 0)
								echo "<div class='separador-menor'></div>";
							$inicio = date("d/m/Y", strtotime($experiencia->inicio));
							if($experiencia->final != '0000-00-00')
								$final = date("d/m/Y", strtotime($experiencia->final));
							else
								$final = "agora";
							echo "<h4 class='azul'><strong>Empresa: </strong>$experiencia->empresa</h4>";
							echo "<h4 class='azul'><strong>Função: </strong>$experiencia->funcao</h4>";
							echo "<h4 class='azul'><strong>Descrição: </strong>$experiencia->descricao</h4>";
							echo "<h4 class='azul'><strong>Período: </strong> De $inicio até $final</h4>";
							$i++;
						}
						//echo "<div class='separador'></div>";
					}

					if($pessoa->autonomo && $total_servicos > 0){
						echo "<h3 class='quebra-separador'>Serviços Autônomos</h3>";
						echo "<div class='separador'></div>";
						foreach($servicos as $servico){
							echo "<h4 class='azul quebra-separador'><b>$servico->titulo_servico</b></h4>";
							?>
							<h4 class='azul'><?php echo $servico->descricao_servico ?></h4>
							<h4 class='azul'><b>Atende em:</b> <?php echo $servico->lugares ?></h4>
							<h4 class='azul'><b>Contato:</b> <?php echo $servico->contato ?></h4>
							<h4 class='azul'><b>Realiza desde:</b> <?php echo date("d/m/Y", strtotime($servico->inicio)) ?></h4>
							<!--<div class="modal fade" tabindex="-1" role="dialog" id='<?php echo $servico->id ?>'>
							  <div class="modal-dialog">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h1 class="modal-title azul"><?php echo $pessoa->nome." ".$pessoa->sobrenome ?></h1>
							      </div>
							      <div class="modal-body">
							        <h3 class='azul'><?php echo $servico->titulo_servico ?></h3>
							        <h4 class='azul'><?php echo $servico->descricao_servico ?></h4>
							        <h4 class='azul'>Atende em: <?php echo $servico->lugares ?></h4>
							        <h4 class='azul'>Contato: <?php echo $servico->contato ?></h4>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
							      </div>
							    </div>
							  </div>
							</div>-->
							<?php
						}
					}


					if ($pessoa->disp_viajar != NULL && $pessoa->disp_viajar != ''){
						echo "<h3 class='quebra-separador'>Outros</h3>";
						echo "<div class='separador'></div>";
						echo "<h4 class='azul'><strong>Disponibilidade para Viajar: </strong>";
							if ($pessoa->disp_viajar == 1){ 
								echo "Sim ";
							}
							if ($pessoa->disp_viajar == 0){ 
								echo "Não ";
							}
							echo "</h4>";
					}

					if ($pessoa->deficiente != NULL && $pessoa->deficiente != ''){
						echo "<h4 class='azul'><strong>Deficiente: </strong>";
							if ($pessoa->deficiente == 0){ 
								echo "Não ";
							}
							if ($pessoa->deficiente == 1){ 
								echo "Sim ";
							}
							echo "</h4>";
					}

					echo "<h4 class='azul'>";
					echo "<strong>Veículos próprios: </strong>";
					if ($pessoa->carro == 1 || $pessoa->moto == 1 || $pessoa->caminhao == 1 || $pessoa->outro_veiculo == 1){
							if ($pessoa->carro == 1){ 
								echo "Carro ";
							}
							if ($pessoa->moto == 1){ 
								echo "Moto ";
							}
							if ($pessoa->caminhao == 1){ 
								echo "Caminhão ";
							}
							if ($pessoa->outro_veiculo == 1){ 
								echo "Outro veículo";
							}

						
					}else{
						echo "Nenhum";
					}
					echo "</h4>";

					echo "<h4 class='azul'><strong>Carteira de Habilitação: </strong>";
					if ($pessoa->hab_a == 1 || $pessoa->hab_b == 1 || $pessoa->hab_c == 1 || $pessoa->hab_d == 1 || $pessoa->hab_e == 1){
						
							if ($pessoa->hab_a == 1){ 
								echo "A ";
							}
							if ($pessoa->hab_b == 1){ 
								echo "B ";
							}
							if ($pessoa->hab_c == 1){ 
								echo "C ";
							}
							if ($pessoa->hab_d == 1){ 
								echo "D ";
							}
							if ($pessoa->hab_e == 1){ 
								echo "E";
							}
							echo "</h4>";

						
					}else{
						echo "Sem habilitação";
					}

					echo "</h4>";

				?>
			</div><!-- /#curriculo -->

			

			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-553d8a231f4f63dc" async="async"></script>
			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<div class="addthis_sharing_toolbox" style="float: left; width:200px;"></div>

			<?php $this->load->view("inc/denunciar.php"); ?>
		</div>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>