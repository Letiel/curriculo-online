<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
	<head>
		<title>Alterar senha - Enviar Currículo</title>
		<?php $this->load->view('inc/headBasico.php');?>
	</head>
	<body>

		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>

				<center class='panel-heading'><h2>Dados de conta</h2></center>
				<?php
					echo $this->session->flashdata('sucesso');
					echo form_open_multipart('Curriculo/senha');
					echo validation_errors();
					if(isset($erro))
						echo $erro;
					echo "<div class='form-group'>";
					echo form_label('Senha antiga:');
					echo form_input(array('name'=>'senhaAntiga', 'class'=>'form-control', 'placeholder'=>'******', 'type'=>'password'));
					echo "</div>";

					echo "<div class='form-group'>";
					echo form_label('Nova senha:');
					echo form_input(array('name'=>'senha', 'class'=>'form-control', 'placeholder'=>'******', 'type'=>'password'));
					echo "</div>";

					echo "<div class='form-group'>";
					echo form_label('Confirmação de senha:');
					echo form_input(array('name'=>'conf_senha', 'class'=>'form-control', 'placeholder'=>'******', 'type'=>'password'));
					echo "</div>";

					echo "<div class='form-group'>";
					echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary', 'value'=>'Alterar'));
					echo "</div>";
					
					echo form_close();
				?>
				<!-- Button HTML (to Trigger Modal) -->
					<a href="#excluir" role="button" class="link" data-toggle="modal">Excluir Conta</a>
					 
					<!-- Modal HTML -->
					<div id="excluir" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Tem certeza que deseja excluir sua conta?</h4>
					            </div>
					            <div class="modal-body">
					                <p>Você perderá o acesso à sua conta.</p>
					                <p class="text-warning"><small>Para voltar a utilizar o sistema, será necessário criar uma nova conta!</small></p>
					            </div>
					            <div class="modal-footer">
					                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					                <a href='<?php echo base_url(); ?>curriculo/excluir_conta' class="btn btn-primary">Excluir Conta</a>
					            </div>
					        </div>
					    </div>
					</div>
			</div>
		</div>		
	</body>
</html>