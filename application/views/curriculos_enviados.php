<html>
	<head>
		<title>Currículo Online - Currículos enviados</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Crie seu currículo totalmente grátis e envie para empresas de sua região. Procurar emprego sem sair de casa com seu Curriculum Vitae." />
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center><h2>Currículos enviados para "<?php echo $curriculos[0]->cargo ?>"</h2></center>
				<?php
				if($total < 1){
					echo "<small><center>Você ainda não recebeu currículos para esta vaga!<br />Você receberá um e-mail no momento em que um curículo for enviado para você.</center></small>";
				}
					foreach($curriculos as $curriculo){
						$curriculo->data = date("d/m/Y", strtotime($curriculo->data));
						$nome = $curriculo->nome_usuario."-".$curriculo->sobrenome_usuario;
						$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º' );
						$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
						$uri = str_replace($what, $by, $nome);
						/*echo "<b><a href='<?php echo base_url(); ?>curriculo/ver/$curriculo->id/$uri' target='_blank'>$curriculo->nome_usuario $curriculo->sobrenome_usuario</b></a> de $curriculo->cidade, $curriculo->uf<br /><small>Enviado em $curriculo->data</small><br />";*/
						echo "<div class='col-md-12' style='clear: both; margin: 1% 0%; border-bottom: 2px solid #D9D9F3; padding-bottom: 2%; max-height: 180px; overflow: hidden;'>
							<a href='/curriculo/ver/$curriculo->id/$uri' target='_blank'>
						      <img class='img-thumbnail' src='$curriculo->foto' alt='Visualizar perfil de $curriculo->nome_usuario' style='width: 20%; float: left;'>
						      <div style='float: left' class='col-md-9'>
							      <h4>$curriculo->nome_usuario $curriculo->sobrenome_usuario</h4>
							      <small>De $curriculo->cidade, $curriculo->uf</small>
							      <br />
							      <small>Enviado em $curriculo->data</small>
							  </div>
						    </a></div>";
					}
					echo $paginacao;
				?>
			</div>
		</div>
		
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>