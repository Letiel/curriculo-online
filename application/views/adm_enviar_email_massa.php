<html>
	<head>
		<title>Enviar e-mails em massa - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<script src="//cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>
	</head>
	<body>
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Enviar E-mails como Administrador</h2></center>
				<?php
					echo form_open_multipart('adm/email_massa');
					echo validation_errors();

					echo form_label('Cidade:');
			        echo "<select name='cidade' class='form-control' id='selectCidade'><option value=''>Selecione...</option>";
			        foreach($cidades as $cidade){
						if($cidade->id == $empresa->idCidade)
							$selected = 'selected';
						else
							$selected = '';
						echo "<option $selected value='".$cidade->id."'>".$cidade->nome.", ".$cidade->estado."</option>";
					}
			        echo "</select>";

			        echo form_label('Descrição:');
			        echo "<textarea name='mensagem' class='form-control ckeditor'>".set_value('descricao')."</textarea>";
			        echo "<button class='btn btn-primary pull-right' type='submit'>Enviar!</button>";

					echo form_close();
				?>
			</div>
			<script type="text/javascript">
				$(document).ready(function(){
					$(".ckeditor").ckeditor();
				});
			</script>
		</div>
	</body>
</html>