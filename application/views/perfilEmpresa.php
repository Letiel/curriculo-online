<?php $empresa = $empresa[0]; ?>
<html>
	<head>
		<title><?php echo $empresa->nome ?> - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="<?php echo "Veja os dados de $empresa->nome de $empresa->cidade, $empresa->uf. Cadastre-se, veja vagas e envie seu curriculo para empresas de sua região." ?>" />
		<meta property="og:image" content="<?php echo $empresa->imagem?>"/>
		<meta property="og:title" content="<?php echo $empresa->nome?>"/>
		<meta property="og:description" content="<?php echo $empresa->textempresa?>"/>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		<?php $this->load->view('inc/anuncioDireita.php');?>

		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<script>
			function envia_curriculo(id){
				$("#resultado_curriculo").html("");
				$.post("/Empresa/envia_curriculo",{
					empresa : id,
				},function(data, status){
					$("#resultado_curriculo").html(data);
				});
			}
		</script>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12' style="padding: 20px 50px 15px 50px">
			<div id='curriculo'>
				<div id='resultado_curriculo'></div>
				<?php
					// if($empresa->recebe_email && !$this->session->userdata('logadoEmpresa')){
					// 	if($this->session->userdata('logado')){
					// 		echo "<button class='btn btn-primary bt-sm pull-right' onclick='envia_curriculo($empresa->id)'>Enviar Currículo</button>";
					// 	}else{
					// 		echo "<button id='makeLogin' class='btn btn-primary bt-sm pull-right' data-toggle='modal' data-toggle='tooltip' data-placement='left' data-target='.bs-example-modal-sm' title='É necessário logar no sistema.'>Enviar Currículo</button>";
					// 	}
					// }
				?>
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-5" style="margin: 10px 50px 10px 25px">
					<?php
					echo "<img src='".$empresa->imagem."' class='img-responsive' alt='$empresa->nome' />"; 
					echo "</div>";
					echo "<h1 class='azul'>".$empresa->nome."</h1><br/>";

					if ($empresa->site != NULL && $empresa->site != ''){
						echo "<h4 class='azul'><strong>Website: </strong> <a href='".$empresa->site."' target='_blank' class='link' >".$empresa->site."</a><br/><br/></h4>";
					}
					
					if ($empresa->textempresa != NULL && $empresa->textempresa != ''){
						echo "<h3 class='quebra-separador'>Sobre a empresa: </h3><div class='separador'></div><h4 class='azul'>".nl2br($empresa->textempresa)."</h4>";
					}

					//$this->load->view("inc/anuncioMeioResultados");
					?>

			

					<?php
					
					if($empresa->endereco != NULL || $empresa->endereco != '' || 
					$empresa->bairro != NULL || $empresa->bairro != '' ||
					$empresa->complemento != NULL || $empresa->complemento != '' ||
					$empresa->cidade != NULL || $empresa->uf != '' ||
					$empresa->cep != NULL || $empresa->cep != '') {
						echo "<h3 class='quebra-separador'>Localização: </h3><div class='separador'></div>";
					}			

					if ($empresa->endereco != NULL && $empresa->endereco != ''){
						echo "<h4 class='azul'><strong>Endereço: </strong>".$empresa->endereco."</h4>";
					}


					if ($empresa->bairro != NULL && $empresa->bairro != ''){
						echo "<h4 class='azul'><strong>Bairro: </strong>".$empresa->bairro."</h4>";
					}


					if ($empresa->complemento != NULL && $empresa->complemento != ''){
						echo "<h4 class='azul'><strong>Complemento: </strong>".$empresa->complemento."</h4>";
					}

					if ($empresa->cidade != NULL && $empresa->uf != ''){
						echo "<h4 class='azul'><strong>Cidade: </strong>".$empresa->cidade.", ".$empresa->uf."</h4>";
					}


					if ($empresa->cep != NULL && $empresa->cep != ''){
						echo "<h4 class='azul'><strong>CEP: </strong>".$empresa->cep."<br/><br/></h4>";
					}
				?>

				<div style='margin: 25px 0; width: 100%; overflow: hidden;'></div>



				<!-- Go to www.addthis.com/dashboard to customize your tools -->
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-553d8a231f4f63dc" async="async"></script>
				<!-- Go to www.addthis.com/dashboard to customize your tools -->
				<div class="addthis_sharing_toolbox" style="float: right;"></div>


				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- EnviarCurriculoLinksAzul -->
				<ins class="adsbygoogle"
				     style="display:inline-block;width:200px;height:90px"
				     data-ad-client="ca-pub-1007226041030915"
				     data-ad-slot="9738125980"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>

			
			</div>
			<?php $this->load->view("inc/denunciar.php"); ?>
		</div>

		<?php if($total_vagas > 0){ ?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12' style="padding: 20px 50px 15px 50px">
			<center><h2 class="azul">Vagas</h2></center>
			<?php
			foreach($vagas as $vaga) {
			?>
			<h4 class='azul'>
				<span><?php echo $vaga->cargo ?></span>
				<a target='_blank' href='/vaga/ver/<?php echo $vaga->id ?>/<?php echo $this->funcoes->get_url($vaga->cargo) ?>' class='pull-right btn btn-sm btn-primary'>Ver</a>

			</h4>
			<div class="separador"></div>
			<?php
			}
			?>
		</div>
		<?php } ?>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>