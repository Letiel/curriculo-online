<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
	<head>
		<title>Editar seu Curriculo - Enviar Currículo</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<style>
			.separa{
				float: left;
				margin-left: 2%;
				font-weight: bold;
				border-left: 1px solid #CCC;
				padding-left: 1%;
			}

			.aumenta{
				overflow: hidden;
  				height: auto;
			}

			label{
				color: #337ab7;
				font-size: 18px;
			}
		</style>

		<script src="<?php echo base_url();?>js/mascara.js"></script>

		<script type='text/javascript'>
		function atualizaCursos(){
			$.get( "<?php echo base_url(); ?>curriculo/pega_cursos_by_user", function( data ) {
			  $('#cursos').html(data);
			});
		}

		function carregaIdiomas(){
			$.post("<?php echo base_url(); ?>curriculo/cata_idiomas_select",{},function(data, status){
		    	$('#select_idiomas').html(data);
		    });
		}

		function atualizaExp(){
			$.post("<?php echo base_url(); ?>curriculo/retorna_exp_by_user",{},function(data, status){
		    	$('#lista_experiencias').html(data);
		    });
		}

		function atualizaIdiomas(){
			$.post("<?php echo base_url(); ?>curriculo/cata_idiomas_by_user",{},function(data, status){
		    	$('#lista_idiomas').html(data);
		    });
		}

		function deletaIdioma(idioma){
			$.post("<?php echo base_url(); ?>curriculo/deletaIdioma",{
				idioma : idioma
			},function(data, status){
		    	$('#lista_idiomas').html(data);
		    });	
		    atualizaIdiomas();
		    carregaIdiomas();
		    atualizaIdiomas();
		    carregaIdiomas();
		}
		setInterval(atualizaExp, 5000);
		setInterval(atualizaIdiomas, 5000);

		function deletaCurso(id){
			$.get( "<?php echo base_url(); ?>curriculo/deletaCurso/" + id, function(){});
			atualizaCursos();
		}

		function deletaExp(id){
			$.post("<?php echo base_url(); ?>curriculo/deletaExp",{
				id : id
			},function(data, status){
		    	atualizaExp();
		    });
		}

		$(document).ready(function(){
			atualizaExp();
			atualizaIdiomas();
			carregaIdiomas();
			$(".out").blur(function(){
				var nome = $(this).attr("name");
				var val = $(this).val();
				$.post("<?php echo base_url(); ?>curriculo/update_especifico",{
					campo : nome,
					value : val,
				},function(data, status){
			    	if(data != "" && data != null){
			    		alert("Parece que sua informação não é válida!\nVerifique os dados inseridos.");
			    		console.log(data);
			    	}
			    });
			});

			$(".radio").change(function(){
				var nome = $(this).attr("name");
				var val = $(this).val();
				console.log(nome + " = " + val);
				$.post("<?php echo base_url(); ?>curriculo/update_especifico",{
					campo : nome,
					value : val,
				},function(data, status){
			    	console.log(data);
			    });
			});


			$.get( "<?php echo base_url(); ?>curriculo/pega_cursos_for_user", function( data ) {
			  $('#id_curso').html(data);
			});
			atualizaCursos();
			setInterval(atualizaCursos, 5000)
			$('#adicionarCurso').click(function(){
				curso = document.getElementById('id_curso').value;
				onde = document.getElementById('instituicao').value;
				if(curso != null && curso != "" && onde != null && onde != ""){
					curso_concluido = document.getElementById('curso_concluido').checked;
					instituicao = document.getElementById('instituicao').value;
					if(curso_concluido == true){
						concluido = 1;
					}else{
						concluido = 0;
					}

					$.post("<?php echo base_url(); ?>curriculo/cadastraCurso",{
						curso : curso,
						concluido : concluido,
						instituicao : instituicao
					},function(data, status){
				    	$('#instituicao').val("");
					  	$('#result_cursos').html(data);
				    });


					$.get( "<?php echo base_url(); ?>curriculo/pega_cursos_for_user", function( data ) {
					  $('#id_curso').html(data);
					});
				}else{
					alert ("Informe todos os dados!");
				}
				atualizaCursos();
			});
		});
		</script>
	</head>
	<body>

		<script>
		jQuery(function($){
   			$("#telefone").mask("(99) 9999-9999?9");
   			$("#exp_telefone").mask("(99) 9999-9999?9");
   			$("#celular").mask("(99) 9999-9999?9");
   			$("#cep").mask("99999-999");
   			$("#dt_nascimento").mask("99/99/9999");
   			$("#exp_inicio").mask("99/99/9999");
   			$("#exp_final").mask("99/99/9999");
			});
		</script>

		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Editar Curr&iacute;culo</h2></center>
				<?php
					$opcoes = array('id' => 'form_curriculo');
					echo form_open_multipart('Curriculo', $opcoes);
					$pessoa = $pessoa[0];
					echo validation_errors();

					echo form_label('Nome:');
					echo form_input(array('name'=>'nome', 'class'=>'form-control out', 'id'=>'nome'), set_value('nome', $pessoa->nome));

					echo form_label('Sobrenome:');
					echo form_input(array('name'=>'sobrenome', 'class'=>'form-control out'), set_value('sobrenome', $pessoa->sobrenome));

					/*echo form_label('CPF:');
					echo form_input(array('name'=>'cpf', 'class'=>'form-control', 'disabled'=>'disabled'), set_value('cpf', $pessoa->cpf));*/

					echo form_label('Data de nascimento:');
					echo form_input(array('name'=>'dt_nascimento', 'class'=>'form-control out', 'id'=>'dt_nascimento'), set_value('dt_nascimento', date('d/m/Y', strtotime($pessoa->dt_nascimento))));

					echo form_label('Cidade:');
					echo "<select name='cidade' class='form-control out'>";
					foreach($cidades as $cidade){
						if($cidade->id == $pessoa->idCidade){
							$selected = "selected = 'selected'";
						}else{
							$selected = "";
						}
						echo "<option value='$cidade->id' $selected>$cidade->nome, $cidade->estado</option>";
					}
					echo "</select>";

					echo form_label('Endereço:');
					echo form_input(array('name'=>'endereco', 'class'=>'form-control out'), set_value('endereco', $pessoa->endereco));

					echo form_label('Bairro:');
					echo form_input(array('name'=>'bairro', 'class'=>'form-control out'), set_value('bairro', $pessoa->bairro));

					echo form_label('Complemento:');
					echo form_input(array('name'=>'complemento', 'class'=>'form-control out'), set_value('complemento', $pessoa->complemento));

					echo form_label('E-mail:');
					echo form_input(array('name'=>'email', 'class'=>'form-control out'), set_value('email', $pessoa->email));

					echo form_label('Facebook:');
					echo form_input(array('name'=>'link_facebook', 'class'=>'form-control out', 'placeholder'=>'URL do perfil do facebook'), set_value('link_facebook', $pessoa->link_facebook));

					echo form_label('Estado civil:');
					echo "<select name='estado_civil' class='form-control out'>
						<option value=''>Selecione</option>";
						foreach($estados_civis as $estado){
							if($estado->id == $pessoa->idEstado_civil)
								$selected = 'selected';
							else
								$selected = '';
							echo "<option $selected value='".$estado->id."'>".$estado->descricao."</option>";
						}
						echo "</select>";

					echo form_label('Sexo:');
					if($pessoa->sexo == 'Feminino'){
						echo "<select name='sexo' class='form-control out'>
						<option value=''>Selecione</option>
						<option value='Feminino' selected='selected'>Feminino</option>
						<option value='Masculino'>Masculino</option>";
						echo "</select>";
					}else{
						if($pessoa->sexo == 'Masculino'){
							echo "<select name='sexo' class='form-control out'>
								<option value=''>Selecione</option>
								<option value='Feminino'>Feminino</option>
								<option value='Masculino' selected='selected'>Masculino</option>";
							echo "</select>";
						}else{
							echo "<select name='sexo' class='form-control out'>
							<option value=''>Selecione</option>
							<option value='Feminino'>Feminino</option>
							<option value='Masculino'>Masculino</option>";
							echo "</select>";
						}
					}
						

					echo form_label('Filhos:');
					echo form_input(array('name'=>'filhos', 'class'=>'form-control out', 'style'=>'width: 60px;', 'maxlength'=>'2'), set_value('filhos', $pessoa->filhos));

					echo form_label('Telefone:');
					echo form_input(array('name'=>'telefone', 'class'=>'form-control out', 'id'=>'telefone'), set_value('telefone', $pessoa->telefone));

					echo form_label('Celular:');
					echo form_input(array('name'=>'celular', 'class'=>'form-control out', 'id'=>'celular'), set_value('celular', $pessoa->celular));

					echo form_label('CEP:');
					echo form_input(array('name'=>'cep', 'class'=>'form-control out', 'id'=>'cep'), set_value('cep', $pessoa->cep));

					echo form_label('Escolaridade:');
					echo "<select name='escolaridade' class='form-control out'>";
					echo "<option value=''>Selecione...</option>";
					foreach($escolaridades as $escolaridade){
						if($escolaridade->id == $pessoa->idEscolaridade)
								$selected = 'selected';
							else
								$selected = '';
						echo "<option $selected value='".$escolaridade->id."'>".$escolaridade->descricao."</option>";
					}
					echo "</select><br/>";


					?>
					<fieldset class='field'>
					<?php
			        echo form_label('Cursos:');
			        echo "<div id='result_cursos'></div>";
			        echo "<div id='cursos'></div>";
			        echo "<select name='curso' style='margin-top: 1%;' class='form-control input-sm' id='id_curso'><option value=''>Cadastrar novo...</option></select>";
					
					echo form_label("Onde: ");
					echo form_input(array('id'=>'instituicao', 'class'=>'form-control'));

					echo form_label("Concluído ");
					echo "<input type='checkbox' id='curso_concluido' /><br/>";
					echo "<button type='button' class='btn btn-success' id='adicionarCurso' onclick='return false'>Adicionar Curso</button>";

					?>
					<a href="#myModal" role="button" class="link" id='abre_modal' data-toggle="modal">Seu curso não está na lista?</a>
					</fieldset>
					<!-- Modal HTML -->
					<div id="modal_curso" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Cadastrar novo curso</h4>
					            </div>
					            <div class="modal-body">
					            	<?php echo form_label('Nome do curso'); ?>
					                <input type='text' id='nome_novo_curso' placeholder='Nome do curso...' class='form-control' />
					                <?php echo form_label('Instituição'); ?>
					                <?php echo form_input(array('id'=>'instituicao_modal', 'class'=>'form-control', 'placeholder'=>'Instituição...'));
					                echo form_label("Concluído ");
									echo "<input type='checkbox' id='curso_concluido_modal' />";
					                ?>
					            </div>
					            <div class="modal-footer">
					                <button type="button" id='botao_cadastrar_curso' class="btn btn-primary" onclick='return false'>Cadastrar</button>
					            </div>
					        </div>
					    </div>
					</div>
					<script type="text/javascript">
					$(document).ready(function(){
					    $("#abre_modal").click(function(){
					        $("#modal_curso").modal('show');
					    });

					    $('#botao_cadastrar_curso').click(function(){
					    	curso = $('#nome_novo_curso').val();

					    	curso_concluido = document.getElementById('curso_concluido_modal').checked;
							instituicao = document.getElementById('instituicao_modal').value;
							if(curso_concluido == true){
								concluido = 1;
							}else{
								concluido = 0;
							}

					    	if(curso != null && curso != "" && instituicao != null && instituicao != ""){
						    	//$.get( "<?php echo base_url(); ?>curriculo/cadastra_novo_curso/" + curso + "/" + concluido + "/" + instituicao, function( data ) {});
						    	$.post("<?php echo base_url(); ?>curriculo/cadastra_novo_curso",{
							        nome_curso: curso,
							        concluido: concluido,
							        instituicao: instituicao
							    },
							    function(data, status){});
						    	$("#modal_curso").modal('hide');
						    	$("#nome_novo_curso").val('');
						    	$("#instituicao_modal").val('');
						    	atualizaCursos();
							}else{
								alert ("Informe os dados do curso!");
							}
					    });
					});
					</script><br/><br/>
					<fieldset class='field'>
						<?php

						echo form_label('Experiências:');
						echo "<div id='lista_experiencias'></div>";
						?>
						<a href="#myExp" role="button" class="btn btn-primary" id='abre_modal_experiencias' data-toggle="modal">Adicionar experiência</a>
					</fieldset>
					<div id="modal_experiencias" class="modal fade">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                <h4 class="modal-title">Adicionar experiência ao currículo</h4>
					            </div>
					            <div class="modal-body">
					            	<div class='form-group'>
						            	<?php echo form_label('Empresa'); ?>
						            	<input type='text' id='empresa' class='form-control' placeholder='Nome da empresa...' />
					            	</div>
					            	<div class='form-group'>
				            			<?php echo form_label('Cidade'); ?>
						            	<select id='exp_cidade' class='form-control'>
						            		<?php 
							            		foreach($cidades as $cidade){
							            			echo "<option value='$cidade->id'>$cidade->nome, $cidade->estado</option>";
							            		}
							            	?>
						            	</select>
						            </div>
						            <div class='form-group'>
				            			<?php echo form_label('Endereço'); ?>
						            	<input type='text' id='exp_endereco' class='form-control' placeholder='Endereço da empresa' />
						            </div>
						            <div class='form-group'>
				            			<?php echo form_label('Telefone'); ?>
						            	<input type='text' id='exp_telefone' class='form-control' placeholder='Telefone da empresa' />
						            </div>
					            	<div class='form-group'>
				            			<?php echo form_label('Função'); ?>
						            	<input type='text' id='exp_funcao' class='form-control' placeholder='Descreva a função...' />
						            </div>
						            <div class='form-group'>
						            	<?php echo form_label('Início'); ?>
						            	<input type='text' id='exp_inicio' class='form-control' />
						            </div>
						            <div class='form-group'>
						            	<?php echo form_label('Final'); ?>
						            	<input type='text' id='exp_final' class='form-control' placeholder='Caso ainda esteja na função deixe em branco'/>
						            </div>
						            <div class='form-group'>
						            	<?php echo form_label('Descrição'); ?>
						                <textarea id='exp_descricao' placeholder='Escreva uma pequena descrição...' class='form-control'></textarea>
						            </div>
					            </div>
					            <div class="modal-footer">
				                	<button type="button" id='botao_cadastrar_exp' class="btn btn-primary" onclick='return false'>Adicionar</button>
					            </div>
					        </div>
					    </div>
					</div>
					<script type="text/javascript">
					$(document).ready(function(){
    					$('.popover_exp').popover();
					    $("#abre_modal_experiencias").click(function(){
					        $("#modal_experiencias").modal('show');
					    });

					    $('#botao_cadastrar_exp').click(function(){
					    	descricao_exp = $('#exp_descricao').val();
					    	funcao_exp = $('#exp_funcao').val();
					    	inicio_exp = $('#exp_inicio').val();
					    	final_exp = $('#exp_final').val();
					    	empresa = $('#empresa').val();
					    	endereco = $('#exp_endereco').val();
					    	telefone = $('#exp_telefone').val();
					    	cidade = $('#exp_cidade').val();


					    	if(descricao_exp != null && descricao_exp != "" && funcao_exp != null && funcao_exp != "" && inicio_exp != null && inicio_exp != "" && empresa != null && empresa != ""){
						    	$.post("<?php echo base_url(); ?>curriculo/cadastra_experiencia",{
							        descricao : descricao_exp,
							        funcao : funcao_exp,
							        inicio : inicio_exp,
							        final_ : final_exp,
							        empresa : empresa,
							        endereco : endereco,
							        telefone : telefone,
							        cidade : cidade
							    },
							    function(data, status){});
						    	$("#modal_experiencias").modal('hide');
						    	descricao_exp = $('#exp_descricao').val('');
						    	funcao_exp = $('#exp_funcao').val('');
						    	inicio_exp = $('#exp_inicio').val('');
						    	final_exp = $('#exp_final').val('');
						    	empresa = $('#empresa').val('');

							}else{
								alert ("Verifique os campos!");
							}
					    });
					});
					</script><br/><br/>
					<fieldset class='field'>
					<?php 

					echo form_label('Idiomas:');
					//$x = $pessoa->idiomas;
					//echo "<textarea name='idiomas' class='form-control'>$x</textarea>";					
					echo "<div id='lista_idiomas'></div>";
					?>
					<select id='select_idiomas' class='form-control'>
						<option value=''>Selecione</option>
					</select>
					<a role="button" class="btn btn-primary" id='botao_cadastrar_idioma' style='margin-top: 10px;'>Adicionar idioma</a>
					</fieldset>
					<script type="text/javascript">
					$(document).ready(function(){

					    $('#botao_cadastrar_idioma').click(function(){
					    	idioma = $('#select_idiomas').val();


					    	if(idioma != null && idioma != ""){
						    	$.post("<?php echo base_url(); ?>curriculo/cadastra_idioma_usuario",{
						    		idioma : idioma
						    	},
							    function(data, status){
							    });
							    carregaIdiomas();
							    atualizaIdiomas();
							}else{
								alert ("Informe um idioma da lista!");
							}
					    });
					});
					</script><br/><br/>
					<?php 

					echo form_label('Habilitação:');
					echo "<div class='form-control aumenta'>";

					if($pessoa->hab_a == 1){
						echo '<div class="separa" style="border: none;">A:<label class="radio-inline">
								  <input checked class="radio" type="radio" name="hab_a" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_a" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa" style="border: none;">A:<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_a" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_a" value="0"> Não
								</label></div>';
					}

					if($pessoa->hab_b == 1){
						echo '<div class="separa">B:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_b" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_b" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa">B:<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_b" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_b" value="0"> Não
								</label></div>';
					}

					if($pessoa->hab_c == 1){
						echo '<div class="separa">C:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_c" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_c" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa">C:<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_c" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_c" value="0"> Não
								</label></div>';
					}

					if($pessoa->hab_d == 1){
						echo '<div class="separa">D:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_d" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_d" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa">D:<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_d" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_d" value="0"> Não
								</label></div>';
					}

					if($pessoa->hab_e == 1){
						echo '<div class="separa">E:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_e" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_e" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa">E:<label class="radio-inline">
								  <input type="radio" class="radio" name="hab_e" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="hab_e" value="0"> Não
								</label></div>';
					}

					echo "</div>";


					echo form_label('Veículos Próprios:');
					echo "<div class='form-control aumenta'>";

					if($pessoa->carro == 1){
						echo '<div class="separa" style="border: none">Carro:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="carro" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" name="carro" class="radio" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa" style="border: none">Carro:<label class="radio-inline">
								  <input type="radio" name="carro" class="radio" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="carro" value="0"> Não
								</label></div>';
					}

					if($pessoa->moto == 1){
						echo '<div class="separa">Moto:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="moto" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="moto" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa">Moto:<label class="radio-inline">
								  <input type="radio" class="radio" name="moto" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="moto" value="0"> Não
								</label></div>';
					}

					if($pessoa->caminhao == 1){
						echo '<div class="separa">Caminhao:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="caminhao" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="caminhao" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa">Caminhão:<label class="radio-inline">
								  <input type="radio" class="radio" name="caminhao" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="caminhao" value="0"> Não
								</label></div>';
					}

					if($pessoa->outro_veiculo == 1){
						echo '<div class="separa">Outro Veículo:<label class="radio-inline">
								  <input checked type="radio" class="radio" name="outro_veiculo" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="outro_veiculo" value="0"> Não
								</label></div>';
					}else{
						echo '<div class="separa">Outro veículo:<label class="radio-inline">
								  <input type="radio" class="radio" name="outro_veiculo" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="outro_veiculo" value="0"> Não
								</label></div>';
					}

					echo "</div>";

					echo form_label('Disponibilidade para viajar?');

					echo "<div class='form-control'>";

					if($pessoa->disp_viajar == 1){
						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="disp_viajar" id="viajarSim" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="disp_viajar" id="viajarSim" value="0"> Não
								</label>';
					}else{
						echo '<label class="radio-inline">
								  <input type="radio" class="radio" name="disp_viajar" id="viajarSim" value="1"> Sim
								</label>';

						echo '<label class="radio-inline">
								  <input checked type="radio" class="radio" name="disp_viajar" id="viajarSim" value="0"> Não
								</label>';
					}

					echo "</div>";

					echo form_label('Portador de defici&ecirc;ncia: ');
					if($pessoa->deficiente == 1){
						echo "<select name='deficiente' class='form-control out'>
							<option value='Selecione'>Selecione...</option>
							<option value='1' selected='selected'>Sim</option>
							<option value='0'>Não</option>
						</select>";
					}else{
						if($pessoa->deficiente == 0){
							echo "<select name='deficiente' class='form-control'>
								<option value='Selecione'>Selecione...</option>
								<option value='1'>Sim</option>
								<option value='0' selected='selected'>Não</option>
							</select>";
						}else{
							echo "<select name='deficiente' class='form-control'>
								<option value='Selecione'>Selecione...</option>
								<option value='1'>Sim</option>
								<option value='0'>Não</option>
							</select>";
						}
					}

					echo form_label('Foto:');
					echo "<div class='input-group'>
			                <span class='input-group-btn'>
			                    <span class='btn btn-primary btn-file'>
			                        Enviar foto <input type='file' name='foto'>
			                    </span>
			                </span>
			                <input type='text' class='form-control' readonly>
			            </div><br/>";

					echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary pull-right', 'value'=>'Atualizar'));

					echo form_close();
				?>
			</div>
			<script>
				$(document).on('change', '.btn-file :file', function() {
				  var input = $(this),
				      numFiles = input.get(0).files ? input.get(0).files.length : 1,
				      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				  input.trigger('fileselect', [numFiles, label]);
				});

				$(document).ready( function() {
				    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
				        
				        var input = $(this).parents('.input-group').find(':text'),
				            log = numFiles > 1 ? numFiles + ' files selected' : label;
				        
				        if( input.length ) {
				            input.val(log);
				        } else {
				            if( log ) alert(log);
				        }
				        
				    });
				});
			</script>
		</div>
		
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		
	</body>
</html>