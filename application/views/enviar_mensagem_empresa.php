<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$nome = str_replace('-', ' ', $nome);
?>
<html>
	<head>
		<title>Enviar Mensagem para <?php echo $nome ?> - Currículo Online</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<style>
			.separa{
				float: left;
				margin-left: 2%;
				font-weight: bold;
				border-left: 1px solid #CCC;
				padding-left: 1%;
			}

			.aumenta{
				overflow: hidden;
  				height: auto;
			}
		</style>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
		<div class='panel panel-default panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<center class='panel-heading'><h2>Enviar mensagem para <?php echo $nome ?></h2></center>
				<?php
					if(isset($sucesso)){
						echo "<div class='alert alert-success'>Mensagem enviada com sucesso!</div>";
						echo $sucesso;
					}else{
						echo form_open('Mensagem/empresa');
						echo validation_errors();
				        
				        echo "<input type='hidden' name='usuario' value='$usuario' />";
				        echo "<div class='form-group'>";
				        echo form_label('Mensagem:');
				        echo "<textarea name='mensagem' class='form-control' placeholder='Enviar mensagem para $nome'></textarea>";
				        echo "</div>";

						echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary', 'value'=>'Enviar'));

						echo form_close();
				}
				?>
			</div>
		</div>
		
		<?php $this->load->view('inc/anuncioMeio.php');?>
		
	</body>
</html>