<html>
	<head>
		<title>Currículo Online - Mensagens</title>
		<?php $this->load->view('inc/headBasico.php');?>
		<meta name='description' content="Crie seu currículo totalmente grátis e envie para empresas de sua região. Procurar emprego sem sair de casa com seu Curriculum Vitae." />
		<script>
		$(document).ready(function(){
		    $('[data-toggle="popover"]').popover();   
		    $(".responder").click(function(){
		        $("#responder").modal('show');
		    });
		});

		function lerMsg(id){
			$.post("<?php echo base_url(); ?>Mensagem/ler",{
				mensagem : id
			},function(data, status){
		    });
		}
		</script>
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>
		
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		
		
		<?php $this->load->view('inc/anuncioDireita.php');?>
		<?php $this->load->view('inc/anuncioMeio.php');?>
		<div class='panel panel-default col-md-8 col-sm-12 col-xs-12'>
			<div class='panel-body'>
				<?php echo $this->session->flashdata('mensagem_enviada') ?>
				<center><h3>Mensagens</h3></center>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php
					$i = 0;
					foreach($mensagens as $mensagem){
				      	if($mensagem->lida == 0 && $mensagem->lado == 0){
							$js = "onclick='lerMsg($mensagem->id_mensagem)'";
							$style = 'style="background: #337ab7; color: #FFF;"';
						}else{
							$js = "";
							$style = 'style="background: #f5f5f5; color: #000;"';
						}
							?>
							
							  <div class="panel panel-default">
							    <div class="panel-heading" <?php echo $style; ?> role="tab" id="heading<?php echo $mensagem->id_mensagem ?>">
							      <h4 class="panel-title">
							        <a role="button" <?php echo $js ?> data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $mensagem->id_mensagem ?>" aria-expanded="false" aria-controls="collapse<?php echo $mensagem->id_mensagem ?>">
							          <?php
							          	$data = date("d/m/Y", strtotime($mensagem->data));
							          	if($mensagem->lado == 0)
							          		echo "De ".$mensagem->usuario." - ".$data."";
							          	else
							          		echo "Para ".$mensagem->usuario." - ".$data;
							          ?>
							        </a>
							      </h4>
							    </div>
							    <div id="collapse<?php echo $mensagem->id_mensagem ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $mensagem->id_mensagem ?>">
							      <div class="panel-body">
							        <?php echo $mensagem->mensagem;
							        if($mensagem->respondida == 0 && $mensagem->lado == 0){
								  ?>

								  <p><a href="#resposta-<?php echo $mensagem->id_mensagem ?>" role="button" class="responder" data-toggle="modal">Responder</a></p>

								  <div id="resposta-<?php echo $mensagem->id_mensagem ?>" class="modal fade">
									    <div class="modal-dialog">
									        <div class="modal-content">
									            <div class="modal-header">
									                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									                <h4 class="modal-title">Responder mensagem</h4>
									            </div>
									            <div class="modal-body">
									                <?php echo form_open('Mensagem/empresa');
									                echo "<input type='hidden' name='usuario' value='$mensagem->idUsuario' />";
									                echo "<input type='hidden' name='id_mensagem' value='$mensagem->id_mensagem' />";
									                echo form_label('Mensagem:');
									                ?>
									                <textarea placeholder='Digite a mensagem...' name='mensagem' class='form-control'></textarea>
									            </div>
									            <div class="modal-footer">
								                	<button type="submit" class="btn btn-primary">Responder</button>
									                <?php echo form_close(); ?>
									            </div>
									        </div>
									    </div>
									</div>
									<?php
									} ?>
							      </div>
							    </div>
							  </div>
						  <?php
						$i++;
					}
					if($i == 0){
						echo "<small><center>Nenhuma mensagem!<br />Envie uma mensagem entrando no currículo de uma pessoa e então clique em \"<span class='text-info'>Enviar Mensagem</span>\"</center></small>";
					}
					echo "<div style='clear: both;'";
					echo $paginacao;
				?>
			</div>
		</div>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<div style='clear: both'></div>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		
	</body>
</html>