<html>
	<head>
		<title>Currículo Online - Criar currículo e procurar emprego grátis</title>
		<?php $this->load->view('inc/headBasico.php');?>

		<meta property="og:locale" content="pt_BR">
		 
		<meta property="og:url" content="http://www.meusite.com.br/ola-mundo">
		 
		<meta property="og:title" content="Currículo Online">
		<meta property="og:site_name" content="Currículo Online">
		 		 
		<meta property="og:image" content="<?php echo base_url(); ?>img/imagem_redes_sociais.jpg"/>
		<meta property="og:image:type" content="image/jpeg">
		<meta property="og:image:width" content="850">
		<meta property="og:image:height" content="850">
					 
		<meta property="og:type" content="website">
		<meta name='description' content="Crie seu currículo totalmente grátis e envie para empresas de sua região. Procure vagas de emprego sem sair de casa com seu Currículo Online." />
		<meta property='og:description' content="Crie seu currículo totalmente grátis e envie para empresas de sua região. Procure vagas de emprego sem sair de casa com seu Currículo Online." />
	</head>
	<body>
		<div class="row topo">
			<?php $this->load->view('inc/topo.php');?>
			<?php $this->load->view('inc/menuSuperior.php');?>
		</div>

		<?php echo $this->session->flashdata("confirmado"); ?>
		
		<?php $this->load->view('inc/botoesCadastro.php');?>
		
		<?php $this->load->view('inc/anuncioDireita.php');?>

		<?php $this->load->view('inc/anuncioMeio.php');?>

		<?php //$this->load->view('inc/cidades.php');?>

		<?php $this->load->view('inc/painelDireitaBaixo.php');?>
		
		<div class='col-lg-1 col-md-2 col-sm-2 col-xs-2'></div>
		<?php $this->load->view('inc/painelEsquerdaBaixo.php');?>
		<?php $this->load->view('inc/anuncioBaixo.php');?>
		<?php $this->load->view('inc/lista_empresas_home.php');?>
		<div style='clear: both'></div>
		
	</body>
</html>