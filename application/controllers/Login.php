<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Login_model', 'login');
    }

    function index() {

        $this->form_validation->set_rules('login', 'Login', 'required|addslashes', array('required'=>'Preencha o campo de LOGIN!'));
        $this->form_validation->set_rules('senha', 'Senha', 'required', array('required'=>'Preencha o campo da SENHA!'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        
        if($this->form_validation->run() == TRUE){
            $query = $this->login->validate();
            if($query != FALSE){
                $this->session->set_userdata(array(
                    'id'=> $query->id,
                    'login'=> $query->login,
                    'cidade'=> $query->cidade,
                    'idCidade'=> $query->idCidade,
                    'nome'=> $query->nome,
                    'sobrenome'=> $query->sobrenome,
                    'logado'=> true,
                    'email'=> $query->email,
                    ));
                if($this->input->post('pagina') != null && $this->input->post('pagina') != "")
                    redirect($this->input->post('pagina'));
                else
                    redirect(base_url());
            }else{
                redirect(base_url().'Login/erro/1');
            }
        }else{
            $this->load->view('login');
        }
    }

    function erro(){
        $dados = array(
            'codigo_erro'=> $this->uri->segment(3),
            'erro'=>true,
            );
        $this->load->view('login', $dados);
    }
}