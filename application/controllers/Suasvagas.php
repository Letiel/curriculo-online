<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Suasvagas extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Login_empresa_model', 'login');
			$this->load->model('Criar_vaga_model', 'vaga');

			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		}

		public function index(){
			$dados = array(
				'pagina'=>'suasVagas',
				'vagas'=>$this->vaga->ver_vagas_empresa()->result(),
				'areas'=>$this->vaga->cata_areas()->result(),
			);
			$this->load->view("suas_vagas", $dados);
		}

		public function editable(){
			$name = $_POST['name'];
			$value = $_POST['value'];
			$pk = $_POST['pk'];
			if($name == "cargo"){
				$this->form_validation->set_rules('value', 'Cargo', 'required|addslashes|max_length[50]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 50 caracteres.'));
			}else if($name == "descricao"){
				$this->form_validation->set_rules('value', 'Descrição', 'required|addslashes', array('required'=>'Informe uma descrição para a vaga!'));
			}

			if($this->form_validation->run() == TRUE){
				$this->db->query("UPDATE vagas SET $name = '$value' WHERE id = $pk");
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
				// $result = array('success'=>false, 'msg'=>$name." - ". $value);
			}

			echo json_encode($result);
		}

		public function cadastro(){
			$this->login->loggedEmpresa();
			$this->form_validation->set_rules('descricao', 'Descri&ccedil;&atilde;o', 'required|min_length[10]|addslashes');
			$this->form_validation->set_rules('cargo', 'Cargo', 'required|min_length[3]|addslashes');
			$this->form_validation->set_rules('area', 'Área', 'required|addslashes');

			$this->form_validation->set_message('required', 'O campo %s é obrigatório!');

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('descricao', 'cargo', 'area'), $this->input->post());
				$dados['empresa'] = $this->session->userdata('idEmpresa');
				$this->vaga->criaVaga($dados);
				$this->session->set_flashdata('cadastroVaga', "<div class='alert alert-success alert-dismissible' role='alert'>
															  <strong>Sucesso!</strong><br />Sua vaga foi cadastrada com sucesso.
															</div>");
				redirect('/vagas');
			}

			$dados = array(
				'areas'=>$this->vaga->cata_areas()->result(),
				'vagas_empresa'=>$this->vaga->ver_vagas_empresa()->result(),
				);

			$this->load->view('criar_vaga', $dados);
		}

		function cadastra_nova_area(){
			$dados = elements(array('nome_area'), $this->input->post());
			$this->vaga->cadastra_area($dados);
		}

		function atualiza_areas(){
			$areas = $this->vaga->cata_areas()->result();
			echo "<option value=''>Selecione...</option>";
			foreach($areas as $area){
				echo "<option value='$area->id'>$area->descricao</option>";
			}
		}

		function deleta_vaga(){
			$dados = elements(array('id'), $this->input->post());
			$this->vaga->deleta_vaga_empresa($dados['id']);
		}
	}