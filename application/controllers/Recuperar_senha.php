<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Recuperar_senha extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Recuperar_senha_model', 'recuperar_senha');
        
        if($this->session->userdata('logadoEmpresa')){
            $this->load->model('Login_empresa_model', 'login_empresa');
            $this->login_empresa->verificaMensagens();
        }else{
            if($this->session->userdata('logado')){
                $this->load->model('Login_model', 'login_user');
                $this->login_user->verificaMensagens();
            }
        }
    }

    function index() {

        $this->form_validation->set_rules('login', 'Login', 'required|addslashes', array('required'=>'Preencha o campo de <b>E-mail</b>!'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if($this->form_validation->run() == TRUE){
            $this->recuperar_senha->validate();
        }
        $this->load->view("recuperar_senha");   

        //redirect(Login);
    }
}