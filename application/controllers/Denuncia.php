<?php
class Denuncia extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){

		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array('required'=>'Preencha o campo E-mail.', 'valid_email'=>'E-mail inválido.'));
		$this->form_validation->set_rules('motivo', 'Motivo', 'required', array('required'=>'Preencha o campo E-mail.'));

		if($this->form_validation->run() == TRUE){
			$dados = elements(array('email', 'motivo', 'url'), $this->input->post());

			$message = "
				Denúncia! <br />
				Email: <b>$dados[email]</b><br />
				Motivo: <b>$dados[motivo]</b><br />
				URL: <b>$dados[url]</b><br />
			";
			$subject = 'Denúncia - Currículo Online';
			$this->load->model('Imail', 'mail');
			$this->mail->enviar($subject,  "letiel@letiel.com.br", $message);
			echo "<div class='alert alert-success'>Sua denúncia foi enviada e será avaliada pelos administradores. Obrigado!</div>";
		} else {
			echo "<div class='alert alert-danger'>".validation_errors()."</div>";
		}
	}
}