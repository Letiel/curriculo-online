<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class VerMensagem extends CI_Controller{
		function __construct(){
			parent::_construct();
			$this->load->model('Cadastro_usuario_model', 'mensagemUser');
			$this->load->model('Cadastro_empresa_model', 'mensagemEmpresa');

			if($this->session->userdata('logadoEmpresa')){
			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
			}else{
				if($this->session->userdata('logado')){
					$this->load->model('Login_model', 'login_user');
					$this->login_user->verificaMensagens();
				}
			}
		
		}

		public function index(){
			redirect('<?php echo base_url(); ?>Mensagens');
		}
	}