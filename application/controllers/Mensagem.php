<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Mensagem extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Mensagem_model', 'mensagem');
			$this->load->model('cadastro_empresa_model', 'empresa');

			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		}

		public function empresa(){

			$this->form_validation->set_rules('mensagem', 'Mensagem', 'required|min_length[2]|max_length[500]|addslashes', array('required'=>'Preencha a mensagem'));

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('mensagem', 'usuario', 'id_mensagem'), $this->input->post());
				$this->mensagem->mensagem_empresa($dados);
				redirect("Mensagens");
			}

			$dados = array(
				'usuario'=>$this->uri->segment(3),
				'nome'=>$this->uri->segment(4),
				);

			$this->load->view('enviar_mensagem_empresa', $dados);
		}

		public function sucesso(){
			$dados = array(
				'usuario'=>$this->uri->segment(3),
				'nome'=>$this->uri->segment(4),
				);
			$this->load->view('enviar_mensagem_empresa', $dados);	
		}

		public function usuario(){
			$this->form_validation->set_rules('mensagem', 'Mensagem', 'required|min_length[2]|max_length[500]|addslashes', array('required'=>'Preencha a mensagem'));

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('mensagem', 'empresa', 'id_mensagem'), $this->input->post());
				$this->mensagem->mensagem_usuario($dados);
				redirect("Mensagens");
			}
		}

		public function ler(){
			$dados = elements(array('mensagem'), $this->input->post());
			if($dados != null){
				$this->mensagem->ler($dados);
			}
		}
	}