<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Confirma extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Cadastro_pessoa_model', 'cadastro');
			$this->load->model('Cadastro_empresa_model', 'cadastro_empresa');
		}

		public function index(){
			redirect('cadastroPessoa');
		}

		public function user(){
			$user = $this->uri->segment(3);
			$dados = array(
				'codigo'=>$user,
				);
			$this->cadastro->confirmaEmail($dados);
			$this->session->set_flashdata('confirmado', '<div class="alert alert-success">Seu e-mail foi confirmado com sucesso!<br />Faça login e complete seu cadastro.</div>');
			$this->load->view('login');
		}

		public function email(){
			$this->load->view('login');
		}

		public function empresa(){
			$user = $this->uri->segment(3);
			$dados = array(
				'codigo'=>$user,
				);
			$this->cadastro_empresa->confirmaEmail_empresa($dados);
			$this->session->set_flashdata('confirmado', '<div class="alert alert-success">Seu e-mail foi confirmado com sucesso!<br />Faça login e complete seu cadastro.</div>');
			$this->load->view('loginEmpresa');
		}

	}