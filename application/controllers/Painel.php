<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Painel extends CI_Controller{
	public function index(){
		$this->load->view(base_url());
		
		if($this->session->userdata('logadoEmpresa')){
			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		}else{
			if($this->session->userdata('logado')){
				$this->load->model('Login_model', 'login_user');
				$this->login_user->verificaMensagens();
			}
		}
	}
}