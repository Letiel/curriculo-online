<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	class Pesquisa extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Pesquisa_model', 'pesquisa');
		}

		function filtrar(){
			$this->form_validation->set_rules('campo_pesquisa_filtro', 'Pesquisa', 'addslashes');
			$this->form_validation->set_rules('cidade', 'Cidade', 'addslashes|numeric');
			$this->form_validation->set_rules('tipo', 'Tipo de pesquisa', 'addslashes|trim');
			if($this->form_validation->run()){
				$data = elements(array('campo_pesquisa_filtro', 'cidade', 'tipo', 'pag'), $this->input->post());
				$inicio = $data['pag'];
				$maximo = 10;
				$resultados = $this->pesquisa->pesquisa_filtrada($data['campo_pesquisa_filtro'], $data['tipo'], $data['cidade'], $inicio, $maximo)->result();
				foreach($resultados as $resultado){
					switch ($tipo){
						case 'vagas':
							foreach($resultados as $vaga){
								if($vaga->especial == 1) {
									$uri = str_replace($what, $by, $vaga->titulo_especial);
									if($vaga->imagem_especial != NULL && $vaga->imagem_especial != "")
										$imagem = "/img/thumbs/vagas/$vaga->imagem_especial";
									else
										$imagem = "/img/img_padrao_vaga.png";
									echo "<div class='col-xs-12 item-lista' style='margin: 1% 0%; border-bottom: 2px solid #D9D9F3; overflow: hidden; padding-bottom: 2%'>
										<a href='".base_url()."Vaga/ver/$vaga->id/$uri'>
									      <img class='col-xs-6 col-sm-4 img-thumbnail pull-left' src='$imagem' alt='Vaga de $vaga->titulo_especial.'>
									      <div class='col-xs-6 col-sm-8 pull-left'>
										      <h4 style='float: left;'>$vaga->titulo_especial</h4>
										      <h5 style='clear: left;'>$vaga->empresa em $vaga->cidade_especial_nome, $vaga->uf_estado_especial</h5>
										  </div>
									    </a></div>";
								} else {
									$uri = str_replace($what, $by, $vaga->cargo);
									if($vaga->imagem != NULL && $vaga->imagem != "")
										$imagem = "/img/thumbs/vagas/$vaga->imagem";
									else
										$imagem = "/img/thumbs/vagas/$vaga->img_padrao_vaga.png";
									echo "<div class='col-xs-12 item-lista' style='margin: 1% 0%; border-bottom: 2px solid #D9D9F3; overflow: hidden; padding-bottom: 2%'>
										<a href='".base_url()."Vaga/ver/$vaga->id/$uri'>
									      <img class='col-xs-6 col-sm-4 img-thumbnail pull-left' src='$imagem' alt='Vaga de $vaga->empresa.'>
									      <div class='col-xs-6 col-sm-8 pull-left'>
										      <h4 style='float: left;'>$vaga->cargo</h4>
										      <h5 style='clear: left;'>$vaga->empresa em $vaga->cidade, $vaga->uf</h5>
										  </div>
									    </a></div>";
								}
							}
							break;
						case 'usuarios':
							foreach($resultados as $curriculo){
								$nome = $curriculo->nome;
								$nome .= " ".$curriculo->sobrenome;

								    
								$uri = str_replace($what, $by, $nome);

								echo "<div class='col-xs-12 item-lista'>
								<a href='".base_url()."curriculo/ver/$curriculo->id/$uri'>
							      <img class='img-thumbnail col-xs-6 col-sm-4' alt='Ver currículo de $curriculo->nome $curriculo->sobrenome' src='$curriculo->foto' alt='Visualizar perfil de $curriculo->nome'>
							      <div class='col-xs-6 col-sm-8 pull-left'>
								      <h4 style='float: left;'>$curriculo->nome $curriculo->sobrenome</h4>
								      <h5 style='clear: left;'>em $curriculo->nome_cidade, $curriculo->uf</h5>
								  </div>
							    </a></div>";
							}
							break;
						case 'empresas':
							foreach($resultados as $empresa){
								$uri = str_replace($what, $by, $empresa->nome);

								$lugar = "";

								if($empresa->nome_cidade != NULL && $empresa->nome_cidade != ""){
									$lugar.=$empresa->nome_cidade;
								}

								if($empresa->uf != NULL && $empresa->uf != ""){
									$lugar.= ", ".$empresa->uf;
								}

								echo "<div class='col-xs-12 item-lista'>
								<a href='".base_url()."empresa/ver/$empresa->id/$uri'>
							      <img class='img-thumbnail col-xs-6 col-sm-4 pull-left' src='$empresa->imagem' alt='Visualizar perfil de $empresa->nome'>
							      <div class='col-xs-6 col-sm-8 pull-left'>
								      <h4 style='float: left;'>$empresa->nome</h4>
								      <h5 style='clear: left;'>em $lugar</h5>
								  </div>
							    </a></div>";
							}
							break;
					}
				}
			}
		}

		function lista_cidades(){
			$this->load->model("Cidades", 'cidades');
			$pesquisa = $this->input->post("term");
			echo json_encode($this->cidades->cidade_select2($pesquisa)->result());
		}

		function index(){
			$this->load->model("Cidades", 'cidades');
			$this->form_validation->set_rules('campo_pesquisa_filtro', 'Pesquisa', 'addslashes');
			$this->form_validation->set_rules('cidade', 'Cidade', 'addslashes|numeric');
			$this->form_validation->set_rules('tipo', 'Tipo de pesquisa', 'addslashes|trim');
			if($this->form_validation->run()){
				$data = elements(array('campo_pesquisa_filtro', 'cidade', 'tipo'), $this->input->post());
				$maximo = 10;
				$inicio = (!$this->uri->segment("2")) ? 0 : $this->uri->segment("2");
				$config['base_url'] = '/pesquisa/filtrar';
				$config['per_page'] = $maximo;
				$config['first_link'] = '<<';
				$config['last_link'] = '>>';
				$config['next_link'] = '>';
				$config['prev_link'] = '<';   
				$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
				$config['full_tag_close'] = '</ul></nav>';
				$config['cur_tag_open'] = '<li class="active"><a >';
				$config['cur_tag_close'] = '</a></li>';
				$config['num_tag_open'] = '<li class="num_pag">';
				$config['num_tag_close'] = '</li>';
				$config['next_tag_open'] = '<li class="proxima">';
				$config['next_tag_close'] = '</li>';
				$config['prev_tag_open'] = '<li class="anterior">';
				$config['prev_tag_close'] = '</li>';
				$config['last_tag_open'] = '<li class="ultima">';
				$config['last_tag_close'] = '</li>';
				$config['first_tag_open'] = '<li class="primeira">';
				$config['first_tag_close'] = '</li>';

				$config['total_rows'] = $this->pesquisa->pesquisa_filtrada($data['campo_pesquisa_filtro'], $data['tipo'], $data['cidade'])->num_rows();
				$this->pagination->initialize($config);
				$tipo = $data['tipo'];
				$resultados = $this->pesquisa->pesquisa_filtrada($data['campo_pesquisa_filtro'], $data['tipo'], $data['cidade'], $inicio, $maximo)->result();
				$paginacao = $this->pagination->create_links();
				if($data['cidade'] != NULL && $data['cidade'] != "")
					$cidade = $this->cidades->nome_cidade($data['cidade'])->first_row();
				else
					$cidade = NULL;
			}else{
				$resultados = $this->pesquisa->pesquisa_filtrada()->result();
				$tipo = NULL;
				$paginacao = NULL;
				$cidade = NULL;
			}
			$dados = array(
				'pagina'=>'pesquisa',
				'cidade'=>$cidade,
				'paginacao'=>$paginacao,
				'tipo'=>$tipo,
				'cidades'=>$this->cidades->get_cidades()->result(),
				'resultados'=>$resultados,
			);
			$this->load->view("pesquisa_filtrada", $dados);
		}

		function vagas(){
			$this->form_validation->set_rules('campo_pesquisa_filtro', 'Pesquisa', 'addslashes|ucfirst');
			$dados = elements(array('campo_pesquisa_filtro'), $this->input->post());


			$maximo = 10;
			$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

			//paginacao------------------
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['base_url'] = '/Pesquisa/vagas';
			$config['per_page'] = $maximo;
			//paginacao------------------
			

			if($dados['campo_pesquisa_filtro'] == '' || $dados['campo_pesquisa_filtro'] == NULL)
				$dados['campo_pesquisa_filtro'] = $this->session->flashdata('pesquisa');
			$config['total_rows'] = $this->pesquisa->retorna_vagas_all($dados['campo_pesquisa_filtro'])->num_rows();
			$this->pagination->initialize($config);


			$dados['vagas'] = $this->pesquisa->retorna_vagas($dados['campo_pesquisa_filtro'], $maximo, $inicio)->result();
			$dados['pagina'] = "Vagas";
			$dados['paginacao'] = $this->pagination->create_links();
			$this->load->view('resultado_pesquisa', $dados);
			$this->session->set_flashdata('pesquisa', $dados['campo_pesquisa_filtro']);
		}

		function empresas(){
			$dados = elements(array('campo_pesquisa_filtro'), $this->input->post());
			if($dados['campo_pesquisa_filtro'] == '' || $dados['campo_pesquisa_filtro'] == NULL)
				$dados['campo_pesquisa_filtro'] = $this->session->flashdata('pesquisa');
			$dados['empresas'] = $this->pesquisa->retorna_empresas($dados['campo_pesquisa_filtro'])->result();
			$dados['pagina'] = "Empresas";
			$dados['paginacao'] = "";
			$this->load->view('resultado_pesquisa', $dados);
			$this->session->set_flashdata('pesquisa', $dados['campo_pesquisa_filtro']);
		}

		function curriculos(){

			$maximo = 10;
			$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

			//paginacao------------------
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['base_url'] = '/Pesquisa/curriculos';
			$config['per_page'] = $maximo;
			//paginacao------------------

			$dados = elements(array('campo_pesquisa_filtro'), $this->input->post());
			if($dados['campo_pesquisa_filtro'] == '' || $dados['campo_pesquisa_filtro'] == NULL)
				$dados['campo_pesquisa_filtro'] = $this->session->flashdata('pesquisa');
			$config['total_rows'] = $this->pesquisa->retorna_curriculos_all($dados['campo_pesquisa_filtro'])->num_rows();
			$this->pagination->initialize($config);

			$dados['curriculos'] = $this->pesquisa->retorna_curriculos($dados['campo_pesquisa_filtro'], $maximo, $inicio)->result();
			$dados['pagina'] = "Currículos";
			$dados['paginacao'] = $this->pagination->create_links();

			$this->load->view('resultado_pesquisa', $dados);
			$this->session->set_flashdata('pesquisa', $dados['campo_pesquisa_filtro']);
		}
	}