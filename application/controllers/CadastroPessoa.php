<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CadastroPessoa extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model("cadastro_pessoa_model");
		$this->load->model('Cidades', 'cidades');
	}

	public function index(){
		$this->form_validation->set_rules('nome', 'nome', 'required|min_length[0]|max_length[50]|trim|ucwords|addslashes', array('required'=>'Preencha o campo Nome'));
		$this->form_validation->set_rules('sexo', 'Sexo', 'required|min_length[0]|max_length[50]|trim|ucwords|addslashes', array('required'=>'Preencha o campo Sexo'));
		$this->form_validation->set_rules('sobrenome', 'sobrenome', 'required|min_length[0]|max_length[50]|trim|ucwords|addslashes', array('required'=>'Preencha o campo Sobrenome'));
		function valid_date($date, $format = 'dd/mm/yyyy'){
			
		   $dateArray = explode("/", $date); // slice the date to get the day, month and year separately

		   $d = 0;
		   $m = 0;
		   $y = 0;
		   if (sizeof($dateArray) == 3) {
		   	if (is_numeric($dateArray[0]))
		   		$d = $dateArray[0];
		   	if (is_numeric($dateArray[1]))
		   		$m = $dateArray[1];
		   	if (is_numeric($dateArray[2]))
		   		$y = $dateArray[2];
		   }

		   return checkdate($m, $d, $y) == 1;
		}
		$this->form_validation->set_rules('dt_nascimento', 'data de nascimento', 'required|valid_date', array('required'=>'Preencha o campo Data de nascimento', 'valid_date'=>'Informe uma data v&aacute;lida!'));
		$this->form_validation->set_rules('email', 'email', 'required|min_length[0]|max_length[50]|trim|valid_email|is_unique[usuarios.email]|addslashes', array('required'=>'Preencha o campo e-mail', 'is_unique'=>'E-mail j� cadastrado!', 'valid_email'=>"Preencha um e-mail v�lido!"));
		$this->form_validation->set_rules('login', 'login', 'required|min_length[0]|max_length[50]|trim|is_unique[usuarios.login]|strtolower|addslashes', array('required'=>'Preencha o campo login', 'is_unique'=>'Login j&aacute; presente no sistema!'));
		$this->form_validation->set_rules('cidade', 'Cidade!', 'required|numeric', array('required'=>'Selecione uma cidade!', 'numeric'=>'Selecione uma cidade!'));
		$this->form_validation->set_rules('senha', 'senha', 'required|min_length[0]|max_length[50]|strtolower', array('required'=>'Preencha o campo senha'));
		$this->form_validation->set_rules('senha2', 'senha', 'required|min_length[0]|max_length[50]|matches[senha]|strtolower', array('required'=>'Preencha o campo de confirma&ccedil;&atilde;o de senha', 'matches'=>'As senhas n�o conferem!'));
		if($this->form_validation->run() == TRUE){
			
			$dados = elements(array('nome', 'sobrenome', 'cidade', 'email', 'login', 'senha', 'dt_nascimento', 'sexo'), $this->input->post());
			$dados['senha'] = md5($dados['senha']);
			$dados['codigo'] = uniqid();

			$this->cadastro_pessoa_model->cadastrar($dados);
			
		}
		$estados = array(
			'pagina'=>'CadastroPessoa',
			'estados' => $this->cidades->get_estados()->result(),
			'cidades' => $this->cidades->get_cidades()->result(),
			);
		$this->load->view('cadastroPessoa', $estados);
	}
	
	
}