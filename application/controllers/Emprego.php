<?php

class Emprego extends CI_Controller{
	function __construct(){
		parent::__construct();
	}
	function index(){
		$dominios = array(
			"curriculoonline.net.br",
			"vagasem.com.br/home",
			// "icurriculumvitae.com.br/emprego/ver",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}

	public function ver(){
		$dominios = array(
			"icurriculumvitae.com.br/emprego/empregos",
			"icurriculumvitae.com.br/emprego/nestle",
			"icurriculumvitae.com.br/emprego/atacadao",
			"icurriculumvitae.com.br/emprego/ambev",
			"icurriculumvitae.com.br/emprego/cacaushow",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}

	public function vagasem(){
		$dominios = array(
			"vagasem.com.br/home",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}

	public function empregos(){
		$dominios = array(
			"vagasdeempregoja.com.br",
			"dicasempregos.com.br",
			"lebou.com.br",
			"procurarempregos.com.br",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}

	public function nestle(){
		$dominios = array(
			"trabalhenestle.com.br",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}

	public function atacadao(){
		$dominios = array(
			"atacadaovagas.com",
			"atacadaovagas.com.br",
			"empregosatacadao.com.br",
			"trabalheatacadao.com.br",
			"trabalhoatacadao.com.br",
			"vagasatacadao.com.br",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}

	public function ambev(){
		$dominios = array(
			"ambevempregos.com.br",
			"ambevemprego.com.br",
			"ambevempregos.com.br",
			"ambevvagas.com.br",
			"empregoambev.com.br",
			"empregosambev.com.br",
			"vagasambev.com.br",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}

	public function cacaushow(){
		$dominios = array(
			"empregoscacaushow.com.br",
			"trabalhecacaushow.com.br",
			"trabalhocacaushow.com.br",
			"vagascacaushow.com.br",
			);
		redirect("http://www.".$dominios[rand(0, sizeof($dominios)-1)]);
	}
}