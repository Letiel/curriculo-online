<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Mensagens extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Cadastro_empresa_model', 'empresa');
			$this->load->model('Cadastro_pessoa_model', 'pessoa');

			if($this->session->userdata('logadoEmpresa')){
			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
			}else{
				if($this->session->userdata('logado')){
					$this->load->model('Login_model', 'login_user');
					$this->login_user->verificaMensagens();
				}
			}
		}

		function index(){
			redirect('Mensagens/ver');
		}

		function ver(){
			$maximo = 10;
			$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

			$config['base_url'] = '/Mensagens/ver';
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';


			if($this->session->userdata('logadoEmpresa')){
				$config['total_rows'] = $this->empresa->get_mensagens_all()->num_rows();
				$this->pagination->initialize($config);
				$dados = array(
					'pagina'=>'mensagens',
					'mensagens'=> $this->empresa->get_mensagens($maximo, $inicio)->result(),
					'paginacao'=> $this->pagination->create_links(),
					);
				$this->load->view('mensagens_empresa', $dados);
			}else{
				if($this->session->userdata('logado')){
					$config['total_rows'] = $this->pessoa->get_mensagens_all()->num_rows();
					$this->pagination->initialize($config);
					$dados = array(
						'pagina'=>'mensagens',
						'mensagens'=>$this->pessoa->get_mensagens($maximo, $inicio)->result(),
						'paginacao'=> $this->pagination->create_links(),
						);
					$this->load->view('mensagens', $dados);
				}
			}
		}
	}