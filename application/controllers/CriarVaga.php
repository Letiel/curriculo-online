<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class CriarVaga extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Login_empresa_model', 'login');
			$this->load->model('Criar_vaga_model', 'vaga');

			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		}

		public function index(){
			$this->login->loggedEmpresa();
			$this->form_validation->set_rules('area', 'Área', 'required|addslashes');
			$this->form_validation->set_rules('descricao', 'Descri&ccedil;&atilde;o', 'required|min_length[10]|addslashes', array("min_length"=>"O campo descrição precisa de pelo menos 10 caracteres.", "required"=>"Informe uma descrição para a vaga."));
			$this->form_validation->set_rules('cargo', 'Cargo', 'required|min_length[3]|addslashes');

			$this->form_validation->set_message('required', 'O campo %s é obrigatório!');

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('descricao', 'cargo', 'area'), $this->input->post());
				$dados['empresa'] = $this->session->userdata('idEmpresa');
				$this->vaga->criaVaga($dados);
				$this->session->set_flashdata('cadastroVaga', "<div class='alert alert-success alert-dismissible' role='alert'>
															  <strong>Sucesso!</strong><br />Sua vaga foi cadastrada com sucesso.
															</div>");
				// redirect('/suasvagas');
				echo "sucesso";
			} else {
				echo validation_errors();
			}

			// $dados = array(
			// 	'areas'=>$this->vaga->cata_areas()->result(),
			// 	'vagas_empresa'=>$this->vaga->ver_vagas_empresa()->result(),
			// 	);

			// $this->load->view('criar_vaga', $dados);
		}

		function cadastra_nova_area(){
			$this->form_validation->set_rules('nome_area', 'Nome da área', 'required|addslashes|trim|is_unique[areas.descricao]', array("is_unique"=>"Área já existente.", 'required'=>'Informe um nome para a área.'));
			if($this->form_validation->run() == TRUE) {
				$dados = elements(array('nome_area'), $this->input->post());
				$this->vaga->cadastra_area($dados);
				echo "sucesso";
			} else {
				echo validation_errors();
			}
		}

		function atualiza_areas(){
			$areas = $this->vaga->cata_areas()->result();
			echo "<option value=''>Selecione...</option>";
			foreach($areas as $area){
				echo "<option value='$area->id'>$area->descricao</option>";
			}
		}

		function deleta_vaga(){
			$dados = elements(array('id'), $this->input->post());
			$this->vaga->deleta_vaga_empresa($dados['id']);
		}

		function alterar_area_vaga(){
			$this->form_validation->set_rules('id_vaga', 'Área', 'required|addslashes|trim', array('required'=>'Ocorreu um erro.'));
			$this->form_validation->set_rules('nova_area', 'Área', 'required|addslashes|trim', array('required'=>'Informe uma área.'));
			if($this->form_validation->run() == TRUE) {
				$dados = elements(array('id_vaga', 'nova_area'), $this->input->post());
				$this->vaga->alterar_area_vaga($dados);
				echo "sucesso";
			} else {
				echo validation_errors();
			}
		}
	}