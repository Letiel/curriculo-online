<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CadastroEmpresa extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('cadastro_empresa_model');
		$this->load->model('Cidades', 'cidades');
	}

	public function index(){
		$this->form_validation->set_rules('nome', 'nome', 'required|min_length[0]|max_length[50]|trim|ucwords|addslashes', array('required'=>'Preencha o campo Nome', 'is_unique'=>'Nome j&aacute; existente no sistema!'));
		$this->form_validation->set_rules('cnpj', 'cnpj', 'required|min_length[0]|max_length[50]|is_unique[empresas.cnpj]|trim|addslashes|valid_cnpj', array('required'=>'Preencha o campo CNPJ', 'is_unique'=>'CNPJ j&aacute; cadastrado', 'valid_cnpj'=>'CNPJ inv&aacute;lido!'));
		$this->form_validation->set_rules('email', 'email!', 'required|min_length[0]|max_length[50]|trim|valid_email|is_unique[empresas.email]|addslashes', array('required'=>'Preencha o campo e-mail', 'is_unique'=>'E-mail já cadastrado!', 'valid_email'=>"Preencha um e-mail válido!"));
		$this->form_validation->set_rules('login', 'login!', 'required|min_length[0]|max_length[50]|trim|is_unique[empresas.login]|strtolower|addslashes', array('required'=>'Preencha o campo NOME DE USU&Aacute;RIO', 'is_unique'=>'Login já presente no sistema!'));
		$this->form_validation->set_rules('cidade', 'Cidade!', 'required|numeric', array('required'=>'Selecione uma cidade!', 'numeric'=>'Selecione uma cidade!'));
		$this->form_validation->set_rules('senha', 'senha!', 'required|min_length[0]|max_length[50]|strtolower', array('required'=>'Preencha o campo senha'));
		$this->form_validation->set_rules('senha2', 'senha!', 'required|min_length[0]|max_length[50]|matches[senha]|strtolower', array('required'=>'Preencha o campo de confirmação de senha', 'matches'=>'As senhas não conferem!'));

		if($this->form_validation->run() == TRUE){
			$dados = elements(array('nome', 'email', 'login', 'senha', 'cnpj', 'cidade'), $this->input->post());
			$dados['senha'] = md5($dados['senha']);
			$dados['codigo'] = uniqid();
			$this->cadastro_empresa_model->cadastrar($dados);
		}

		$estados = array(
			'pagina'=>'CadastroEmpresa',
			'estados' => $this->cidades->get_estados()->result(),
			);

		$this->load->view('cadastroEmpresa', $estados);
	}
}