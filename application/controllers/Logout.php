<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Logout extends CI_Controller{
		public function index(){
			$this->session->unset_userdata('logged');
			$this->session->unset_userdata('username');
		    $this->session->sess_destroy();
		    redirect(base_url());
		}
	}