<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Adm extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("Adm_model", "adm");
			$this->load->model("Empresa", "empresa");
			$this->load->model("Cidades", "cidade");
		}

		function logado(){
			if(!$this->session->userdata("logadoAdm")){
				redirect(base_url()."Adm/login");
			}
		}

		function logout(){
			$this->session->unset_userdata('logadoAdm');
			$this->session->unset_userdata('login');
			$this->session->unset_userdata('id');
		    $this->session->sess_destroy();
		    redirect(base_url().'adm/login');
		}

		function login(){
			$this->form_validation->set_rules('login', 'Login', 'required|trim');
			$this->form_validation->set_rules('senha', 'Senha', 'md5|required');

			$this->form_validation->set_message('required', 'O campo "%s" é obrigat&oacute;rio!');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

			$dados = elements(array('login', 'senha'), $this->input->post());
			if($this->form_validation->run() == TRUE){
				$dados = elements(array('login', 'senha'), $this->input->post());
				if($this->adm->logar($dados)->num_rows() == 1){
					$login = $this->adm->logar($dados)->result();
					$login = $login[0];
					$this->session->set_userdata(array(
                    'id'=> $login->id,
                    'login'=> $login->login,
                    'logadoAdm'=> true
                    ));
				}else{
					$this->session->set_flashdata("Erro", "<div class='alert alert-danger'><h3>Usuário e/ou senha incorretos!</h3></div>");
				}
			}

			if(!$this->session->userdata("logadoAdm")){
				$this->load->view("loginAdm");
			}else{
				redirect(base_url()."Adm/home");
			}
		}

		function home(){
			$this->logado();
			echo "O que você está fazendo aqui?";
		}

		function cadastrar_empresa(){
			$this->logado();

			$this->form_validation->set_rules('nome', 'Nome', 'trim|ucfirst|addslashes|required');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|addslashes|required');
			$this->form_validation->set_rules('site', 'Website', 'trim|addslashes|prep_url');
			$this->form_validation->set_rules('endereco', 'Endere&ccedil;o', 'addslashes');
			$this->form_validation->set_rules('bairro', 'Bairro', 'trim');
			$this->form_validation->set_rules('cidade', 'Cidade', 'trim');
			$this->form_validation->set_rules('textempresa', 'Texto de Apresenta&ccedil;&atilde;o', 'addslashes|required');
			$this->form_validation->set_rules('cep', 'CEP', 'addslashes|trim');
			

			$config['upload_path'] = './img/thumbs/empresas';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '10000';
			$config['max_width'] = '1920';
			$config['max_height'] = '1080';
			$nome = uniqid('foto_');
			$config['file_name'] = $nome;

			$this->load->library('upload', $config);


			if($this->form_validation->run() == TRUE){
				$dados = elements(array('nome', 'email', 'endereco', 'bairro', 'cidade', 'textempresa', 'site', 'cep', 'cnpj'), $this->input->post());
				$dados['codigo'] = uniqid();
				$this->adm->cadastraEmpresa($dados);
				$nome_foto = $_FILES['foto']['name'];
				if($nome_foto != NULL && $nome_foto != ''){
					$ext = explode('.', $nome_foto);
					$foto = $this->upload->data();
					$this->upload->do_upload('foto');
					$this->adm->atualiza_foto(base_url().'img/thumbs/empresas/'.$foto['file_name'].'.'.$ext[1]);
				}
			}

			$estados = $this->cidade->get_estados()->result();
			$cidades = $this->cidade->get_cidades()->result();
			$dados = array(
				'estados'=>$estados,
				'cidades'=>$cidades,
				);

			$this->load->view("adm_cadastro_empresa", $dados);

		}

		function imagens(){
			$this->logado();
			$dados = array(
				'empresas'=>$this->adm->carrega_empresas()->result(),
				);

			$usuarios = $this->adm->carrega_usuarios()->result();

			require('wideimage/WideImage.php');
			foreach($usuarios as $usuario){
				$nome_imagem = str_replace(base_url(), '', $usuario->foto);
				$nome_imagem = str_replace('/img', '', $nome_imagem);
				$nome_imagem = str_replace('thumbs', '', $nome_imagem);
				$nome_imagem = str_replace('empresas', '', $nome_imagem);
				$nome_imagem = str_replace('/', '', $nome_imagem);
				$nome_imagem = explode('.', $nome_imagem);
				$ext = $nome_imagem[1];
				$nome = $nome_imagem[0];
				if($nome != "avatar-padrao"){
					$image = wideImage::load($usuario->foto);
					$image = $image->resize(200, 200, 'outside');
					$file = "img/thumbs/".$nome."."."$ext";
					$image->saveToFile($file);
					echo "$file<br />";
					$image->destroy();
					//$this->adm->update_foto($empresa->id, "/img/thumbs/empresas/".$nome."_crop.".$ext);
				}
			}
			//$this->load->view("adm_imagens", $dados);
		}

		function email_massa(){
			ini_set('max_execution_time', 60000);
			$this->logado();

			$this->form_validation->set_rules('cidade', 'Cidade', 'required');
			$this->form_validation->set_rules('mensagem', 'Mensagem', 'required');

			if($this->form_validation->run()){
				$dados = elements(array('cidade', 'mensagem'), $this->input->post());
				$cidade = $dados['cidade'];
				$mensagem = $dados['mensagem'];
				
				$this->load->model('Imail', 'mail');
				/*$lista = array(
					"letiel@gmail.com",
					"lauro@hotmail.com.br",
					"gmail@letiel.com.br",
					"letielzaza@hotmail.com"
					);*/
				//$query = $this->db->query("SELECT usuarios.email, usuarios.nome, usuarios.sobrenome, cidade.nome as cidade FROM usuarios JOIN cidade ON (cidade.id = usuarios.cidade) WHERE confirmado = 1 AND bloqueado = 0 AND desativado = 0 AND disp_viajar = 1 AND cidade.nome LIKE 'Londrina'");
				$query = $this->db->query("SELECT usuarios.email, usuarios.nome, usuarios.sobrenome, usuarios.codigo, cidade.nome as cidade FROM usuarios JOIN cidade ON (cidade.id = usuarios.cidade) WHERE usuarios.recebe_email_massa_cidade = 1 AND usuarios.cidade = $cidade AND usuarios.confirmado = 1");
				$lista = $query->result();
				// $arquivo = fopen("email.txt", "w");
				foreach($lista as $email){
					$message = str_replace("@nome", $email->nome, $mensagem);
					$message = str_replace("@cidade", $email->cidade, $message);
					// $message = "Olá, $lista->nome, uma nova empresa se cadastrou em sua cidade! Clique <a href='".base_url()."'>aqui</a> pra conferir.<br />Se não desejar receber mais este tipo de e-mail, <a href='".base_url()."curriculo/cancela_email/$email->codigo'>clique aqui</a>";
					$this->mail->enviar("Tem mensagem nova para você!",  $email->email, $message);
					//echo "Email enviado para '$email'"." as ".date("H:i:s")." de ".date("d/m/Y").".<br />";
					//fwrite($arquivo, $email." enviado as ".date("H:i:s")." de ".date("d/m/Y").".\n");
					sleep(10);
					echo "$email->email - $email->nome $email->sobrenome em <b>$email->cidade</b><br />";
				}
				// fclose($arquivo);
			}

			$dados = array(
				'cidades'=>$this->cidade->get_cidades()->result(),
			);

			$this->load->view("adm_enviar_email_massa", $dados);
		}

		function sitemap(){
			$total_empresas = $this->adm->sitemap_empresas()->num_rows();
			$total_usuarios = $this->adm->sitemap_curriculos()->num_rows();
			$total_vagas = $this->adm->sitemap_vagas()->num_rows();

			$total_lista_empresas = $this->adm->sitemap_lista_empresas()->num_rows();
			$total_lista_usuarios = $this->adm->sitemap_lista_usuarios()->num_rows();
			$total_lista_vagas = $this->adm->sitemap_lista_vagas()->num_rows();

			$cabecalho = '<?xml version="1.0" encoding="utf-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
			$rodape = '</urlset>';

			$sitemap = fopen("sitemap/sitemap_x.xml", "w");

			$indice = fopen("sitemap/indice.xml", "w");
			fwrite($indice, "<?xml version='1.0' encoding='UTF-8'?><sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>");
			fwrite($indice, "<sitemap><loc>".base_url()."sitemap/sitemap_x.xml</loc></sitemap>");

			fwrite($sitemap, $cabecalho);

			fwrite($sitemap, "<url><loc>".base_url()."CadastroPessoa</loc><changefreq>always</changefreq><priority>0.8</priority></url>");

			fwrite($sitemap, "<url><loc>".base_url()."CadastroEmpresa</loc><changefreq>always</changefreq><priority>0.8</priority></url>");

			fwrite($sitemap, "<url><loc>".base_url()."</loc><changefreq>always</changefreq><priority>1.0</priority></url>");
			fwrite($sitemap, $rodape);
			fclose($sitemap);

			$pPagina = 9000;

			$maximo = 9000; //precisa dar número inteiro, se não dá erro na leitura no banco!;

			$total = $total_empresas + $total_usuarios + $total_vagas/* + $total_lista_empresas + $total_lista_usuarios + $total_lista_vagas*/;
			$paginas = $total/$maximo;

			$inicio = 0;

			//echo "Total de registros: $total<br />Maximo de cada tipo: $maximo<br />Páginas: $paginas<br /><br />";
			$empresas_add = 0;
			$usuarios_add = 0;
			$vagas_add = 0;
			$total_arquivos = 1; //valor 1 pois já criou um para a home e telas de cadastro acima!

			for($i = 1; $i <= $paginas+1; $i++){

				$empresas = $this->adm->sitemap_empresas_pag($inicio, $maximo)->result();
				$usuarios = $this->adm->sitemap_curriculos_pag($inicio, $maximo)->result();
				$vagas = $this->adm->sitemap_vagas_pag($inicio, $maximo)->result();

				$sitemap = fopen("sitemap/sitemap_".$i.".xml", "w");
				fwrite($indice, "<sitemap><loc>".base_url()."sitemap/sitemap_$i.xml</loc></sitemap>");

				fwrite($sitemap, $cabecalho);

				foreach($empresas as $empresa){
					$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º' );

					$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
							    
					$uri = str_replace($what, $by, $empresa->nome);

					fwrite($sitemap, "
					<url>
					    <loc>".base_url()."Empresa/ver/$empresa->id/$uri</loc>
					    <changefreq>always</changefreq>
					    <priority>0.6</priority>
					  </url>
					");
					$empresas_add++;
				}

				foreach($usuarios as $usuario){
					$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º' );

					$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
					
					$nome = $usuario->nome." ".$usuario->sobrenome;
					
					$uri = str_replace($what, $by, $nome);

					fwrite($sitemap, "
					<url>
					    <loc>".base_url()."Curriculo/ver/$usuario->id/$uri</loc>
					    <changefreq>always</changefreq>
					    <priority>0.6</priority>
					  </url>
					");
					$usuarios_add++;
				}

				foreach($vagas as $vaga){
					$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º' );

					$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
					
					$uri = str_replace($what, $by, $vaga->cargo);

					fwrite($sitemap, "
					<url>
					    <loc>".base_url()."Vaga/ver/$vaga->id/$uri</loc>
					    <changefreq>always</changefreq>
					    <priority>0.6</priority>
					  </url>
					");
					$vagas_add++;
				}

				fwrite($sitemap, $rodape);
				fclose($sitemap);
				$inicio += $maximo;
				$total_arquivos++;
			}

			/*$inicio = 0;
			$maximo = 10;
			for($i = 0; $i<ceil($total_lista_usuarios/10); $i++){

				$lista_usuarios = $this->adm->sitemap_lista_usuarios($inicio, $maximo)->result();
				$lista_empresas = $this->adm->sitemap_lista_empresas($inicio, $maximo)->result();
				$lista_vagas = $this->adm->sitemap_lista_vagas($inicio, $maximo)->result();

				$sitemap = fopen("sitemap/sitemap_lista_usuarios_".$i, "w");
				foreach($lista_usuarios as $lista){
					fwrite($cabecalho);
					fwrite($sitemap, "
					<url>
					    <loc><?php echo base_url(); ?>pessoas/lista/$i</loc>
					    <changefreq>always</changefreq>
					    <priority>0.6</priority>
					  </url>
					");
					fwrite($rodape);
				}
				fclose($sitemap);
				$inicio += $maximo;
			}
			*/
			fwrite($indice, "</sitemapindex>");
			fclose($indice);
			$total_urls = $vagas_add + $usuarios_add + $empresas_add;
			echo "<h3>Total de url's adicionadas: $total_urls e $total_arquivos arquivos .xml criados</h3>";
			//$this->load->view("adm_sitemap", $dados);
		}

		function cadastrar_vaga(){
			$this->logado();
			$this->form_validation->set_rules('titulo', 'Título', 'ucfirst|addslashes|required');
			$this->form_validation->set_rules('nome_empresa', 'Nome da Empresa', 'ucfirst|addslashes|required');
			$this->form_validation->set_rules('cidade', 'Cidade', 'required|is_numeric');
			$this->form_validation->set_rules('descricao', 'Descrição', 'required|addslashes');

			if($this->form_validation->run()){


				$config['upload_path'] = './img/thumbs/vagas';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '10000';
				$config['max_width'] = '1920';
				$config['max_height'] = '1080';
				$nome = uniqid('foto_');
				$config['file_name'] = $nome;

				$this->load->library('upload', $config);


				$dados = elements(array('titulo', 'nome_empresa', 'email', 'cidade', 'descricao'), $this->input->post());

				$nome_foto = $_FILES['foto']['name'];
				if($nome_foto != NULL && $nome_foto != ''){
					$ext = explode('.', $nome_foto);
					$dados['foto'] = $nome.".".$ext[sizeof($ext)-1];
					$foto = $this->upload->data();
					$this->upload->do_upload('foto');
					// $this->adm->atualiza_foto('/img/thumbs/vagas/'.$foto['file_name'].'.'.$ext[sizeof($ext - 1)]);
				} else
					$dados['foto'] = NULL;


				$this->adm->cadastra_vaga_especial($dados);
			}

			$dados = array(
				'cidades'=>$this->cidade->get_cidades()->result(),
			);

			$this->load->view("adm_cadastro_vaga", $dados);
		}
	}