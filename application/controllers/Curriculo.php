<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Curriculo extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('cata_escolaridade_model', 'escolaridade');
			$this->load->model('Login_model', 'login');
			$this->load->model('Cata_estado_civil_model', 'estado_civil');
			$this->load->model('Cadastro_pessoa_model', 'pessoa');
			$this->load->model('Cidades', 'cidades');

			$this->load->library('funcoes');

			$this->load->model('Login_model', 'login_user');
			$this->login_user->verificaMensagens();
		}

		public function index(){
			$this->login->logged();

			function valid_date($date, $format = 'dd/mm/yyyy'){
		 
		       $dateArray = explode("/", $date); // slice the date to get the day, month and year separately
		 
		       $d = 0;
		       $m = 0;
		       $y = 0;
		       if (sizeof($dateArray) == 3) {
		           if (is_numeric($dateArray[0]))
		               $d = $dateArray[0];
		           if (is_numeric($dateArray[1]))
		               $m = $dateArray[1];
		           if (is_numeric($dateArray[2]))
		               $y = $dateArray[2];
		       }
		 
		       return checkdate($m, $d, $y) == 1;
		   }

			$this->form_validation->set_rules('nome', 'Nome', 'trim|required|addslashes');
			$this->form_validation->set_rules('sobrenome', 'Sobrenome', 'required|addslashes');
			$this->form_validation->set_rules('dt_nascimento', 'Data de nascimento', 'required|valid_date');
			$this->form_validation->set_message('valid_date', 'Data de Nascimento inválida!');
			$this->form_validation->set_rules('endereco', 'Endere&ccedil;o', 'addslashes');
			$this->form_validation->set_rules('complemento', 'Complemento', 'addslashes');
			$this->form_validation->set_rules('bairro', 'Bairro', 'addslashes');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|required|addslashes|valid_email');
			$this->form_validation->set_rules('link_facebook', 'Facebook', 'trim|addslashes|prep_url');
			$this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|addslashes');
			$this->form_validation->set_rules('filhos', 'Quantidade de Filhos', 'trim|addslashes');
			$this->form_validation->set_rules('telefone', 'Telefone', 'trim|addslashes');
			$this->form_validation->set_rules('celular', 'Celular', 'trim|addslashes');
			$this->form_validation->set_rules('cep', 'CEP', 'trim|addslashes');
			$this->form_validation->set_rules('escolaridade', 'Escolaridade', 'trim|addslashes');
			$this->form_validation->set_rules('curso_nome', 'Nome do Curso', 'trim|addslashes');
			$this->form_validation->set_rules('curso_concluido', 'Curso Concluído', 'trim|addslashes');
			$this->form_validation->set_rules('experiencias', 'Experiências', 'trim|addslashes');
			$this->form_validation->set_rules('idiomas', 'Idiomas', 'trim|addslashes');
			$this->form_validation->set_rules('disp_viajar', 'Disponibilidade para Viajar', 'trim|addslashes');
			$this->form_validation->set_rules('sexo', 'Sexo', 'trim|addslashes');
			$this->form_validation->set_rules('deficiente', 'Deficiente', 'trim|addslashes');
			$this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');

			$config['upload_path'] = './img/thumbs';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '500';
			$config['max_width'] = "1000";
			$config['max_height'] = "1000";
			$nome = uniqid('foto_');
			$config['file_name'] = $nome;

			$this->load->library('upload', $config);

			//$this->form_validation->set_rules('senha', 'Senha', 'trim|xss_clean|addslashes|matches[conf_senha]');
			//$this->form_validation->set_rules('conf_senha', 'Confirma&ccedil;&atilde;o de senha', 'trim|xss_clean|addslashes');

			$this->form_validation->set_message('required', 'O campo "%s" é obrigat&oacute;rio!');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('nome', 'sobrenome', 'dt_nascimento', 'endereco', 'complemento', 'bairro', 'email', 'link_facebook', 'deficiente', 'estado_civil', 'filhos', 'telefone', 'celular', 'cep', 'escolaridade', 'curso_nome', 'curso_concluido', 'experiencias', 'idiomas', 'disp_viajar', 'hab_a', 'hab_b', 'hab_c', 'hab_d', 'hab_e', 'carro', 'moto', 'caminhao', 'outro_veiculo', 'sexo', 'cidade'), $this->input->post());
				$data = explode("/", $dados['dt_nascimento']);
				$ano = $data[2];
				$mes = $data[1];
				$dia = $data[0];
				$dados['dt_nascimento'] = date('Y-m-d', strtotime("$ano-$mes-$dia"));
				$this->pessoa->update_curriculo($dados);
				$nome_foto = $this->input->post('foto');
				$nome_foto = $_FILES['foto']['name'];
				$exp = explode('.', $nome_foto);
				$n_ext = (sizeof($exp) - 1);
				$ext = $exp[$n_ext];
				if($nome_foto != NULL && $nome_foto != ''){
					$foto = $this->upload->data();
					if(!$this->upload->do_upload('foto')){
						$this->session->set_flashdata('erro_upload', '<div class="alert alert-danger"><b>Ocorreu um erro ao enviar a imagem!</b><br /><small>A imagem pode ter no máximo 500Kb de tamanho e altura e largura máxima de 1000px!</small></div>');
					}else{
						$this->pessoa->atualiza_foto(base_url().'img/thumbs/'.$foto['file_name'].".".$ext);
						$this->session->set_flashdata('erro_upload', '<div class="alert alert-success"><b>Imagem atualizada com sucesso!</b></div>');
					}
				}
			}

			$escolaridades = $this->escolaridade->get_escolaridade()->result();
			$estados_civis = $this->estado_civil->get_estado_civil()->result();
			$cursos = $this->pessoa->cata_cursos()->result();
			$pessoa = $this->pessoa->get_by_id()->result();
			$dados = array(
				'escolaridades'=> $escolaridades,
				'estados_civis'=> $estados_civis,
				'pessoa'=>$pessoa,
				'cursos'=>$cursos,
				'cidades'=>$this->cidades->get_cidades()->result(),
				);
			$this->load->view('editar_curriculo_oldd', $dados);
		}

		public function ver(){
			$perfil = $this->uri->segment(3);
			$nome = $this->uri->segment(4);
			$pessoa = $this->pessoa->ver_curriculo($perfil)->result();
			if($this->pessoa->ver_curriculo($perfil)->num_rows() > 0){
				$exp = $this->pessoa->ver_exp($perfil)->result();
				$cursos = $this->pessoa->ver_cursos($perfil)->result();
				$idiomas = $this->pessoa->ver_idiomas($perfil)->result();
				//enviando dados para edição
				if($this->session->userdata("id") == $perfil){
					$lista_escolaridades = $this->escolaridade->get_escolaridade()->result();
					$lista_estados_civis = $this->estado_civil->get_estado_civil()->result();
					$lista_cursos = $this->pessoa->cata_cursos()->result();
					$areas_usuario = $this->pessoa->get_areas_usuario()->result();
					$all_areas = $this->pessoa->get_all_areas()->result();
					$pagina = "perfil";
				}else{
					$lista_escolaridades = null;
					$lista_estados_civis = null;
					$lista_cursos = null;
					$areas_usuario = $this->pessoa->get_areas_usuario($perfil)->result();
					$all_areas = null;
					$pagina = null;
				}

				$dados = array(
					'pagina'=>$pagina,
					'perfil' => $perfil,
					'pessoa'=> $pessoa,
					'exp' => $exp,
					'cursos' => $cursos,
					'idiomas' => $idiomas,
					'lista_cursos' => $lista_cursos,
					'lista_escolaridades' => $lista_escolaridades,
					'lista_estados_civis' => $lista_estados_civis,
					'cidades'=>$this->cidades->get_cidades()->result(),
					'servicos'=>$this->pessoa->autonomo($perfil)->result(),
					'total_servicos'=>$this->pessoa->autonomo($perfil)->num_rows(),
					'areas_usuario'=>$areas_usuario,
					'all_areas'=>$all_areas
				);
				if($this->session->userdata("id") == $perfil)
					$this->load->view('editar_curriculo', $dados);
				else
					$this->load->view('perfilUsuario', $dados);
			}else{
				$this->session->set_flashdata('confirmado', "<div class='alert alert-danger' style='overflow: hidden;margin-top: 5px;text-align: center;'><h3>Curriculo não encontrado!</h3></div>");
				redirect(base_url());
			}
		}

		public function configuracoes() {
			$this->login->logged();

			$dados = array('pagina'=>'configuracoes');
			$this->load->view('configuracoes_usuario', $dados);
		}

		public function config_altera_login(){
			$this->form_validation->set_rules('login', 'Login', 'required|trim|addslashes|is_unique[usuarios.login]');

			$this->form_validation->set_message('required', 'O campo <b>%s</b> é obrigat&oacute;rio!');
			$this->form_validation->set_message('is_unique', 'Login já existente no sistema. Por favor informe outro.');

			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			if($this->form_validation->run() == TRUE) {
				$dados = elements(array('login'), $this->input->post());
				$this->pessoa->alterar_login($dados);
				$this->session->unset_userdata('logado');
				echo "<div class='alert alert-success'>Login alterado com sucesso! Faça login novamente.</div><meta http-equiv='refresh' content='4; ".base_url()."'>";
			} else {
				echo validation_errors();
			}

		}

		public function config_altera_senha(){
			$this->form_validation->set_rules('senhaAntiga', 'Senha Antiga', 'required|trim|addslashes');
			$this->form_validation->set_rules('senha', 'Nova Senha', 'required|trim|addslashes|matches[conf_senha]');
			$this->form_validation->set_rules('conf_senha', 'Confirma&ccedil;&atilde;o de senha', 'required|trim|addslashes');

			$this->form_validation->set_message('required', 'O campo "%s" é obrigat&oacute;rio!');
			$this->form_validation->set_message('matches', 'As senhas não conferem');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('senha'), $this->input->post());
				$senha = elements(array('senhaAntiga'), $this->input->post());
				$senha['senhaAntiga'] = md5($senha['senhaAntiga']);
				$dados['senha'] = md5($dados['senha']);
				$confirma = $this->pessoa->confirma_senha()->result();
				if($confirma[0]->senha == $senha['senhaAntiga']){
					$this->pessoa->troca_senha($dados);
					$this->session->unset_userdata('logado');
					echo '<div class="alert alert-success">Sua senha foi alterada com sucesso! Faça login novamente.</div><meta http-equiv="refresh" content="4; '.base_url().'">';
					//echo $this->session->flashdata('sucesso');
				}else{
					echo '<div class="alert alert-danger">Senha antiga inválida!</div>';
				}
			} else {
				echo validation_errors();
			}
		}

		public function senha(){
			$this->login->logged();

			$this->form_validation->set_rules('senhaAntiga', 'Senha Antiga', 'required|trim|addslashes');
			$this->form_validation->set_rules('senha', 'Nova Senha', 'required|trim|addslashes|matches[conf_senha]');
			$this->form_validation->set_rules('conf_senha', 'Confirma&ccedil;&atilde;o de senha', 'required|trim|addslashes');

			$this->form_validation->set_message('required', 'O campo "%s" é obrigat&oacute;rio!');
			$this->form_validation->set_message('matches', 'As senhas não conferem');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('senha'), $this->input->post());
				$senha = elements(array('senhaAntiga'), $this->input->post());
				$senha['senhaAntiga'] = md5($senha['senhaAntiga']);
				$dados['senha'] = md5($dados['senha']);
				$confirma = $this->pessoa->confirma_senha()->result();
				if($confirma[0]->senha == $senha['senhaAntiga']){
					$this->pessoa->troca_senha($dados);
					$this->session->set_flashdata('sucesso', '<div class="alert alert-success">Sua senha foi alterada com sucesso!</div>');
					//echo $this->session->flashdata('sucesso');
					redirect('curriculo/senha');
				}else{
					$this->session->set_flashdata('sucesso', '<div class="alert alert-danger">Erro ao alterar senha!</div>');
					redirect('curriculo/senha');
				}
			}
			$dados = array('pagina'=>'configuracoes');
			$this->load->view('trocar_senha', $dados);
		}

		public function cadastraCurso(){
			$this->login->logged();
			$dados = elements(array('curso', 'concluido', 'instituicao'), $this->input->post());
			$this->pessoa->cadastraCurso($dados);
		}

		public function pega_cursos_by_user(){ //retorna os cursos da pessoa logada
			$this->login->logged();
			$cursos = $this->pessoa->cata_cursos_by_user()->result();
			foreach($cursos as $curso){
				if($curso->concluido == 1){
					$concluido = "Completo";
				}else{
					$concluido = "Incompleto";
				}
				//echo "<a onclick='deletaCurso($curso->id)' style='cursor: pointer; margin: 0.2%; margin-bottom: 10px; display: block;' class='pull-left' title='Clique para excluir'><span class='label label-success'>$curso->nome - $concluido</span></a>";
				?>
				<div role='alert'>
				  <button type='button' onclick='deletaCurso(<?php echo $curso->id ?>)' class='close' data-dismiss='alert' aria-label='Close'><span style='color: #F00;' aria-hidden='true'>&times;</span></button>
				  <h4 class='azul item'><?php echo "$curso->nome - $curso->instituicao"?> </h4>
				</div>
				<?php
			}
		}

		public function pega_cursos_for_user(){ //alimenta o select, para que a pessoa possa selecionar um curso novo
			$this->login->logged();
			$cursos = $this->pessoa->cata_cursos()->result();
			echo "<option value=''>Cadastrar novo</option>";
			foreach($cursos as $curso){
				echo "<option value='$curso->id'>$curso->nome</option>";
			}
		}

		public function deletaCurso(){
			$this->login->logged();
			$dados = array(
				'curso'=>$this->uri->segment(3),
				);

			$this->pessoa->deletaCurso($dados);
		}

		public function cadastra_novo_curso(){
			$this->login->logged();
			$dados = elements(array('nome_curso', 'concluido', 'instituicao'), $this->input->post());
			//$dados = array('nome'=>$curso, 'concluido'=>$concluido, 'instituicao'=>$instituicao);
			$this->pessoa->cadastra_novo_curso($dados);
		}

		public function cadastra_nova_area_usuario(){
			$this->login->logged();
			$dados = elements(array('area'), $this->input->post());
			$this->pessoa->cadastra_nova_area($dados);
		}

		public function cadastra_experiencia(){
			$this->login->logged();
			$dados = elements(array('descricao', 'funcao', 'inicio', 'final_', 'empresa', 'telefone', 'cidade', 'endereco'), $this->input->post());
			$this->pessoa->cadastra_experiencia($dados);
		}

		public function cadastra_servico(){
			$this->login->logged();
			$dados = elements(array('titulo_servico', 'descricao_servico', 'lugares', 'contato', 'inicio'), $this->input->post());
			$this->pessoa->cadastra_servico($dados);
		}

		public function retorna_exp_by_user(){
			$this->login->logged();
			$exps = $this->pessoa->cata_experiencias_by_user()->result();
			$i = 0;
			foreach($exps as $exp){
				$exp->inicio = date("d/m/Y", strtotime($exp->inicio));
				if($exp->final == "0000-00-00"){
					$exp->final = "hoje";
				}else{
					$exp->final = date("d/m/Y", strtotime($exp->final));
				}


				/*echo "<a onclick='deletaExp($exp->id)' style='cursor: pointer; margin: 0.2%; margin-bottom: 10px; display: block;' class='pull-left'>
						<span class='label label-default popover_exp' data-toggle='popover' data-trigger='hover' title='Clique para deletar experiência' data-content='$exp->descricao'>$exp->funcao</span>
					</a>";*/
				if($i > 0)
					echo "<div class='separador-menor'></div>";
				?>
				<div role='alert'>
				  <button type='button' onclick='deletaExp(<?php echo $exp->id ?>)' style='color: #F00;' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				  <h4 class='azul'><strong>Empresa: </strong><?php echo $exp->empresa ?></h4>
				  <h4 class='azul'><strong>Função: </strong><?php echo $exp->funcao ?></h4>
				  <h4 class='azul'><strong>Descrição: </strong><?php echo $exp->descricao ?></h4>
				  <h4 class='azul'><strong>Período: </strong>De <?php echo $exp->inicio ?> até <?php echo $exp->final ?></h4>
				</div>
				<?php
				$i++;
			}
			echo "<div style='margin-bottom: 10px; overflow: hidden;'></div>";
		}

		public function retorna_servicos_by_user(){
			$this->login->logged();
			$servicos = $this->pessoa->autonomo()->result();
			foreach($servicos as $servico){
				?>
				<div role='alert'>
					<button type='button' onclick='deletaServico(<?php echo $servico->id ?>)' style='color: #F00;' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
					<h4 class='azul'><b><a href='#' data-name='titulo_servico' data-pk='<?php echo $servico->id ?>' data-title='O que você faz?' class='editable-autonomo'><?php echo $servico->titulo_servico ?></a></b></h4>
					<h4 class='azul'><a href='#' data-name='descricao_servico' data-type='textarea' data-pk='<?php echo $servico->id ?>' data-title='Descreva sua profissão' class='editable-autonomo'><?php echo $servico->descricao_servico ?></a></h4>
					<h4 class='azul'><b>Atende em:</b> <a href='#' data-name='lugares' data-pk='<?php echo $servico->id ?>' data-title='Informe os lugares onde você atende' class='editable-autonomo'><?php echo $servico->lugares ?></a></h4>
					<h4 class='azul'><b>Contato:</b> <a href='#' data-name='contato' data-pk='<?php echo $servico->id ?>' data-title='Informe seu contato' class='editable-autonomo'><?php echo $servico->contato ?></a></h4>
					<h4 class='azul'><b>Realiza desde:</b> <a href='#' data-name='inicio' data-pk='<?php echo $servico->id ?>' data-title='Informe desde quando você faz isso' class='editable-autonomo-data'><?php echo date("d/m/Y", strtotime($servico->inicio)) ?></a></h4>
				</div>
				<div style='margin-bottom: 10px; overflow: hidden;'></div>
				<?php
			}
		}

		public function deletaExp(){
			$this->login->logged();
			$dados = elements(array('id'), $this->input->post());
			$this->pessoa->deletaExp($dados);
		}

		public function deletaServico(){
			$this->login->logged();
			$dados = elements(array('id'), $this->input->post());
			$this->pessoa->deletaServico($dados);
		}

		public function cata_idiomas_select(){
			$idiomas = $this->pessoa->lista_select_idiomas()->result();
			echo "<option value=''>Selecione</option>";
			foreach($idiomas as $idioma){
				echo "<option value='$idioma->id'>$idioma->nome</option>";
			}
		}

		public function cadastra_idioma_usuario(){
			$dados = elements(array('idioma'), $this->input->post());
			$this->pessoa->cadastra_idioma_usuario($dados);
		}

		public function cadastra_area_usuario(){
			$dados = elements(array('area'), $this->input->post());
			$this->pessoa->cadastra_area_usuario($dados);
		}

		public function cata_idiomas_by_user(){
			$idiomas = $this->pessoa->cata_idiomas_by_user()->result();
			foreach($idiomas as $idioma){
				/*echo "<a onclick='deletaIdioma($idioma->id)' style='cursor: pointer; margin: 0.2%; margin-bottom: 10px; display: block;' class='pull-left'>
						<span class='label label-default'  title='Clique para deletar experiência'>$idioma->nome</span>
					</a>";*/
				?>
				<div role='alert' style='margin-bottom: 15px;'>
				  <button type='button' onclick='deletaIdioma(<?php echo $idioma->id ?>)' class='close' data-dismiss='alert' aria-label='Close'><span style='color: #F00;' aria-hidden='true'>&times;</span></button>
				  <h4 class='azul'><?php echo $idioma->nome ?></h4>
				</div>
				<?php
			}
		}

		public function cata_areas_by_user(){
			$areas = $this->pessoa->cata_areas_by_user()->result();
			foreach($areas as $area){
				?>
				<div role='alert' style='margin-bottom: 15px;'>
				  	<button type='button' onclick='deletaArea(<?php echo $area->area ?>)' class='close' data-dismiss='alert' aria-label='Close'><span style='color: #F00;' aria-hidden='true'>&times;</span></button>
					<h4 class="azul item"><?php echo $area->descricao ?></h4>
				</div>
				<?php
			}
		}

		public function deletaArea(){
			$this->login->logged();
			$dados = elements(array('area'), $this->input->post());
			$this->pessoa->deletaArea($dados);
		}

		public function deletaIdioma(){
			$this->login->logged();
			$dados = elements(array('idioma'), $this->input->post());
			$this->pessoa->deletaIdioma($dados);
		}

		public function excluir_conta(){
			$this->login->logged();
			$pessoa = $this->pessoa->dados_para_exclusao()->result();
			$this->pessoa->excluir_conta($pessoa);
			redirect(base_url()."logout");
		}

		public function update_especifico(){


			function valid_date($date, $format = 'dd/mm/yyyy'){
		 
		       $dateArray = explode("/", $date); // slice the date to get the day, month and year separately
		 
		       $d = 0;
		       $m = 0;
		       $y = 0;
		       if (sizeof($dateArray) == 3) {
		           if (is_numeric($dateArray[0]))
		               $d = $dateArray[0];
		           if (is_numeric($dateArray[1]))
		               $m = $dateArray[1];
		           if (is_numeric($dateArray[2]))
		               $y = $dateArray[2];
		       }
		 
		       return checkdate($m, $d, $y) == 1;
		   }


			$dados = elements(array('campo', 'value'), $this->input->post());
			$campo = $dados['campo'];
			$value = $dados['value'];
			$_POST[$campo] = $value;

			if($campo=='nome'){
				$this->form_validation->set_rules('nome', 'Nome', 'trim|required|addslashes');
			}else if($campo=='sobrenome'){
				$this->form_validation->set_rules('sobrenome', 'Sobrenome', 'required|addslashes');
			}else if($campo=='dt_nascimento'){
				$this->form_validation->set_rules('dt_nascimento', 'Data de nascimento', 'required|valid_date');
			}else if($campo=='endereco'){
				$this->form_validation->set_rules('endereco', 'Endere&ccedil;o', 'addslashes');
			}else if($campo=='complemento'){
				$this->form_validation->set_rules('complemento', 'Complemento', 'addslashes');
			}else if($campo=='bairro'){
				$this->form_validation->set_rules('bairro', 'Bairro', 'addslashes');
			}else if($campo=='email'){
				$this->form_validation->set_rules('email', 'E-mail', 'trim|required|addslashes|valid_email');
			}else if($campo=='link_facebook'){
				$this->form_validation->set_rules('link_facebook', 'Facebook', 'trim|addslashes|prep_url');
			}else if($campo=='estado_civil'){
				$this->form_validation->set_rules('estado_civil', 'Estado Civil', 'trim|addslashes');
			}else if($campo=='filhos'){
				$this->form_validation->set_rules('filhos', 'Quantidade de Filhos', 'trim|addslashes');
			}else if($campo=='telefone'){
				$this->form_validation->set_rules('telefone', 'Telefone', 'trim|addslashes');
			}else if($campo=='celular'){
				$this->form_validation->set_rules('celular', 'Celular', 'trim|addslashes');
			}else if($campo=='cep'){
				$this->form_validation->set_rules('cep', 'CEP', 'trim|addslashes');
			}else if($campo=='escolaridade'){
				$this->form_validation->set_rules('escolaridade', 'Escolaridade', 'trim|addslashes');
			}else if($campo=='curso_nome'){
				$this->form_validation->set_rules('curso_nome', 'Nome do Curso', 'trim|addslashes');
			}else if($campo=='curso_concluido'){
				$this->form_validation->set_rules('curso_concluido', 'Curso Concluído', 'trim|addslashes');
			}else if($campo=='experiencias'){
				$this->form_validation->set_rules('experiencias', 'Experiências', 'trim|addslashes');
			}else if($campo=='idiomas'){
				$this->form_validation->set_rules('idiomas', 'Idiomas', 'trim|addslashes');
			}else if($campo=='disp_viajar'){
				$this->form_validation->set_rules('disp_viajar', 'Disponibilidade para Viajar', 'trim|addslashes');
			}else if($campo=='sexo'){
				$this->form_validation->set_rules('sexo', 'Sexo', 'trim|addslashes');
			}else if($campo=='deficiente'){
				$this->form_validation->set_rules('deficiente', 'Deficiente', 'trim|addslashes');
			}else if($campo=='cidade'){
				$this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');
			}			

			$this->form_validation->set_message('required', 'O campo "%s" é obrigat&oacute;rio!');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			echo validation_errors();

			if($this->form_validation->run() == TRUE){
				$this->pessoa->update_dado_especifico($campo, $value);
			}else{
				echo "Dados inválidos";
			}
		}

		public function cancela_email(){
			$pessoa = $this->uri->segment(3);
			$dados = array(
				'codigo'=>$pessoa,
				);
			$this->pessoa->cancela_email($dados);
			$this->session->set_flashdata('confirmado', "<div class='alert alert-success' style='text-align: center;'><h3>Você não receberá mais emails de <b>Currículo Online</b>!</h3></div>");
			redirect(base_url());
		}

		function editable(){
			$name = $_POST['name'];
			$value = $_POST['value'];
			$pk = $_POST['pk'];

			if($name == "nome"){
				$this->form_validation->set_rules('value', 'Nome', 'required|addslashes|max_length[50]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 50 caracteres no nome.'));
			}else if($name == "sobrenome"){
				$this->form_validation->set_rules('value', 'Sobrenome', 'required|addslashes|max_length[50]', array('required'=>'Informe seu sobrenome!', 'max_length'=>'Máximo de 50 caracteres no sobrenome.'));
			}else if($name == "sexo"){
				$this->form_validation->set_rules('value', 'Sexo', 'required|addslashes', array('required'=>'Selecione um item!'));
			}else if($name == "dt_nascimento"){
				$this->form_validation->set_rules('value', 'Data de nascimento', 'required|addslashes|max_length[50]', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "escolaridade"){
				$this->form_validation->set_rules('value', 'Escolaridade', 'addslashes|is_numeric', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "estado_civil"){
				$this->form_validation->set_rules('value', 'Estado Civil', 'addslashes|is_numeric', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "filhos"){
				$this->form_validation->set_rules('value', 'Filhos', 'addslashes|is_numeric', array('required'=>'Não pode ficar vazio!', 'is_numeric'=>'Somente números.'));
			}else if($name == "endereco"){
				$this->form_validation->set_rules('value', 'Endereco', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "bairro"){
				$this->form_validation->set_rules('value', 'Bairro', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "complemento"){
				$this->form_validation->set_rules('value', 'Complemento', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "cidade"){
				$this->form_validation->set_rules('value', 'Cidade', 'required|trim|addslashes|is_numeric', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "cep"){
				$this->form_validation->set_rules('value', 'CEP', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "telefone"){
				$this->form_validation->set_rules('value', 'Telefone', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "celular"){
				$this->form_validation->set_rules('value', 'Celular', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "disp_viajar"){
				$this->form_validation->set_rules('value', 'Viajar', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "deficiente"){
				$this->form_validation->set_rules('value', 'Deficiente', 'trim|addslashes', array('required'=>'Não pode ficar vazio!'));
			}else if($name == "recebe_email_massa_cidade"){
				$this->form_validation->set_rules('value', 'Receber e-mails em massa', 'trim|addslashes|required', array('required'=>'Não pode ficar vazio!'));
			}

			if($this->form_validation->run() == TRUE){
				$value = $_POST['value'];
				$this->db->query("UPDATE usuarios SET $name = '$value' WHERE id = $pk");
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
				// $result = array('success'=>false, 'msg'=>$name." - ". $value);
			}

			//$result = array('success'=>false, 'msg'=>"$name - $value - $pk");
			echo json_encode($result);

		}

		function editable_autonomo(){
			$name = $_POST['name'];
			$value = (isset($_POST['value']) ? $_POST['value'] : "");
			$pk = $_POST['pk'];

			function valid_date($date, $format = 'dd/mm/yyyy'){
		 
		       $dateArray = explode("/", $date); // slice the date to get the day, month and year separately
		 
		       $d = 0;
		       $m = 0;
		       $y = 0;
		       if (sizeof($dateArray) == 3) {
		           if (is_numeric($dateArray[0]))
		               $d = $dateArray[0];
		           if (is_numeric($dateArray[1]))
		               $m = $dateArray[1];
		           if (is_numeric($dateArray[2]))
		               $y = $dateArray[2];
		       }
		 
		       return checkdate($m, $d, $y) == 1;
		   }

			if($name == "titulo_servico"){
				$this->form_validation->set_rules('value', 'Nome', 'required|addslashes|max_length[50]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 50 caracteres.'));
			}else if($name == "descricao_servico"){
				$this->form_validation->set_rules('value', 'Descrição', 'required|addslashes', array('required'=>'Informe seu sobrenome!'));
			}else if($name == "lugares"){
				$this->form_validation->set_rules('value', 'Lugares onde atende', 'required|addslashes', array('required'=>'Informe seu sobrenome!'));
			}else if($name == "contato"){
				$this->form_validation->set_rules('value', 'Contato', 'required|addslashes', array('required'=>'Informe seu sobrenome!'));
			}else if($name == "inicio"){
				$this->form_validation->set_rules('value', 'Data de início', 'required|valid_date', array('required'=>'Informe a data!', 'valid_date'=>'Informe uma data válida!'));
				// $data = explode("/", $value);
				// $ano = $data[2];
				// $mes = $data[1];
				// $dia = $data[0];
				// $value = date('Y-m-d', strtotime("$ano-$mes-$dia"));
				$value = date('Y-m-d', strtotime($value));
			}

			if($this->form_validation->run() == TRUE){
				$this->db->query("UPDATE servicos_usuarios SET $name = '$value' WHERE id = $pk");
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
				// $result = array('success'=>false, 'msg'=>$name." - ". $value);
			}

			//$result = array('success'=>false, 'msg'=>"$name - $value - $pk");
			echo json_encode($result);

		}

		function editable_veiculos(){
			$name = $_POST['name'];
			$value = (isset($_POST['value']) ? $_POST['value'] : NULL);
			$pk = $_POST['pk'];
			if(isset($pk) && isset($name)){
				$this->db->query("UPDATE usuarios SET carro = 0, moto = 0, caminhao = 0, outro_veiculo = 0 WHERE id = $pk");
				if($value != null)
				foreach($value as $v){
					$this->db->query("UPDATE usuarios SET $v = 1 WHERE id = $pk");
				}
				$result = array('success'=>true, 'msg'=>"Atualizado com sucesso");
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>true, 'msg'=>validation_errors());
			}
			echo json_encode($result);
		}

		function editable_habilitacao(){
			$name = $_POST['name'];
			$value = (isset($_POST['value']) ? $_POST['value'] : NULL);
			$pk = $_POST['pk'];
			if(isset($pk) && isset($name)){
				$this->db->query("UPDATE usuarios SET hab_a = 0, hab_b = 0, hab_c = 0, hab_d = 0, hab_e = 0 WHERE id = $pk");
				if($value != null)
					foreach($value as $v){
						$this->db->query("UPDATE usuarios SET $v = 1 WHERE id = $pk");
					}
				$result = array('success'=>true, 'msg'=>"Atualizado com sucesso");
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>"Problema na atualização");
			}
			echo json_encode($result);
		}

		function troca_imagem(){
			$dados = elements(array('imagem'), $this->input->post());
			$this->form_validation->set_rules('imagem', 'Imagem', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
			if($this->form_validation->run() == TRUE){
				$usuario = $this->session->userdata("id");
				$pessoa = $this->pessoa->get_by_id()->first_row();
				$foto_antiga = $pessoa->foto;
				// $foto_antiga = str_replace("/img/thumbs", "", $foto_antiga);
				$foto_antiga = explode("/", $foto_antiga);
				$foto_antiga = $foto_antiga[(sizeof($foto_antiga) - 1)];

				unlink("./img/thumbs/".$foto_antiga);
				unlink(".".$dados['imagem']);

				$imagem = explode(".", $dados['imagem']);
				$dados['imagem'] = $imagem[0]."-avatar.".$imagem[1];
				$this->db->query("UPDATE usuarios SET foto = '$dados[imagem]' WHERE id = $usuario");
			}else{
				$this->form_validation->set_error_delimiters('', '');
				echo validation_errors();
			}
		}

		public function autonomo(){
			$perfil = $this->uri->segment(3);
			$nome = $this->uri->segment(4);
			$pessoa = $this->pessoa->ver_curriculo($perfil)->result();
			if($this->pessoa->ver_curriculo($perfil)->num_rows() > 0){
				$exp = $this->pessoa->ver_exp($perfil)->result();
				$cursos = $this->pessoa->ver_cursos($perfil)->result();
				$idiomas = $this->pessoa->ver_idiomas($perfil)->result();
				//enviando dados para edição
				if($this->session->userdata("id") == $perfil){
					$lista_escolaridades = $this->escolaridade->get_escolaridade()->result();
					$lista_estados_civis = $this->estado_civil->get_estado_civil()->result();
					$lista_cursos = $this->pessoa->cata_cursos()->result();
				}else{
					$lista_escolaridades = null;
					$lista_estados_civis = null;
					$lista_cursos = null;
				}

				$dados = array(
					'perfil' => $perfil,
					'pessoa'=> $pessoa,
					'exp' => $exp,
					'cursos' => $cursos,
					'idiomas' => $idiomas,
					'lista_cursos' => $lista_cursos,
					'lista_escolaridades' => $lista_escolaridades,
					'lista_estados_civis' => $lista_estados_civis,
					'cidades'=>$this->cidades->get_cidades()->result(),
					'servicos'=>$this->pessoa->autonomo($perfil)->result(),
					);
					$this->load->view('autonomo', $dados);
			}else{
				$this->session->set_flashdata('confirmado', "<div class='alert alert-danger' style='overflow: hidden;margin-top: 5px;text-align: center;'><h3>Curriculo não encontrado!</h3></div>");
				redirect(base_url());
			}
		}
	}