<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	class Pessoas extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Pessoas_model', 'pessoas');

			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		}

		function index(){
			redirect('http://www.icurriculumvitae.com.br');
		}

		function lista(){
			$id = $this->uri->segment(2);

			$maximo = 10;
			$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

			$config['base_url'] = '/pessoas/lista';
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['total_rows'] = $this->pessoas->cata_pessoas_all()->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'pagina'=>'curriculos',
				'pessoas'=>$this->pessoas->cata_pessoas($maximo, $inicio)->result(),
				'paginacao'=>$this->pagination->create_links(),
			);
			$this->load->view('lista_usuarios', $dados);
		}
	}