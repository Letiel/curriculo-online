<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	
	class Vaga extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Criar_vaga_model', 'vaga');
			$this->load->model('Cadastro_pessoa_model', 'pessoa');
		}

		function ver(){
			$id = $this->uri->segment(3);
			if($this->vaga->ver_vaga($id)->num_rows() == 1){
				if($this->session->userdata('logado')){
					if($this->vaga->teste_candidato($id)->num_rows() == 1)
						$candidato = true;
					else
						$candidato = false;
					$dados = array(
						'vaga' => $this->vaga->ver_vaga($id)->result(),
						'candidatura' => $candidato,
					);
				}else{
					$dados = array(
						'vaga' => $this->vaga->ver_vaga($id)->result(),
						'candidatura' => false,
					);
				}
			}else{
				redirect(base_url());
			}
			$this->load->view('ver_vaga', $dados);
		}

		function candidatar(){
			$vaga = elements(array('vaga'), $this->input->post());
			if(!$this->vaga->candidatar($vaga)){
				echo "Erro ao se candidatar a vaga!\nVocê pode já ter se candidatato anteriormente a esta vaga!\nSe isto não aconteceu, entre em contato com os administradores do sistema!";
			}
			$vag = $this->vaga->ver_vaga($vaga['vaga'])->result();
			$vag = $vag[0];
			$pessoa = $this->pessoa->cria_link_curriculo()->result();
			$pessoa = $pessoa[0];
			$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º' );
			$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
			$nome = $pessoa->nome."-".$pessoa->sobrenome;
			$uri = str_replace($what, $by, $nome);
			$link = base_url()."curriculo/ver/$pessoa->id/$uri";

			$link_cancelar = base_url()."vaga/cancelar/$vag->codigo";
			$link_vaga = base_url()."vaga/ver/$vag->id_vaga";

			$mensagem_vaga_especial = "Olá $vag->nome_empresa_especial, 
			você recebeu um currículo de $pessoa->nome $pessoa->sobrenome, 
			da cidade de $pessoa->cidade - $pessoa->uf. 
			Para ver o currículo completo de $pessoa->nome clique em 
			<a href='$link'>$link</a>.<br />Link da vaga: <a href='$link_vaga'>$link_vaga</a><br />Se você não deseja mais receber mensagens sobre esta vaga, ou você não é responsável por ela, por favor <a href='$link_cancelar'>clique aqui</a> ou cole o endereço abaixo no seu navegador.<br />$link_cancelar.";

			$mensagem_vaga_normal = "Olá $vag->nome_empresa, 
			você recebeu um currículo de $pessoa->nome $pessoa->sobrenome, 
			da cidade de $pessoa->cidade - $pessoa->uf. 
			Para ver o currículo completo de $pessoa->nome 
			<a href='$link'>$link</a>.<br />Link da vaga: <a href='$link_vaga'>$link_vaga</a><br />Se você não deseja mais receber mensagens sobre esta vaga, ou você não é responsável por ela, por favor <a href='$link_cancelar'>clique aqui</a> ou cole o endereço abaixo no seu navegador.<br />$link_cancelar";
			$message = ($vag->especial ? $mensagem_vaga_especial : $mensagem_vaga_normal);

            $subject = 'Alguém se candidatou à sua vaga!';
            $this->load->model('Imail', 'mail');
            if(($vag->email_empresa_especial != NULL && $vag->email_empresa_especial != "") || ($vag->email != NULL && $vag->email != "")){
	            if($vag->especial == 1)
	            	$this->mail->enviar($subject,  $vag->email_empresa_especial, $message);
	            else
	            	$this->mail->enviar($subject,  $vag->email, $message);
	        }
		}

		function cancelar(){
			$codigo_vaga = $this->uri->segment(3);
			$this->vaga->desativar_vaga($codigo_vaga);
		}
	}