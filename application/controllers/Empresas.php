<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Empresas extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Criar_vaga_model', 'vaga');
			$this->load->model('Empresa', 'empresa');

			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		
		}

		public function index(){
			$pagina = $this->uri->segment(2);
			$cidade = $this->uri->segment(3);

			$dados = array(
				'pagina'=>$pagina,
				'cidade'=>$cidade,	
				);

			$this->load->view('empresas', $dados);
		}

		public function listar(){
			$maximo = 10;
			$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

			$config['base_url'] = '/Empresas/listar';
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['total_rows'] = $this->empresa->listar_all()->num_rows();
			$this->pagination->initialize($config);


			$dados = array(
				'pagina'=>"empresas",
				'empresas'=>$this->empresa->listar($maximo, $inicio)->result(),
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view('lista_empresas', $dados);
		}

		public function ver(){
			$id = $this->uri->segment(3);
			$empresa = $this->empresa->ver($id);
		}

		public function vagas(){
			redirect("vagas");
			$maximo = 10;
			$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

			$config['base_url'] = '/Empresas/vagas/';
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['total_rows'] = $this->vaga->cata_vagas_all()->num_rows();
			$this->pagination->initialize($config);

			$vagas = $this->vaga->cata_vagas($maximo, $inicio)->result();

			$dados = array(
				'paginacao'=>$this->pagination->create_links(),
				'vagas'=>$vagas,
				);

			$this->load->view('vagas', $dados);	
		}
	}