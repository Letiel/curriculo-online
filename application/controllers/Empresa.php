<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Empresa extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('Login_empresa_model', 'login');
			$this->load->model('Cadastro_empresa_model', 'empresa');
			$this->load->model('Cadastro_pessoa_model', 'pessoa');
			$this->load->model('Cidades', 'cidade');

			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		}

		public function index(){
			$this->login->loggedEmpresa();
			$this->form_validation->set_rules('nome', 'Nome', 'trim|ucfirst|addslashes|required');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|addslashes|required');
			$this->form_validation->set_rules('site', 'Website', 'trim|addslashes|prep_url');
			$this->form_validation->set_rules('endereco', 'Endere&ccedil;o', 'addslashes');
			$this->form_validation->set_rules('bairro', 'Bairro', 'trim');
			$this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');
			$this->form_validation->set_rules('textempresa', 'Texto de Apresenta&ccedil;&atilde;o', 'addslashes');
			$this->form_validation->set_rules('cep', 'CEP', 'addslashes|trim');
			

			$config['upload_path'] = './img/thumbs/empresas';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '10000';
			$config['max_width'] = '1920';
			$config['max_height'] = '1080';
			$nome = uniqid('foto_');
			$config['file_name'] = $nome;

			$this->load->library('upload', $config);


			$this->form_validation->set_message('required', 'O campo "%s" é obrigat&oacute;rio!');
			$this->form_validation->set_message('valid_email', 'Email inv&aacute;lido!');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('nome', 'email', 'endereco', 'bairro', 'cidade', 'textempresa', 'site', 'cep',), $this->input->post());
				$this->empresa->update($dados);
				$nome_foto = $_FILES['foto']['name'];

				$nome_foto = $this->input->post('foto');
				$nome_foto = $_FILES['foto']['name'];
				$exp = explode('.', $nome_foto);
				$n_ext = (sizeof($exp) - 1);
				$ext = $exp[$n_ext];

				if($nome_foto != NULL && $nome_foto != ''){
					$foto = $this->upload->data();
					$this->upload->do_upload('foto');
					$this->empresa->atualiza_foto(base_url().'img/thumbs/empresas/'.$foto['file_name'].'.'.$ext);
				}
			}

			
			// $empresa = $this->empresa->get_by_id()->result();
			$empresa = $this->empresa->ver_perfil($this->session->userdata("idEmpresa"))->result();;
			$estados = $this->cidade->get_estados()->result();
			$cidades = $this->cidade->get_cidades()->result();
			$dados = array(
				'pagina'=>'empresa',
				'empresa'=>$empresa,
				'estados'=>$estados,
				'cidades'=>$cidades,
				);
			$this->load->view('editar_perfil_empresa', $dados);
		}

		public function configuracoes(){
			$this->login->loggedEmpresa();
			$msg = $this->login->verificaMensagens()->num_rows();
			if($msg > 0){
				$this->session->set_userdata('mensagem', $msg);
			}else{
				$this->session->set_userdata('mensagem', "Nada");
			}

			$dados = array('pagina'=>'configuracoes');
			$this->load->view('configuracoes_empresa', $dados);
		}

		public function config_altera_login(){
			$this->form_validation->set_rules('login', 'Login', 'required|trim|addslashes|is_unique[empresas.login]');

			$this->form_validation->set_message('required', 'O campo <b>%s</b> é obrigat&oacute;rio!');
			$this->form_validation->set_message('is_unique', 'Login já existente no sistema. Por favor informe outro.');

			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			if($this->form_validation->run() == TRUE) {
				$dados = elements(array('login'), $this->input->post());
				$this->empresa->alterar_login($dados);
				$this->session->unset_userdata('logadoEmpresa');
				echo "<div class='alert alert-success'>Login alterado com sucesso! Faça login novamente. Saindo da conta.</div><meta http-equiv='refresh' content='4; ".base_url()."'>";
			} else {
				echo validation_errors();
			}

		}

		public function config_altera_senha(){
			$this->form_validation->set_rules('senhaAntiga', 'Senha Antiga', 'required|trim|addslashes');
			$this->form_validation->set_rules('senha', 'Nova Senha', 'required|trim|addslashes|matches[conf_senha]');
			$this->form_validation->set_rules('conf_senha', 'Confirma&ccedil;&atilde;o de senha', 'required|trim|addslashes');

			$this->form_validation->set_message('required', 'O campo <b>%s</b> é obrigat&oacute;rio!');
			$this->form_validation->set_message('matches', 'As senhas não conferem');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

			if($this->form_validation->run() == TRUE) {
				$dados = elements(array('senha'), $this->input->post());
				$senha = elements(array('senhaAntiga'), $this->input->post());
				$senha['senhaAntiga'] = md5($senha['senhaAntiga']);
				$dados['senha'] = md5($dados['senha']);
				$confirma = $this->empresa->confirma_senha()->result();
				if($confirma[0]->senha == $senha['senhaAntiga']){
					$this->empresa->troca_senha($dados);
					echo '<div class="alert alert-success">Sua senha foi alterada com sucesso! Saindo da conta.</div> <meta http-equiv="refresh" content="4; '.base_url().'">';
					$this->session->unset_userdata('logadoEmpresa');
				}else{
					echo '<div class="alert alert-danger">Senha antiga inválida!</div>';
				}
			} else {
				echo validation_errors();
			}
		}

		public function senha(){
			$this->login->loggedEmpresa();
			$msg = $this->login->verificaMensagens()->num_rows();
			if($msg > 0){
				$this->session->set_userdata('mensagem', $msg);
			}else{
				$this->session->set_userdata('mensagem', "Nada");
			}

			$this->form_validation->set_rules('senhaAntiga', 'Senha Antiga', 'trim|addslashes');
			$this->form_validation->set_rules('senha', 'Nova Senha', 'trim|addslashes|matches[conf_senha]');
			$this->form_validation->set_rules('conf_senha', 'Confirma&ccedil;&atilde;o de senha', 'trim|addslashes');

			$this->form_validation->set_message('required', 'O campo "%s" é obrigat&oacute;rio!');
			$this->form_validation->set_message('matches', 'As senhas não conferem');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

			if($this->form_validation->run() == TRUE){
				$dados = elements(array('senha'), $this->input->post());
				$senha = elements(array('senhaAntiga'), $this->input->post());
				$senha['senhaAntiga'] = md5($senha['senhaAntiga']);
				$dados['senha'] = md5($dados['senha']);
				$confirma = $this->empresa->confirma_senha()->result();
				if($confirma[0]->senha == $senha['senhaAntiga']){
					$this->empresa->troca_senha($dados);
					$this->session->set_flashdata('sucesso', '<div class="alert alert-success">Sua senha foi alterada com sucesso!</div>');
					//echo $this->session->flashdata('sucesso');
					redirect('Empresa/senha');
				}else{
					$this->session->set_flashdata('sucesso', '<div class="alert alert-danger">Erro ao alterar senha!</div>');
					redirect('Empresa/senha');
				}
			}
			$dados = array('pagina'=>'configuracoes');
			$this->load->view('trocar_senha_empresa', $dados);
		}

		public function ver(){
			$perfil = $this->uri->segment(3);
			$nome = $this->uri->segment(4);
			if($this->empresa->ver_perfil($perfil)->num_rows() == 1){
				$empresa = $this->empresa->ver_perfil($perfil)->result();
				$dados = array(
					'id' => $perfil,
					'empresa'=> $empresa,
					'vagas'=> $this->empresa->ver_ultimas_vagas($perfil)->result(),
					'total_vagas'=> $this->empresa->ver_ultimas_vagas($perfil)->num_rows(),
					);
				$this->load->view('perfilEmpresa', $dados);
			}else{
				$this->session->set_flashdata('confirmado', "<div class='alert alert-danger' style='overflow: hidden; margin-top: 5px;text-align: center;'><h3>Esta empresa não existe mais!</h3></div>");
				redirect(base_url());
			}
		}


		function envia_curriculo(){

			$empresa = elements(array('empresa'), $this->input->post());
			/*if(!$this->vaga->candidatar($empresa)){
				echo "Erro ao se candidatar a vaga!\nVocê pode já ter se candidatato anteriormente a esta vaga!\nSe isto não aconteceu, entre em contato com os administradores do sistema!";
			}*/
			$emp = $this->empresa->ver_perfil($empresa['empresa'])->result();
			$emp = $emp[0];
			if($emp -> recebe_email){

				$pessoa = $this->pessoa->cria_link_curriculo()->result();
				$pessoa = $pessoa[0];
				$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º' );
				$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
				$nome = $pessoa->nome."-".$pessoa->sobrenome;
				$uri = str_replace($what, $by, $nome);
				$link = base_url()."curriculo/ver/$pessoa->id/$uri";
				if($emp->pre_cadastrado==0){
					$message = "Olá $emp->nome, você recebeu um currículo de $pessoa->nome $pessoa->sobrenome, da cidade de $pessoa->cidade - $pessoa->uf. Para ver o currículo completo de $pessoa->nome <a href='$link'> clique aqui</a>.<br/><br/>
					Caso não queira mais receber currículos do <b>Enviar Currículo</b>, <a href='".base_url()."empresa/cancela_email/$emp->codigo'>clique aqui</a>.

					<br />Caso o link acima não funcione, copie o endereço a seguir e cole no seu navegador: ".base_url()."empresa/cancela_email/$emp->codigo
					";


				}
				else{
					$message = "Olá $emp->nome, você recebeu um currículo de $pessoa->nome $pessoa->sobrenome, da cidade de $pessoa->cidade - $pessoa->uf. Para ver o currículo completo de $pessoa->nome <a href='$link'> clique aqui</a>.<br/><br/>
					Para acessar todos currículos já enviados para você, além de poder criar vagas de emprego, complete o seu cadastro <a href='".base_url()."completar/cadastro/$emp->codigo'> clicando aqui</a>.<br/><br/>
					<br />
					Caso o link acima não funcione, copie o endereço a seguir e cole no seu navegador: ".base_url()."empresa/completar/cadastro/$emp->codigo
					";

				}
				
				if($this->empresa->envia_curriculo($pessoa->id, $empresa['empresa'])){
					$subject = "Currículo de $pessoa->nome";
		            $this->load->model('Imail', 'mail');
		            $this->mail->enviar($subject,  $emp->email, $message);
		            echo "<div class='alert alert-success' style='text-align: center;'>Seu curriculo foi enviado para $emp->nome com sucesso.</div>";
				}else{
					echo "<div class='alert alert-success' style='text-align: center;'>Você já enviou um curriculo para essa empresa hoje!</div>";
				}

			}

		}

		public function cancela_email(){
			$empresa = $this->uri->segment(3);
			$dados = array(
				'codigo'=>$empresa,
				);
			$this->empresa->cancela_email($dados);
			$this->session->set_flashdata('confirmado', "<div class='alert alert-success' style='text-align: center;'><h3>Você não receberá mais emails de <b>Enviar Currículo</b>!</h3></div>");
			redirect(base_url());
		}

		public function curriculos_enviados(){
			$this->login->loggedEmpresa();
			$vaga = $this->uri->segment(2);
			$maximo = 10;
			$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

			$config['base_url'] = '/empresa/curriculos_enviados';
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['total_rows'] = $this->empresa->curriculos_enviados_all($vaga)->num_rows();
			if($config['total_rows'] == 0)
				redirect("suasvagas");
			$this->pagination->initialize($config);

			$dados = array(
				'curriculos' => $this->empresa->curriculos_enviados($maximo, $inicio, $vaga)->result(),
				'paginacao'=>$this->pagination->create_links(),
				'total'=>$config['total_rows'],
				);

			$this->load->view('curriculos_enviados', $dados);
		}

		public function excluir_conta(){
			$this->empresa->deleta_conta();
			redirect(base_url()."LogoutEmpresa");
		}

		public function update_especifico(){
			$dados = elements(array('campo', 'value'), $this->input->post());
			$this->empresa->update_dado_especifico($dados);
		}

		function editable(){
			$name = $_POST['name'];
			$value = $_POST['value'];
			$pk = $_POST['pk'];

			if($name == "nome"){
				$this->form_validation->set_rules('value', 'Nome', 'required|addslashes|max_length[100]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 100 caracteres no nome.'));
			}else if($name == "site"){
				$this->form_validation->set_rules('value', 'Site', 'addslashes|max_length[100]', array('max_length'=>'Máximo de 100 caracteres no sobrenome.'));
			}else if($name == "textempresa"){
				$this->form_validation->set_rules('value', 'Descrição', 'required|addslashes', array('required'=>'Fale um pouco sobre sua empresa.'));
			}else if($name == "endereco"){
				$this->form_validation->set_rules('value', 'Endereço', 'addslashes|max_length[100]', array('max_length'=>'Máximo de 100 caracteres!'));
			}else if($name == "bairro"){
				$this->form_validation->set_rules('value', 'Bairro', 'addslashes|max_length[50]', array('max_length'=>'Máximo de 50 caracteres!'));
			}else if($name == "complemento"){
				$this->form_validation->set_rules('value', 'Complemento', 'addslashes|max_length[100]', array('max_length'=>'Máximo de 100 caracteres!'));
			}else if($name == "cep"){
				$this->form_validation->set_rules('value', 'Cep', 'addslashes|max_length[10]', array('max_length'=>'Máximo de 10 caracteres!'));
			}else if($name == "cidade"){
				$this->form_validation->set_rules('value', 'Cidade', 'addslashes|is_numeric', array('is_numeric'=>'Erro!'));
			}
			if($this->form_validation->run() == TRUE){
				$value = $_POST['value'];
				$this->db->query("UPDATE empresas SET $name = '$value' WHERE id = $pk");
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
				// $result = array('success'=>false, 'msg'=>$name." - ". $value);
			}

			//$result = array('success'=>false, 'msg'=>"$name - $value - $pk");
			echo json_encode($result);
		}

		function troca_imagem(){
			$dados = elements(array('imagem'), $this->input->post());
			$this->form_validation->set_rules('imagem', 'Imagem', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
			if($this->form_validation->run() == TRUE){
				$empresa = $this->session->userdata("idEmpresa");
				/*GAMBIARRA AQUI*/
				$empresa = $this->empresa->ver_perfil($empresa)->first_row();
				$foto_antiga = $empresa->imagem;
				$foto_antiga = explode("/", $foto_antiga);
				$foto_antiga = $foto_antiga[(sizeof($foto_antiga) - 1)];

				unlink("./img/thumbs/empresas/".$foto_antiga);
				unlink(".".$dados['imagem']);

				$imagem = explode(".", $dados['imagem']);
				$dados['imagem'] = $imagem[0]."-avatar.".$imagem[1];
				$this->db->query("UPDATE empresas SET imagem = '$dados[imagem]' WHERE id = $empresa->id");
				/*GAMBIARRA AQUI*/
			}else{
				$this->form_validation->set_error_delimiters('', '');
				echo validation_errors();
			}
		}
	}