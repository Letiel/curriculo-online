<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Completar extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("Cadastro_empresa_model", 'empresa');
		}

		function index(){
			redirect (base_url());
		}

		function cadastro(){
			$codigo = $this->uri->segment(3);
			$this->form_validation->set_rules('login', 'login', 'required|min_length[0]|max_length[50]|addslashes|is_unique[empresas.login]', array('required'=>'Preencha o campo Login', 'is_unique'=>'Nome j&aacute; existente no sistema!'));
			$this->form_validation->set_rules('senha', 'senha!', 'required|min_length[0]|max_length[50]|strtolower', array('required'=>'Preencha o campo senha'));
			$this->form_validation->set_rules('senha2', 'senha!', 'required|min_length[0]|max_length[50]|matches[senha]|strtolower', array('required'=>'Preencha o campo de confirmação de senha', 'matches'=>'As senhas não conferem!'));
			if($this->form_validation->run() == TRUE){
				$dados = elements(array('login', 'senha'), $this->input->post());
				$dados['senha'] = md5($dados['senha']);
				$dados['codigo'] = $codigo;
				if($this->empresa->cadastra_pre($dados)){
					$this->session->set_flashdata('confirmado', "<div class='alert alert-info alert-dismissible' role='alert'>
					  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
					  <strong>Cadastro realizado com sucesso!</strong> Faça login para utilizar o sistema.
					</div>");
					redirect (base_url());
				}
			}
			$cd = array(
				'codigo'=>$codigo,
				);
			$this->load->view("completar_cadastro_pre", $cd);
		}

	}