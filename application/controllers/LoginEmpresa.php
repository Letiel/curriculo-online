<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class LoginEmpresa extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Login_empresa_model', 'login');
    }

    function index() {

        $this->form_validation->set_rules('login', 'Login', 'required|addslashes', array('required'=>'Preencha o campo de LOGIN!'));
        $this->form_validation->set_rules('senha', 'Senha', 'required', array('required'=>'Preencha o campo da SENHA!'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        
        if($this->form_validation->run() == TRUE){
            $query = $this->login->validate();
            if($query != FALSE){
                $this->session->set_userdata(array(
                    'idEmpresa'=> $query->id,
                    'loginEmpresa'=> $query->login,
                    'nomeEmpresa'=>$query->nome,
                    'nomeCidade'=> $query->cidade,
                    'idCidade'=>$query->idCidade,
                    'logadoEmpresa'=> true,
                    'email'=> $query->email,
                    ));
                redirect(base_url());
            }else{
                redirect(base_url().'LoginEmpresa/erro/1');
            }
        }else{
            redirect(base_url().'LoginEmpresa/erro/1');
        }
    }

    public function erro(){
        $dados = array(
            'codigo_erro'=> $this->uri->segment(3),
            'erro'=>true,
            );
        $this->load->view('loginEmpresa', $dados);
    }
}