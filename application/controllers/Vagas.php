<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	
	class Vagas extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Vagas_model', 'vagas');
		}

		function all(){
			$maximo = 10;
			$inicio = (!$this->uri->segment("2")) ? 0 : $this->uri->segment("2");
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/vagas";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$config['total_rows'] = $this->vagas->carrega_vagas_all()->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'pagina'=>'vagas',
				'vagas' => $this->vagas->carrega_vagas($inicio, $maximo)->result(),
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view('vagas', $dados);
		}
	}