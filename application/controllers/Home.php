<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Cidades', 'cidades');
		$this->load->model('Home_model', 'home');
		if($this->session->userdata('logadoEmpresa')){
			$this->load->model('Login_empresa_model', 'login_empresa');
			$this->login_empresa->verificaMensagens();
		}else{
			if($this->session->userdata('logado')){
				$this->load->model('Login_model', 'login_user');
				$this->login_user->verificaMensagens();
			}
		}
	}

	public function index(){
		$dados = array(
			'pagina'=>'home',
			'estados'=>$this->cidades->get_estados()->result(),
			'curriculos'=>$this->home->carrega_ultimos_curriculos()->result(),
			'vagas'=>$this->home->carrega_ultimas_vagas()->result(),
			'total_vagas'=>$this->home->carrega_ultimas_vagas()->num_rows(),
			'empresas'=>$this->home->carrega_ultimas_empresas()->result(),
		);
		$this->load->view('home', $dados);
	}
	
	public function ajax_cidades(){
		$estado = $this->uri->segment(3);
		$dados = array(
			'cidades'=>$this->cidades->cata_cidades($estado)->result(),
		);
		
		$this->load->view('ajax/cidades', $dados);
	}
}