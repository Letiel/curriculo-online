<?php
class Login_empresa_model extends CI_Model {

    # VALIDA USUÁRIO
    function validate() {
        //$this->db->where('login', $this->input->post('login')); 
        //$this->db->where('senha', md5($this->input->post('senha')));
        //$this->db->where('bloqueado', 0); 
        //$this->db->where('confirmado', 1);// Verifica o status do usuário

        //$query = $this->db->get('usuarios')->row(1);

        $login = $this->input->post('login');
        $senha = md5($this->input->post('senha'));
        $query = $this->db->query("SELECT empresas.login, empresas.id, empresas.nome, cidade.nome as cidade, cidade.id as idCidade, empresas.email FROM empresas JOIN cidade ON (cidade.id = empresas.cidade) WHERE (login = '$login' OR email = '$login') AND senha = '$senha' AND bloqueado = 0 AND (confirmado = 1 OR pre_cadastrado = 1)");
        if($query->num_rows() == 1)
            return $query->row(1);
        else
            return false;
        /*if ($query->num_rows == 1) { 
            return true; // RETORNA VERDADEIRO
        }*/
    }

    function verificaMensagens(){
        if($this->session->userdata('logadoEmpresa')){
            $user = $this->session->userdata('idEmpresa');
            $query = $this->db->query("SELECT * FROM mensagens WHERE empresa = $user AND lida = 0 AND lado = 0");
            if($query->num_rows() > 0)
                $this->session->set_flashdata('mensagem', $query->num_rows());
            else
                $this->session->set_flashdata('mensagem', 0);
            return $query;
        }
    }

    # VERIFICA SE O USUÁRIO ESTÁ LOGADO
    function loggedEmpresa() {
        $logged = $this->session->userdata('logadoEmpresa');
        $this->verificaMensagens();

        if (!isset($logged) || $logged != true) {
            redirect('http://www.icurriculumvitae.com.br');
            die();
        }
    }
}