<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class cadastro_pessoa_model extends CI_Model{
		public function cadastrar($dados = NULL){
			if($dados != null){
				$data = explode("/", $dados['dt_nascimento']);
				$ano = $data[2];
				$mes = $data[1];
				$dia = $data[0];
				$dados['dt_nascimento'] = date('Y-m-d', strtotime("$ano-$mes-$dia"));
				$this->db->insert("usuarios", $dados);
				$message = 'Ol&aacute;  ' . $dados['nome'] . ',<br/><br/>
						Para confirmar seu cadastro, por favor, clique no link abaixo:<br/>
						<a href="'.base_url().'Confirma/user/'.$dados['codigo'].'">Clique aqui para confirmar seu cadastro</a><br/><br/>

						Caso o link acima não funcione, copie o endereço a seguir e cole no seu navegador: '.base_url().'Confirma/user/'.$dados['codigo'] .'<br/>

						Se n&atilde;o tiver sido voc&ecirc; ou n&atilde;o desejar confirmar o cadastro, apenas desconsidere esta mensagem.<br/><br/>
						Muito obrigado!';

            	$subject = 'Confirmação de Cadastro';
				$this->load->model('Imail', 'mail');
				$this->mail->enviar($subject,  $dados['email'], $message);
				$this->session->set_flashdata('confirmado', "<div class='alert alert-info'>Foi enviada uma mensagem de confirmação para o seu e-mail!<br />Caso não encontre o link, procure na caixa de spam.</div>");
				redirect(base_url().'Confirma/email');
			}
		}

		public function cancela_email($dados = NULL){
			if($dados != NULL){
				$this->db->query("UPDATE usuarios SET recebe_email_massa_cidade = 0 WHERE codigo = '$dados[codigo]'");
			}
		}

		public function get_by_id(){
			$id = $this->session->userdata('id');
			return $this->db->query("SELECT usuarios.id, usuarios.nome, usuarios.sobrenome, usuarios.cpf, estado.uf as uf, estado_civil.descricao as estado_civil, estado_civil.id as idEstado_civil, usuarios.filhos, 
				usuarios.dt_nascimento, usuarios.sexo, usuarios.telefone, usuarios.celular, usuarios.cep, estado.nome as estado, cidade.nome as cidade, cidade.id as idCidade, 
				estado.id as idEstado, usuarios.bairro, usuarios.endereco, usuarios.numero, usuarios.complemento, usuarios.foto, escolaridade.descricao as escolaridade, escolaridade.id as idEscolaridade, 
				usuarios.curso_nome, usuarios.curso_concluido, usuarios.experiencias, usuarios.idiomas, usuarios.hab_a, usuarios.hab_b, usuarios.hab_c, 
				usuarios.hab_d, usuarios.hab_e, usuarios.carro, usuarios.moto, usuarios.caminhao, usuarios.outro_veiculo, usuarios.disp_viajar, usuarios.login, 
				usuarios.email, usuarios.link_facebook, usuarios.confirmado, usuarios.bloqueado, usuarios.deficiente as deficiente FROM usuarios LEFT JOIN cidade ON (cidade.id = usuarios.cidade) 
				LEFT JOIN estado ON (estado.id = cidade.estado) LEFT JOIN estado_civil ON (estado_civil.id = usuarios.estado_civil) 
				LEFT JOIN escolaridade ON (escolaridade.id = usuarios.escolaridade) WHERE usuarios.id = $id LIMIT 1");
		}

		public function cria_link_curriculo(){
			$id = $this->session->userdata('id');
			return $this->db->query("SELECT usuarios.id, usuarios.nome, usuarios.sobrenome, cidade.nome as cidade, estado.uf as uf FROM usuarios JOIN cidade ON (cidade.id = usuarios.cidade) JOIN estado ON (estado.id = cidade.estado) WHERE usuarios.id = $id LIMIT 1");
		}

		public function ver_curriculo($id = NULL){
			return $this->db->query("SELECT usuarios.id, usuarios.nome, usuarios.sobrenome, usuarios.cpf, estado.uf as uf, estado_civil.descricao as estado_civil, estado_civil.id as Estado_civil, usuarios.filhos, 
				usuarios.dt_nascimento, usuarios.sexo, usuarios.telefone, usuarios.celular, usuarios.cep, estado.nome as estado, cidade.nome as cidade, cidade.id as idCidade, 
				estado.id as idEstado, usuarios.bairro, usuarios.endereco, usuarios.numero, usuarios.complemento, usuarios.foto, escolaridade.descricao as escolaridade, escolaridade.id as idEscolaridade, 
				usuarios.curso_nome, usuarios.curso_concluido, usuarios.experiencias, usuarios.idiomas, usuarios.hab_a, usuarios.hab_b, usuarios.hab_c, 
				usuarios.hab_d, usuarios.hab_e, usuarios.carro, usuarios.moto, usuarios.caminhao, usuarios.outro_veiculo, usuarios.disp_viajar, usuarios.login, 
				usuarios.email, usuarios.link_facebook, usuarios.confirmado, usuarios.bloqueado, usuarios.deficiente as deficiente, usuarios.recebe_email_massa_cidade, usuarios.autonomo FROM usuarios LEFT JOIN cidade ON (cidade.id = usuarios.cidade) 
				LEFT JOIN estado ON (estado.id = cidade.estado) LEFT JOIN estado_civil ON (estado_civil.id = usuarios.estado_civil) 
				LEFT JOIN escolaridade ON (escolaridade.id = usuarios.escolaridade) WHERE usuarios.id = $id LIMIT 1");
		}

		public function ver_exp($id = NULL){
			return $this->db->query("SELECT * FROM experiencias WHERE usuario = $id");
		}

		public function ver_cursos($id = NULL){
			return $this->db->query("SELECT cursos.nome, curso_usuario.instituicao FROM cursos JOIN curso_usuario ON (curso_usuario.curso = cursos.id) WHERE curso_usuario.usuario = $id ORDER BY cursos.nome ASC");
		}

		public function ver_idiomas($id = NULL){
			return $this->db->query("SELECT * FROM idiomas JOIN idioma_usuario ON (idioma_usuario.idioma = idiomas.id) WHERE idioma_usuario.usuario = $id ORDER BY idiomas.nome ASC");
		}

		public function update_curriculo($dados = NULL){
			if($dados != NULL){
				$this->db->where('id', $this->session->userdata('id'));
				$this->db->update('usuarios', $dados);
			}
		}

		public function atualiza_foto($string = NULL){
			if($string != NULL){
				$usuario = $this->session->userdata('id');
				$this->db->query("UPDATE usuarios SET foto = '$string' WHERE id = $usuario");
			}
		}

		public function alterar_login($dados){
			$this->db->where('id', $this->session->userdata('id'));
			$this->db->update('usuarios', $dados);
		}

		public function troca_senha($dados){
			$this->db->where('id', $this->session->userdata('id'));
			$this->db->update('usuarios', $dados);
		}

		public function confirma_senha(){
			$id = $this->session->userdata('id');
			return $this->db->query("SELECT * FROM usuarios WHERE id = $id");
		}

		public function confirmaEmail($dados = NULL){
			if($dados != NULL){
				$codigo = $dados['codigo'];
				$this->db->query("UPDATE usuarios SET confirmado = 1 WHERE codigo = '$codigo'");
			}
		}

		public function cadastraCurso($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');
				$curso = $dados['curso'];
				$conc = $dados['concluido'];
				$instituicao = $dados['instituicao'];
				$this->db->query("INSERT INTO curso_usuario (curso, usuario, concluido, instituicao) VALUES ($curso, $user, $conc, '$instituicao')");
			}
		}

		public function cata_cursos(){
			//return $this->db->get('cursos');
			$user = $this->session->userdata('id');
			$query = $this->db->query("SELECT * FROM cursos WHERE id NOT IN (SELECT curso FROM curso_usuario WHERE usuario = $user) ORDER BY cursos.nome ASC");
			return $query;
		}

		public function cata_cursos_by_user(){
			$user = $this->session->userdata('id');
			return $query = $this->db->query("SELECT cursos.nome as nome, cursos.id as id, curso_usuario.concluido, curso_usuario.instituicao FROM curso_usuario JOIN cursos ON (cursos.id = curso_usuario.curso) WHERE curso_usuario.usuario = $user ORDER BY cursos.nome ASC");
		}

		public function deletaCurso($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');
				$curso = $dados['curso'];
				$this->db->query("DELETE FROM curso_usuario WHERE usuario = $user AND curso = $curso");
			}
		}

		public function cadastra_novo_curso($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');
				$nome_curso = $dados['nome_curso'];
				$concluido = $dados['concluido'];
				$instituicao = $dados['instituicao'];
				$this->db->query("INSERT INTO cursos (nome) VALUES ('$nome_curso')");
				$id = $this->db->insert_id();
				$this->db->query("INSERT INTO curso_usuario (curso, usuario, concluido, instituicao) VALUES ($id, '$user', '$concluido', '$instituicao')");
			}
		}

		public function cadastra_nova_area($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');
				$nome_area = $dados['area'];
				$this->db->query("INSERT INTO areas (descricao) VALUES ('$nome_area')");
				$id = $this->db->insert_id();
				$this->db->query("INSERT INTO usuario_area (area, usuario) VALUES ($id, '$user')");
			}
		}

		public function cadastra_experiencia($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');
				$descricao = $dados['descricao'];
				$funcao = $dados['funcao'];
				$empresa = $dados['empresa'];

				$cidade = $dados['cidade'];
				$telefone = $dados['telefone'];
				$endereco = $dados['endereco'];

				$inicio = date("Y-m-d",strtotime(str_replace('/','-',$dados['inicio'])));
				if($dados['final_'] != NULL && $dados['final_'] != "")
					$final = date("Y-m-d",strtotime(str_replace('/','-',$dados['final_'])));
				else
					$final = null;

				//$final = date('Y-m-d', strtotime($dados['final']));
				$this->db->query("INSERT INTO experiencias (usuario, descricao, inicio, final, funcao, empresa, cidade, telefone, endereco) VALUES ($user, '$descricao', '$inicio', '$final', '$funcao', '$empresa', $cidade, '$telefone', '$endereco')");
			}
		}

		public function cadastra_servico($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');

				// $dados['inicio'] = date("Y-m-d",strtotime(str_replace('/','-',$dados['inicio'])));
				$dados['usuario'] = $user;
				//$final = date('Y-m-d', strtotime($dados['final']));
				$data = explode("/", $dados['inicio']);
				$ano = $data[2];
				$mes = $data[1];
				$dia = $data[0];
				$dados['inicio'] = date('Y-m-d', strtotime("$ano-$mes-$dia"));
				$this->db->query("INSERT INTO servicos_usuarios (titulo_servico, descricao_servico, lugares, contato, inicio, usuario) VALUES ('$dados[titulo_servico]', '$dados[descricao_servico]', '$dados[lugares]', '$dados[contato]',' $dados[inicio]', $user)");
				// $this->db->insert("servicos_usuarios", $dados);
			}
		}

		public function cata_experiencias_by_user(){
			$user = $this->session->userdata('id');
			return $this->db->query("SELECT * FROM experiencias WHERE usuario = $user");
		}

		public function deletaExp($dados = NULL){
			if($dados != NULL){
				$id = $dados['id'];
				$this->db->query("DELETE FROM experiencias WHERE id = $id");
			}
		}

		public function deletaServico($dados = NULL){
			if($dados != NULL){
				$id = $dados['id'];
				$this->db->query("DELETE FROM servicos_usuarios WHERE id = $id");
			}
		}

		public function cadastra_idioma_usuario($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');
				$id = $dados['idioma'];
				$this->db->query("INSERT INTO idioma_usuario (idioma, usuario) VALUES ($id, $user)");
			}
		}

		public function cadastra_area_usuario($dados = NULL){
			if($dados != NULL){
				$user = $this->session->userdata('id');
				$id = $dados['area'];
				$this->db->query("INSERT INTO usuario_area (area, usuario) VALUES ($id, $user)");
			}
		}

		public function lista_select_idiomas(){
			$user = $this->session->userdata('id');
			return $this->db->query("SELECT * FROM idiomas WHERE id NOT IN (SELECT idioma FROM idioma_usuario WHERE usuario = $user) ORDER BY nome ASC");
		}

		public function cata_idiomas_by_user(){
			$user = $this->session->userdata('id');
			return $this->db->query("SELECT idiomas.nome, idiomas.id FROM idioma_usuario JOIN idiomas ON (idiomas.id = idioma_usuario.idioma) WHERE idioma_usuario.usuario = $user");
		}

		public function cata_areas_by_user(){
			$user = $this->session->userdata('id');
			return $this->db->query("SELECT * FROM usuario_area JOIN areas ON (areas.id = usuario_area.area) WHERE usuario_area.usuario = $user AND areas.desativado = 0");
		}

		public function deletaArea($dados = NULL){
			if($dados != NULL){
				$area = $dados['area'];
				$user = $this->session->userdata('id');
				$this->db->query("DELETE FROM usuario_area WHERE area = $area AND usuario = $user");
			}
		}

		public function deletaIdioma($dados = NULL){
			if($dados != NULL){
				$idioma = $dados['idioma'];
				$user = $this->session->userdata('id');
				$this->db->query("DELETE FROM idioma_usuario WHERE idioma = $idioma AND usuario = $user");
			}
		}

		public function get_mensagens($maximo, $inicio){
			$user = $this->session->userdata('id');
			return $this->db->query("SELECT mensagens.id as id_mensagem, mensagens.mensagem, mensagens.data, mensagens.lida, mensagens.respondida, 
				mensagens.lado, usuarios.nome as usuario, empresas.nome as empresa, empresas.id as idEmpresa, usuarios.id as idUsuario 
				FROM mensagens JOIN usuarios ON (usuarios.id = mensagens.usuario) JOIN empresas ON (empresas.id = mensagens.empresa) 
				WHERE mensagens.usuario = $user ORDER BY mensagens.id DESC LIMIT $inicio, $maximo");
		}

		public function get_mensagens_all(){
			$user = $this->session->userdata('id');
			return $this->db->query("SELECT mensagens.id as id_mensagem, mensagens.mensagem, mensagens.data, mensagens.lida, mensagens.respondida, 
				mensagens.lado, usuarios.nome as usuario, empresas.nome as empresa, empresas.id as idEmpresa, usuarios.id as idUsuario 
				FROM mensagens JOIN usuarios ON (usuarios.id = mensagens.usuario) JOIN empresas ON (empresas.id = mensagens.empresa) 
				WHERE mensagens.usuario = $user ORDER BY mensagens.id DESC");
		}

		public function dados_para_exclusao(){
			$id = $this->session->userdata("id");
			return $this->db->query("SELECT * FROM usuarios WHERE id = $id");
		}

		public function excluir_conta($pessoa = NULL){
			if($pessoa != NULL){
				$dados = $pessoa[0];
				// $this->db->query("INSERT INTO excluidos SELECT * FROM usuarios WHERE id = $dados->id");
				$this->db->query("DELETE FROM usuarios WHERE id = $dados->id");
				$this->session->set_flashdata("confirmado", "<div class='alert alert-success'><h3>Conta excluída com sucesso!</h3></div>");
				//$this->db->query("INSERT INTO excluidos (confirmado, nome, sobrenome, cpf, estado_civil, filhos, dt_nascimento, sexo, telefone, celular, cep, estado, cidade, bairro, endereco, numero, complemento, foto, escolaridade, curso_nome, curso_concluido, experiencias, idiomas, hab_a, hab_b, hab_c, hab_d, hab_e, carro, moto, caminhao, outro_veiculo, disp_viajar, login, senha, email, link_facebook, bloqueado, deficiente, desativado, codigo) VALUES ($dados->confirmado, '$dados->nome', '$dados->sobrenome', '$dados->cpf', $dados->estado_civil, $dados->filhos, '$dados->dt_nascimento', '$dados->sexo', '$dados->telefone', '$dados->celular', '$dados->cep', $dados->estado, $dados->cidade, '$dados->bairro', '$dados->endereco', '$dados->numero', '$dados->complemento', '$dados->foto', $dados->escolaridade, '$dados->curso_nome', $dados->curso_concluido, '$dados->experiencias', '$dados->idiomas', $dados->hab_a, $dados->hab_b, $dados->hab_c, $dados->hab_d, $dados->hab_e, $dados->carro, $dados->moto, $dados->caminhao, $dados->outro_veiculo, $dados->disp_viajar, '$dados->login', '$dados->senha', '$dados->email', '$dados->link_facebook', $dados->bloqueado, $dados->deficiente, $dados->desativado, '$dados->codigo')");
			}
		}

		public function update_dado_especifico($campo = NULL, $value = NULL){
			if($campo != NULL && $value != NULL){
				$id = $this->session->userdata("id");
				if($value == "cidade")
					$this->db->query("UPDATE usuarios SET $campo = $value WHERE id = $id");
				else
					$this->db->query("UPDATE usuarios SET $campo = '$value' WHERE id = $id");
			}
		}

		function autonomo($id = NULL){
			if($id == NULL)
				$id = $this->session->userdata("id");
			if($id != NULL && is_numeric($id)){
				// $this->db->where("usuario", $id);
				// return $this->db->get("servicos_usuarios");
				$query = $this->db->query("SELECT * FROM servicos_usuarios WHERE usuario = $id");
				$result = $query->first_row();
				$user = $this->db->query("SELECT autonomo FROM usuarios WHERE id = $id")->first_row();
				if($query->num_rows() > 0) {
					$set = 1;
				} else if($query->num_rows() == 0) {
					$set = 0;
				}
				$this->db->query("UPDATE usuarios SET autonomo = $set WHERE id = $id");
				return $query;
			}
		}

		function get_all_areas(){
			return $this->db->query("SELECT * FROM `areas` WHERE `desativado` = '0'");
		}

		function get_areas_usuario($id = NULL){
			if($id == NULL)
				$id = $this->session->userdata("id");
			return $this->db->query("SELECT * FROM `usuario_area` JOIN `areas` ON `areas`.`id` = `usuario_area`.`area` WHERE `desativado` = '0' AND `usuario` = '$id' ORDER BY `descricao` ASC");
		}
	}