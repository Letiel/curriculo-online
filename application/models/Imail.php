<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imail extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function enviar($assunto, $para, $mensagem){
		
			$this->load->library('email');

			
			

            // Get full html:
			$body =
			'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			    <title>'.htmlspecialchars($assunto, ENT_QUOTES, $this->email->charset).'</title>
			    <style type="text/css">
			        body {
			            font-family: Arial, Verdana, Helvetica, sans-serif;
			            font-size: 16px;
			        }
			    </style>
			</head>
			<body>
			'.$mensagem.'
			</body>
			</html>';
            // Also, for getting full html you may use the following internal method:
            //$body = $this->email->full_html($subject, $message);

            $result = $this->email
                // ->from('nao-responder@oqueeecomofazer.com.br')
                ->from('nao-responder@curriculoonline.net.br')
                ->to($para)
                ->subject($assunto)
                ->message($body)
                ->send();


}
	
	
}