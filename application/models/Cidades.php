<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cidades extends CI_Model{
	public function get_estados(){		
		$this->db->order_by('nome', 'asc');
		return $this->db->get('estado');
	}
	
	public function cata_cidades($estado){
		$this->db->order_by('nome', 'asc');
		return $this->db->query("SELECT * FROM cidade WHERE estado = $estado");
	}

	public function get_cidades(){
		$this->db->order_by('nome', 'asc');
		return $this->db->query("SELECT cidade.id, cidade.nome, cidade.estado, estado.uf as estado FROM cidade JOIN estado ON (cidade.estado = estado.id)");
	}

	public function cidade_select2($pesquisa){
		$this->db->order_by('nome', 'asc');
		return $this->db->query("SELECT cidade.id, cidade.nome, cidade.estado, estado.uf as estado FROM cidade JOIN estado ON (cidade.estado = estado.id) WHERE cidade.nome LIKE '%$pesquisa%'");
	}

	public function nome_cidade($id = null){
		if($id != NULL)
			return $this->db->query("SELECT cidade.nome, cidade.id FROM cidade WHERE id = $id");
	}
}