<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Empresa extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function ver($id){
			return $this->db->query("SELECT empresas.nome, empresas.imagem, empresas.email FROM empresas LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) JOIN  WHERE empresas.id = $id");
		}

		function listar($maximo, $inicio){
			return $this->db->query("SELECT empresas.nome, cidade.nome as cidade, estado.uf as uf, empresas.imagem, empresas.id FROM empresas LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) WHERE empresas.bloqueado = 0 AND (empresas.confirmado = 1 OR empresas.pre_cadastrado = 1) ORDER BY empresas.id DESC LIMIT $inicio, $maximo");
		}

		function listar_all(){
			return $this->db->query("SELECT empresas.nome, cidade.nome as cidade, estado.uf as uf, empresas.imagem, empresas.id FROM empresas LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) WHERE empresas.bloqueado = 0 AND (empresas.confirmado = 1 OR empresas.pre_cadastrado = 1) ORDER BY empresas.id DESC");
		}
	}