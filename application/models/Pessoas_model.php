<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Pessoas_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function cata_pessoas_all(){
		return $this->db->query("SELECT * FROM usuarios WHERE bloqueado = 0 AND desativado = 0");
	}

	function cata_pessoas($maximo, $inicio){
		return $this->db->query("SELECT * FROM usuarios WHERE bloqueado = 0 AND desativado = 0 ORDER BY confirmado DESC, id DESC LIMIT $inicio, $maximo");
	}
}