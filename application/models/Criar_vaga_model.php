<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Criar_vaga_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function criaVaga($dados){
			$this->db->insert('vagas', $dados);
		}

		function cata_vagas($maximo, $inicio){
			//$this->db->get('vagas');
			return $this->db->query("SELECT vagas.id, vagas.descricao, vagas.cargo, empresas.nome as nome, empresas.id as idEmpresa, empresas.imagem FROM vagas JOIN empresas ON (empresas.id = vagas.empresa) WHERE desativado = 0 LIMIT $inicio, $maximo");
		}

		function cata_vagas_all(){
			//$this->db->get('vagas');
			return $this->db->query("SELECT vagas.id, vagas.descricao, vagas.cargo, empresas.nome as nome, empresas.id as idEmpresa, empresas.imagem FROM vagas JOIN empresas ON (empresas.id = vagas.empresa) WHERE desativado = 0");
		}

		function cata_areas(){
			return $this->db->query("SELECT * FROM areas WHERE desativado = 0");
		}

		function cadastra_area($dados = NULL){
			if($dados != NULL){
				$area = $dados['nome_area'];
				$this->db->query("INSERT INTO areas (descricao) VALUES ('$area')");
			}
		}

		function ver_vaga($id = NULL){
			if($id != NULL){
				return $this->db->query("SELECT vagas.codigo as codigo, vagas.id as id_vaga, empresas.id as id_empresa, empresas.email, 
					empresas.nome as nome_empresa, vagas.descricao, vagas.cargo, empresas.nome, areas.descricao as area, 
					empresas.imagem, cidade.nome as cidade, estado.uf as uf, estado.nome as estado, vagas.especial, 
					vagas_especiais.titulo as titulo_especial, vagas_especiais.descricao as descricao_especial, 
					vagas_especiais.email_empresa as email_empresa_especial, 
					vagas_especiais.nome_empresa as nome_empresa_especial, vagas_especiais.imagem as imagem_especial, 
					cidade_especial.nome as nome_cidade_especial, estado_especial.uf as uf_estado_especial 
					FROM vagas LEFT JOIN empresas ON (empresas.id = vagas.empresa) 
					LEFT JOIN areas ON (areas.id = vagas.area) LEFT JOIN cidade ON (cidade.id = empresas.cidade) 
					LEFT JOIN estado ON (estado.id = cidade.estado) 
					LEFT JOIN vagas_especiais ON (vagas_especiais.vaga_id = vagas.id) 
					LEFT JOIN cidade as cidade_especial ON (cidade_especial.id = vagas_especiais.cidade) 
					LEFT JOIN estado as estado_especial ON (estado_especial.id = cidade_especial.estado) 
					WHERE vagas.id = $id");
			}
		}

		function ver_vagas_empresa(){
			$id = $this->session->userdata('idEmpresa');
			return $this->db->query("SELECT COUNT(usuario_vaga.id) as total, vagas.id as id, vagas.descricao, vagas.cargo, vagas.moderado, vagas.bloqueado, empresas.nome as empresa, areas.descricao as area, vagas.desativado FROM vagas 
				LEFT JOIN usuario_vaga ON (usuario_vaga.vaga = vagas.id) 
				JOIN empresas ON (vagas.empresa = empresas.id)
				JOIN areas ON (areas.id = vagas.area) 
			WHERE vagas.empresa = $id GROUP BY vagas.id ORDER BY vagas.id DESC");
		}

		function deleta_vaga_empresa($id = NULL){
			if($id != NULL){
				$empresa = $this->session->userdata('idEmpresa');
				$this->db->query("UPDATE vagas SET desativado = NOT desativado WHERE id = $id AND empresa = $empresa");
			}
		}

		function candidatar($dados = NULL){
			if($dados != NULL){
				try{
					$candidato = $this->session->userdata('id');
					$vaga = $dados['vaga'];
					$data = date("Y-m-d");
					if(!$this->db->query("INSERT INTO usuario_vaga (usuario, vaga, data) VALUES ($candidato, $vaga, '$data')"))
						return false;
					else
						return true;
				}
				catch (Exception $e){
					exit("Erro!");
				}
			}
		}

		function teste_candidato($vaga = NULL){
			if($vaga != NULL){
				$usuario = $this->session->userdata('id');
				return $this->db->query("SELECT * FROM usuario_vaga WHERE usuario = $usuario AND vaga = $vaga");
			}
		}

		function desativar_vaga($vaga){
			$this->db->query("UPDATE vagas SET desativado = 1 WHERE codigo = '$vaga'");
			$this->session->set_flashdata("confirmado", "<div class='alert alert-success'><b>Sucesso!</b> Sua vaga foi cancelada e agora você não receberá mais notificações sobre ela!</div>");
			redirect(base_url());
		}

		function alterar_area_vaga($dados){
			$this->db->query("UPDATE vagas SET area = $dados[nova_area] WHERE vagas.id = $dados[id_vaga]");
		}
	}