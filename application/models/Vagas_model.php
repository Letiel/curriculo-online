<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Vagas_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function carrega_vagas_all(){
			return $this->db->query("SELECT vagas.cargo, vagas.id, empresas.nome as empresa, areas.descricao as area, vagas.descricao as descricao, empresas.imagem, cidade.nome as cidade, estado.uf, vagas_especiais.titulo as titulo_especial, vagas_especiais.cidade, vagas_especiais.nome_empresa, vagas_especiais.imagem as imagem_especial, cidade_especial.nome as cidade_especial_nome, estado_especial.uf as uf_estado_especial, vagas.especial, vagas_especiais.descricao as descricao_especial FROM vagas LEFT JOIN empresas ON (empresas.id = vagas.empresa) LEFT JOIN areas ON (areas.id = vagas.area) LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) LEFT JOIN vagas_especiais ON (vagas_especiais.vaga_id = vagas.id) LEFT JOIN cidade as cidade_especial ON (cidade_especial.id = vagas_especiais.cidade) LEFT JOIN estado as estado_especial ON (estado_especial.id = cidade_especial.estado) WHERE vagas.bloqueado = 0 AND vagas.desativado = 0 ORDER BY vagas.id DESC");
		}

		function carrega_vagas($inicio = NULL, $maximo = NULL){
			return $this->db->query("SELECT vagas.cargo, vagas.id, empresas.nome as empresa, areas.descricao as area, vagas.descricao as descricao, empresas.imagem, cidade.nome as cidade, estado.uf, vagas_especiais.titulo as titulo_especial, vagas_especiais.cidade, vagas_especiais.nome_empresa, vagas_especiais.imagem as imagem_especial, cidade_especial.nome as cidade_especial_nome, estado_especial.uf as uf_estado_especial, vagas.especial, vagas_especiais.descricao as descricao_especial FROM vagas LEFT JOIN empresas ON (empresas.id = vagas.empresa) LEFT JOIN areas ON (areas.id = vagas.area) LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) LEFT JOIN vagas_especiais ON (vagas_especiais.vaga_id = vagas.id) LEFT JOIN cidade as cidade_especial ON (cidade_especial.id = vagas_especiais.cidade) LEFT JOIN estado as estado_especial ON (estado_especial.id = cidade_especial.estado) WHERE vagas.bloqueado = 0 AND vagas.desativado = 0 ORDER BY vagas.id DESC LIMIT $inicio, $maximo");		
		}
	}