<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Adm_model extends CI_Model{
		function __construct(){
			parent::__construct();
			$this->load->model('Imail', 'mail');
		}

		function logar($dados = NULL){
			if($dados != NULL)
				return $this->db->query("SELECT * FROM usuarios_adm WHERE login LIKE '$dados[login]' AND senha = '$dados[senha]'");
		}

		function cadastraEmpresa($dados = NULL){
			if($dados != NULL){
				$message = "Ol&aacute;  " . $dados['nome'] . ",<br/><br/>
						Sua empresa foi cadastrada no <a href='http://www.icurriculumvitae.com.br/completar/cadastro/$dados[codigo]'>I Curriculum Vitae!</a>
						<br />
						Seu cadastro ainda não está totalmente efetivado. Para efetivar seu cadastro e utilizar o sistema, por favor clique <a href='http://www.icurriculumvitae.com.br/completar/cadastro/$dados[codigo]'>AQUI.</a>
						<br />
						Se não desejar receber mais e-mails do sistema <a href='http://www.icurriculumvitae.com.br/empresa/cancela_email/$dados[codigo]'>clique aqui</a>";

            	$subject = 'Confirmação de Cadastro';
				$this->mail->enviar($subject,  $dados['email'], $message);

				$this->db->query("INSERT INTO empresas (nome, email, endereco, bairro, cidade, textempresa, site, cep, cnpj, pre_cadastrado, codigo) VALUES ('$dados[nome]', '$dados[email]', '$dados[endereco]', '$dados[bairro]', '$dados[cidade]', '$dados[textempresa]', '$dados[site]', '$dados[cep]', '$dados[cnpj]', 1, '$dados[codigo]')");
				$this->session->set_flashdata("last_id", $this->db->insert_id());
			}
		}

		function atualiza_foto($endereco){
			$id = $this->session->flashdata("last_id");
			$this->db->query("UPDATE empresas SET imagem = '$endereco' WHERE id = $id");
		}

		function carrega_empresas(){
			return $this->db->query("SELECT * FROM empresas");
		}

		function update_foto($id = NULL, $nome = NULL){
			if($id != NULL && $nome != NULL){
				$this->db->query("UPDATE empresas SET imagem = '$nome' WHERE id = $id");
			}
		}

		function carrega_usuarios(){
			return $this->db->query("SELECT * FROM usuarios WHERE id > 1");
		}

		function cadastra_vaga_especial($dados = NULL){
			if($dados != NULL){
				$codigo = uniqid();
				$this->db->query("INSERT INTO vagas (descricao, area, desativado, especial, codigo) VALUES ('', '', 0, 1, '$codigo')");
				$inserido = $this->db->insert_id();
				$this->db->query("INSERT INTO vagas_especiais (vaga_id, titulo, descricao, email_empresa, nome_empresa, cidade, imagem) VALUES 
					($inserido, '$dados[titulo]', '$dados[descricao]', '$dados[email]', '$dados[nome_empresa]', $dados[cidade], '$dados[foto]')");
			}
		}

		//------------------------------ SITEMAP ------------------------//

		function sitemap_empresas(){
			return $this->db->query("SELECT * FROM empresas WHERE bloqueado = 0 ORDER BY id DESC");
		}

		function sitemap_empresas_pag($inicio, $maximo){
			return $this->db->query("SELECT * FROM empresas WHERE bloqueado = 0 ORDER BY id DESC LIMIT $inicio, $maximo");
		}

		function sitemap_curriculos(){
			return $this->db->query("SELECT * FROM usuarios WHERE bloqueado = 0 AND desativado = 0 ORDER BY id DESC");
		}

		function sitemap_curriculos_pag($inicio, $maximo){
			return $this->db->query("SELECT * FROM usuarios WHERE bloqueado = 0 AND desativado = 0 ORDER BY id DESC LIMIT $inicio, $maximo");
		}

		function sitemap_vagas(){
			return $this->db->query("SELECT * FROM vagas WHERE bloqueado = 0 AND desativado = 0 ORDER BY id DESC");
		}

		function sitemap_vagas_pag($inicio, $maximo){
			return $this->db->query("SELECT * FROM vagas WHERE bloqueado = 0 AND desativado = 0 ORDER BY id DESC LIMIT $inicio, $maximo");
		}

		function sitemap_lista_usuarios(){
			return $this->db->query("SELECT * FROM usuarios ORDER BY id DESC");
		}

		function sitemap_lista_usuarios_pag($inicio, $maximo){
			return $this->db->query("SELECT * FROM usuarios ORDER BY id DESC LIMIT $inicio, $maximo");
		}

		function sitemap_lista_empresas(){
			return $this->db->query("SELECT * FROM empresas ORDER BY id DESC");
		}

		function sitemap_lista_empresas_pag($inicio, $maximo){
			return $this->db->query("SELECT * FROM empresas ORDER BY id DESC LIMIT $inicio, $maximo");
		}

		function sitemap_lista_vagas(){
			return $this->db->query("SELECT * FROM vagas ORDER BY id DESC");
		}

		function sitemap_lista_vagas_pag($inicio, $maximo){
			return $this->db->query("SELECT * FROM vagas ORDER BY id DESC LIMIT $inicio, $maximo");
		}

		//------------------------------ SITEMAP ------------------------//
	}