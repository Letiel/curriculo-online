<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cata_escolaridade_model extends CI_Model{
	public function get_escolaridade(){		
		$this->db->order_by('descricao', 'asc');
		return $this->db->get('escolaridade');
	}
}