<?php
class Login_model extends CI_Model {

    # VALIDA USUÁRIO
    function validate() {
        $login = $this->input->post('login');
        $senha = md5($this->input->post('senha'));
        $query = $this->db->query("SELECT usuarios.id, usuarios.login, usuarios.nome, usuarios.sobrenome, cidade.nome as cidade, cidade.id as idCidade, usuarios.email FROM usuarios JOIN cidade ON (cidade.id = usuarios.cidade) WHERE (usuarios.login = '$login' OR usuarios.email = '$login') AND usuarios.senha = '$senha' AND usuarios.bloqueado = 0 AND usuarios.confirmado = 1");
        if($query->num_rows() == 1)
            return $query->row(1);
        else
            return false;
    }

    function verificaMensagens(){
        if($this->session->userdata('logado')){
            $user = $this->session->userdata('id');
            $query = $this->db->query("SELECT * FROM mensagens WHERE usuario = $user AND lida = 0 AND lado = 1");
            if($query->num_rows() > 0)
                $this->session->set_flashdata('mensagem', $query->num_rows());
            else
                $this->session->set_flashdata('mensagem', "Nada");
        }
    }

    # VERIFICA SE O USUÁRIO ESTÁ LOGADO
    function logged() {
        $logged = $this->session->userdata('logado');
        $this->verificaMensagens();

        if (!isset($logged) || $logged != true) {
            redirect(base_url());
            die();
        }
    }
}