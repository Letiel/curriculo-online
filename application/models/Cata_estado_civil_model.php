<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cata_estado_civil_model extends CI_Model{
	public function get_estado_civil(){		
		$this->db->order_by('id', 'asc');
		return $this->db->get('estado_civil');
	}
}