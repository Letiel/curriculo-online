<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Cadastro_empresa_model extends CI_Model{
		public function cadastrar($dados = NULL){
			if($dados != null){
				$this->db->insert("empresas", $dados);
				$message = 'Ol&aacute;  ' . $dados['nome'] . ",<br/><br/>
						Para confirmar seu cadastro, por favor, clique no link abaixo:<br/>
						<a href='".base_url()."Confirma/empresa/$dados[codigo]'>Clique aqui para confirmar seu cadastro</a><br/>
						Para não receber mais nossos e-mails, <a href='".base_url()."Empresa/cancela_email/$dados[codigo]'>clique aqui</a>
						<br/><br/>
						Se n&atilde;o tiver sido voc&ecirc; ou n&atilde;o desejar confirmar o cadastro, apenas desconsidere esta mensagem.<br/><br/>
						Muito obrigado!";

            	$subject = 'Confirmação de Cadastro';
				$this->load->model('Imail', 'mail');
				$this->mail->enviar($subject,  $dados['email'], $message);
				$this->session->set_flashdata('confirmado', "<div class='alert alert-info'>Uma mensagem de confirmação foi enviado para seu e-mail!<br />Verifique e clique no link informado para confirmar seu cadastro!</div>");
				redirect(base_url().'Confirma/email');
			}
		}

		public function get_by_id(){
			//$this->db->where('id', $this->session->userdata('idEmpresa'));
			//return $this->db->get('empresas');
			$id = $this->session->userdata('idEmpresa');
			return $this->db->query("SELECT empresas.id, empresas.nome, empresas.cnpj, empresas.email, empresas.login, empresas.imagem, 
				empresas.endereco, empresas.bairro, empresas.cep, empresas.site as site, cidade.nome as cidade, estado.nome as estado, estado.uf as uf, cidade.id as idCidade, 
				estado.id as idEstado, empresas.textempresa, empresas.confirmado, empresas.bloqueado FROM empresas 
				LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) WHERE empresas.id = $id");
		}

		public function atualiza_foto($string){
			$this->db->where('id', $this->session->userdata('idEmpresa'));
			$id = $this->session->userdata('idEmpresa');
			//$this->db->update('empresas', $string);
			$this->db->query("UPDATE empresas SET imagem = '$string' WHERE id = $id");
		}

		public function update($dados = NULL){
			$id = $this->session->userdata('idEmpresa');
			$this->db->where('id', $id);
			$this->db->update('empresas', $dados);
		}

		public function ver_perfil($id = NULL){
			return $this->db->query("SELECT empresas.id, empresas.recebe_email, empresas.codigo, empresas.nome, empresas.cnpj, empresas.email, empresas.login, empresas.imagem, 
				empresas.endereco, empresas.bairro, cidade.nome as cidade, estado.nome as estado, estado.uf as uf, cidade.id as idCidade, 
				estado.id as idEstado, empresas.textempresa, empresas.confirmado, empresas.site as site, empresas.bloqueado, empresas.complemento, empresas.cep, empresas.pre_cadastrado, empresas.recebe_email FROM empresas 
				LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) WHERE empresas.id = $id");
		}

		public function confirmaEmail($dados = NULL){
			if($dados != NULL){
				$codigo = $dados['codigo'];
				$codigo = addslashes($codigo);
				$this->db->query("UPDATE empresas SET confirmado = 1 WHERE codigo = '$codigo'");
			}
		}

		public function get_mensagens($maximo, $inicio){
			$user = $this->session->userdata('idEmpresa');
			return $this->db->query("SELECT usuarios.id as idUsuario, mensagens.id as id_mensagem, mensagens.mensagem, mensagens.data, mensagens.lida, 
				mensagens.lado, mensagens.respondida, usuarios.nome as usuario, empresas.nome as empresa FROM mensagens JOIN usuarios 
				ON (usuarios.id = mensagens.usuario) JOIN empresas ON (empresas.id = mensagens.empresa) 
				WHERE mensagens.empresa = $user ORDER BY mensagens.id DESC LIMIT $inicio, $maximo");
		}

		public function get_mensagens_all(){
			$user = $this->session->userdata('idEmpresa');
			return $this->db->query("SELECT usuarios.id as idUsuario, mensagens.id as id_mensagem, mensagens.mensagem, mensagens.data, mensagens.lida, mensagens.lado, mensagens.respondida, usuarios.nome as usuario, empresas.nome as empresa FROM mensagens JOIN usuarios ON (usuarios.id = mensagens.usuario) JOIN empresas ON (empresas.id = mensagens.empresa) WHERE mensagens.empresa = $user ORDER BY mensagens.id DESC");
		}

		public function troca_senha($dados){
			$this->db->where('id', $this->session->userdata('idEmpresa'));
			$this->db->update('empresas', $dados);
		}

		public function alterar_login($dados) {
			$this->db->where('id', $this->session->userdata('idEmpresa'));
			$this->db->update('empresas', $dados);
		}

		public function confirma_senha(){
			$id = $this->session->userdata('idEmpresa');
			return $this->db->query("SELECT * FROM empresas WHERE id = $id");
		}

		public function confirmaEmail_empresa($dados = NULL){
			if($dados != NULL){
				$codigo = $dados['codigo'];
				$codigo = addslashes($codigo);
				$this->db->query("UPDATE empresas SET confirmado = 1 WHERE codigo = '$codigo'");
			}
		}

		public function envia_curriculo($usuario = NULL, $empresa = NULL){
			if($usuario != NULL && $empresa != NULL){
				$data = date('Y-m-d');
				if($this->db->query("SELECT * FROM envio_curriculo WHERE usuario = $usuario AND empresa = $empresa AND data = '$data'")->num_rows() == 0){
					$this->db->query("INSERT INTO envio_curriculo (usuario, empresa, data) VALUES ($usuario, $empresa, '$data')");
					return true;
				}else{
					return false;
				}
			}
		}

		public function cadastra_pre($dados = NULL){
			if($dados != NULL){
				if($this->db->query("UPDATE empresas SET login = '$dados[login]', senha = '$dados[senha]', pre_cadastrado = 0, confirmado = 1 WHERE codigo = '$dados[codigo]'"))
					return true;
				else
					return false;
			}
		}

		public function cancela_email($dados = NULL){
			if($dados != NULL){
				$this->db->query("UPDATE empresas SET recebe_email = 0 WHERE codigo = '$dados[codigo]'");
			}
		}

		public function curriculos_enviados_all($vaga){
			$id_empresa = $this->session->userdata("idEmpresa");
			// $this->db->select("usuarios.nome, cidade.nome as cidade, estado.uf, usuario_vaga.data, vagas.cargo, vagas.id as id_vaga");
			// $this->db->join("vagas", "vagas.id = usuario_vaga.vaga");
			// $this->db->join("empresas", "empresas.id = vagas.empresa");
			// $this->db->join("usuarios", "usuarios.id = usuario_vaga.usuario");
			// $this->db->join("cidade", "usuarios.cidade = cidade.id");
			// $this->db->join("estado", "estado.id = cidade.estado");
			// $this->db->where("vagas.id", $vaga);
			// $this->db->where("empresas.id", $id_empresa);
			// return $this->db->get("usuario_vaga");
			return $this->db->query(
				"SELECT usuarios.nome as nome_usuario, usuarios.sobrenome as sobrenome_usuario FROM usuario_vaga 
					RIGHT JOIN usuarios ON (usuarios.id = usuario_vaga.usuario) 
					JOIN vagas ON (vagas.id = usuario_vaga.vaga) 
					JOIN empresas ON (empresas.id = vagas.empresa) 
					JOIN cidade ON (cidade.id = usuarios.cidade) 
					JOIN estado ON (estado.id = cidade.estado) 
				WHERE vagas.id = $vaga AND empresas.id = $id_empresa"
			);
			// return $this->db->query("SELECT usuarios.nome, usuarios.id as id_usuario, empresas.nome as nome_empresa, vagas.descricao as descricao_vaga, areas.descricao as area, usuario_vaga.data FROM usuario_vaga JOIN usuarios ON (usuarios.id = usuario_vaga.usuario) JOIN vagas ON (vagas.id = usuario_vaga.vaga) JOIN empresas ON (vagas.empresa = empresas.id) JOIN areas ON (areas.id = vagas.area) WHERE empresas.id = $id_empresa");
			// return $this->db->query("SELECT enviado.id, enviado.data, usuarios.nome as nome_usuario, usuarios.foto, usuarios.sobrenome as sobrenome_usuario, cidade.nome as cidade, estado.uf FROM envio_curriculo as enviado JOIN usuarios ON (usuarios.id = enviado.usuario) JOIN empresas ON (empresas.id = enviado.empresa) JOIN cidade ON (cidade.id = usuarios.cidade) JOIN estado ON (estado.id = cidade.estado) WHERE empresas.id = $id_empresa ORDER BY data DESC");
		}

		public function curriculos_enviados($maximo, $inicio, $vaga){
			$id_empresa = $this->session->userdata("idEmpresa");
			return $this->db->query(
				"SELECT usuarios.nome as nome_usuario, usuarios.sobrenome as sobrenome_usuario, usuario_vaga.data, usuarios.id as id, usuarios.foto as foto, cidade.nome as cidade, estado.uf, vagas.cargo FROM usuario_vaga 
					JOIN usuarios ON (usuarios.id = usuario_vaga.usuario) 
					JOIN vagas ON (vagas.id = usuario_vaga.vaga) 
					JOIN empresas ON (empresas.id = vagas.empresa) 
					JOIN cidade ON (cidade.id = usuarios.cidade) 
					JOIN estado ON (estado.id = cidade.estado) 
				WHERE vagas.id = $vaga AND empresas.id = $id_empresa
				LIMIT $inicio, $maximo"
			);
			// return $this->db->query("SELECT usuarios.id, usuarios.nome as nome_usuario, usuarios.sobrenome as sobrenome_usuario, usuarios.foto, empresas.nome as nome_empresa, vagas.descricao as descricao_vaga, areas.descricao as area, usuario_vaga.data, cidade.nome as cidade, estado.uf FROM usuario_vaga JOIN usuarios ON (usuarios.id = usuario_vaga.usuario) JOIN vagas ON (vagas.id = usuario_vaga.vaga) JOIN empresas ON (vagas.empresa = empresas.id) JOIN areas ON (areas.id = vagas.area) JOIN cidade ON (cidade.id = usuarios.cidade) JOIN estado ON (estado.id = cidade.estado) WHERE empresas.id = $id_empresa LIMIT $inicio, $maximo");
			// return $this->db->query("SELECT enviado.usuario as id, enviado.data, usuarios.nome as nome_usuario, usuarios.foto, usuarios.sobrenome as sobrenome_usuario, cidade.nome as cidade, estado.uf FROM envio_curriculo as enviado JOIN usuarios ON (usuarios.id = enviado.usuario) JOIN empresas ON (empresas.id = enviado.empresa) JOIN cidade ON (cidade.id = usuarios.cidade) JOIN estado ON (estado.id = cidade.estado) WHERE empresas.id = $id_empresa ORDER BY data DESC LIMIT $inicio, $maximo");
		}

		public function deleta_conta(){
			$id = $this->session->userdata('idEmpresa');
			$this->db->query("DELETE FROM empresas WHERE id = $id");
			$this->session->set_flashdata("confirmado", "<div class='alert alert-success'><h3>Conta excluída com sucesso!</h3></div>");
		}

		public function update_dado_especifico($dados = NULL){
			if($dados != NULL){
				$id = $this->session->userdata("idEmpresa");
				if($dados['value'] == "cidade")
					$this->db->query("UPDATE empresas SET $dados[campo] = $dados[value] WHERE id = $id");
				else
					$this->db->query("UPDATE empresas SET $dados[campo] = '$dados[value]' WHERE id = $id");
				echo "Alterado com sucesso!";
			}
		}

		function ver_ultimas_vagas($id) {
			$this->db->where("desativado", "0");
			$this->db->where("empresa", $id);
			return $this->db->get("vagas");
		}
	}