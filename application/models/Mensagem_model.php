<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');
	class Mensagem_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function get_dados_usuario($usuario){
			$this->db->where('id', $usuario);
			return $this->db->get('usuarios');
		}

		function get_dados_empresa($empresa){
			$this->db->where('id', $empresa);
			return $this->db->get('empresas');
		}

		public function mensagem_empresa($dados = NULL){
			if($dados != NULL){
				$data = date("Y-m-d");
				$empresa = $this->session->userdata('idEmpresa');
				$usuario = $dados['usuario'];
				$mensagem = $dados['mensagem'];
				$this->db->query("INSERT INTO mensagens (empresa, usuario, mensagem, data, lado) VALUES ('$empresa', '$usuario', '$mensagem', '$data', 1)");

				$usuario = $this->get_dados_usuario($dados['usuario'])->first_row();
				$email = $usuario->email;
				// $email = $this->session->userdata('email');
				$message = "Você recebeu uma nova mensagem em <a href='".base_url()."'>Currículo Online</a>.<br />Faça login no sistema para visualizar a mensagem.<br/ >Caso o link acima não funcione, copie e cole o código abaixo no seu navegador. <br />".base_url()."";
            	$subject = "Você recebeu uma nova mensagem! - Currículo Online";
				$this->load->model('Imail', 'mail');
				$this->mail->enviar($subject,  $email, $message);

				$this->session->set_flashdata('mensagem_enviada', "<div class='alert alert-success'>Mensagem enviada com sucesso!</div>");
			}
		}

		public function mensagem_usuario($dados = NULL){
			if($dados != NULL){
				$data = date("Y-m-d");
				$usuario = $this->session->userdata('id');
				$empresa = $dados['empresa'];
				$mensagem = $dados['mensagem'];
				$id_mensagem = $dados['id_mensagem'];
				$this->db->query("INSERT INTO mensagens (empresa, usuario, mensagem, data, lado) VALUES ('$empresa', '$usuario', '$mensagem', '$data', 0)");
				$this->db->query("UPDATE mensagens SET respondida = 1 WHERE id = $id_mensagem");
				
				$empresa = $this->get_dados_empresa($dados['empresa'])->first_row();
				$email = $empresa->email;
				// $email = $this->session->userdata('email');
				$message = "Você recebeu uma nova mensagem em <a href='".base_url()."'>Currículo Online</a>.<br />Faça login no sistema para visualizar a mensagem.<br/ >Caso o link acima não funcione, copie e cole o código abaixo no seu navegador. <br />".base_url()."";
            	$subject = "Você recebeu uma nova mensagem! - ".base_url();
				$this->load->model('Imail', 'mail');
				$this->mail->enviar($subject,  $email, $message);

				$this->session->set_flashdata('mensagem_enviada', "<div class='alert alert-success'>Mensagem enviada com sucesso!</div>");
			}
		}

		public function ler($dados = NULL){
			if($dados != NULL){
				$id = $dados['mensagem'];
				$this->db->query("UPDATE mensagens SET lida = 1 WHERE id = $id");
			}
		}
	}