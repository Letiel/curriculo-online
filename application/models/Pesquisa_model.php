<?php
	// ini_set("display_errors", 0);
	class Pesquisa_model extends CI_Model{
		function pesquisa_filtrada($pesquisa = NULL, $tipo = 'vagas', $cidade = NULL, $inicio = NULL, $maximo = NULL){
			if($tipo == null)
				$tipo = 'vagas';
			$this->db->start_cache();
			$pesquisa = ($pesquisa != NULL ? str_replace(" ", "%", $pesquisa) : NULL);
			switch($tipo){
				case 'usuarios':
					$this->db->order_by("usuarios.confirmado", "desc");
					$this->db->order_by("usuarios.id", "desc");
					if($pesquisa != NULL){
						$this->db->where('CONCAT(usuarios.nome, usuarios.sobrenome) LIKE ', "%".$pesquisa."%");
						$this->db->or_where("usuarios.id IN (SELECT usuario FROM usuario_area JOIN areas ON (areas.id = usuario_area.area) WHERE areas.descricao LIKE '%".$pesquisa."%')");
						$this->db->or_where("usuarios.id IN (SELECT usuario FROM servicos_usuarios WHERE servicos_usuarios.descricao_servico LIKE '%".$pesquisa."%' OR servicos_usuarios.titulo_servico LIKE '%".$pesquisa."%' OR servicos_usuarios.contato LIKE '%".$pesquisa."%')");
					}
					if($cidade != NULL)
						$this->db->where('cidade.id', $cidade);
					$this->db->where('usuarios.desativado', 0);
					$this->db->where('usuarios.bloqueado', 0);
					$this->db->join('cidade', "cidade.id = usuarios.cidade");
					$this->db->join('estado', "estado.id = cidade.estado");
					$this->db->select("usuarios.*, cidade.nome as nome_cidade, cidade.id as id_cidade, estado.uf");
					break;
				case 'vagas':
					$this->db->order_by("vagas.id", "desc");
					if($pesquisa != NULL){
						$this->db->or_where('vagas_especiais.titulo LIKE ', "%".$pesquisa."%");
						$this->db->or_where('vagas_especiais.descricao LIKE ', "%".$pesquisa."%");
						$this->db->or_where('vagas.cargo LIKE ', "%".$pesquisa."%");
						$this->db->or_where('vagas.descricao LIKE ', "%".$pesquisa."%");
						$this->db->or_where('areas.descricao LIKE ', "%".$pesquisa."%");
					}
					if($cidade != NULL)
						$this->db->where('cidade.id', $cidade);
					$this->db->join("areas", "areas.id = vagas.area");
					$this->db->join("empresas", "empresas.id = vagas.empresa");
					$this->db->join('vagas_especiais', "vagas_especiais.vaga_id = vagas.id", "left");
					$this->db->join('cidade', "cidade.id = vagas_especiais.cidade", "left");
					$this->db->join('estado', "estado.id = cidade.estado", 'left');

					$this->db->join('cidade as cidade_empresa', "cidade_empresa.id = empresas.cidade", "left");
					$this->db->join('estado as estado_empresa', "estado_empresa.id = cidade_empresa.estado", 'left');

					$this->db->select("empresas.nome as nome_empresa, empresas.imagem, cidade_empresa.nome as cidade, estado_empresa.uf as uf, vagas.*, vagas_especiais.titulo as titulo_especial, vagas_especiais.descricao as descricao_especial, vagas_especiais.nome_empresa as empresa_especial, vagas_especiais.imagem as imagem_especial, cidade.nome as cidade_especial_nome, estado.uf as uf_estado_especial");
					// echo $this->db->get_compiled_select($tipo);
					// exit();
				break;
				case 'empresas':
					$this->db->order_by("empresas.id", "desc");
					if($pesquisa != NULL){
						$this->db->or_like('empresas.nome', $pesquisa);
						$this->db->or_like('empresas.textempresa', $pesquisa);
					}
					if($cidade != NULL)
						$this->db->where('cidade.id', $cidade);
					$this->db->join('cidade', "cidade.id = empresas.cidade", "left");
					$this->db->join('estado', "estado.id = cidade.estado");
					$this->db->select("empresas.*, cidade.nome as nome_cidade, estado.uf");
					break;
			}
			$this->db->limit($maximo, $inicio);
			$query = $this->db->get($tipo);
			$this->db->stop_cache();	
			$this->db->flush_cache();
			return $query;
		}

		function retorna_vagas($pesquisa, $maximo, $inicio){
			$novo = array("");
			$novo2 = array("");
			$array = explode(" ", $pesquisa);
			$palavras = sizeof($array);

			for($i = 0; $i < $palavras; $i++){
			   for($j = 0; $j < $palavras - $i; $j++){
			      $novo[$i] .= $array[$j]." ";
			   }
			   for($k = $i; $k < $palavras; $k++){
			      $novo2[$i] .= $array[$k]." ";
			   }
			}
			$pesquisa = array_merge($novo, $novo2);
			foreach($pesquisa as $p){
				$p = trim($p); //retirando os espa�os a mais da string, colocados acima
				$this->db->or_like('vagas.cargo', "$p");
				$this->db->or_like('vagas.descricao', "$p");

				$nome_empresa = $this->db->query("SELECT id FROM empresas WHERE nome LIKE '$p'");
				if($nome_empresa->num_rows()>0){
					$nome_empresa = $nome_empresa->result();
					$nome_empresa = $nome_empresa[0];
					$this->db->where('vagas.empresa', $nome_empresa->id);
				}

				$cidade = $this->db->query("SELECT id FROM cidade WHERE nome LIKE '$p'");
				if($cidade->num_rows()>0){
					$cidade = $cidade->result();
					$cidade = $cidade[0];
					$this->db->where('cidade', $cidade->id);
				}

				$estado = $this->db->query("SELECT id FROM estado WHERE uf LIKE '$p'");
				if($estado->num_rows()>0){
					$estado = $estado->result();
					$estado = $estado[0];
					$this->db->where('estado.id', $estado->id);
				}
			}
			$this->db->join('empresas', 'empresas.id = vagas.empresa');
			$this->db->join('areas', 'areas.id = vagas.area');
			$this->db->join('cidade', 'cidade.id = empresas.cidade');
			$this->db->join('estado', 'estado.id = cidade.estado');
			$this->db->select('vagas.id, , empresas.imagem, vagas.cargo, vagas.descricao, empresas.nome as empresa, areas.descricao as area, cidade.nome as cidade, estado.uf as uf');
			$this->db->from('vagas');
			$this->db->where('vagas.bloqueado', 0);
			$this->db->where('vagas.desativado', 0);
			$this->db->limit($maximo, $inicio);
			return $this->db->get();
		}

		function retorna_vagas_all($pesquisa){
			$novo = array("");
			$novo2 = array("");
			$array = explode(" ", $pesquisa);
			$palavras = sizeof($array);

			for($i = 0; $i < $palavras; $i++){
			   for($j = 0; $j < $palavras - $i; $j++){
			      $novo[$i] .= $array[$j]." ";
			   }
			   for($k = $i; $k < $palavras; $k++){
			      $novo2[$i] .= $array[$k]." ";
			   }
			}
			$pesquisa = array_merge($novo, $novo2);
			foreach($pesquisa as $p){
				$p = trim($p); //retirando os espa�os a mais da string, colocados acima
				$this->db->or_like('vagas.cargo', "$p");
				$this->db->or_like('vagas.descricao', "$p");

				$nome_empresa = $this->db->query("SELECT id FROM empresas WHERE nome LIKE '%$p%'");
				if($nome_empresa->num_rows()>0){
					$nome_empresa = $nome_empresa->result();
					$nome_empresa = $nome_empresa[0];
					$this->db->where('vagas.empresa', $nome_empresa->id);
				}

				$cidade = $this->db->query("SELECT id FROM cidade WHERE nome LIKE '$p'");
				if($cidade->num_rows()>0){
					$cidade = $cidade->result();
					$cidade = $cidade[0];
					$this->db->where('cidade', $cidade->id);
				}

				$estado = $this->db->query("SELECT id FROM estado WHERE uf LIKE '$p'");
				if($estado->num_rows()>0){
					$estado = $estado->result();
					$estado = $estado[0];
					$this->db->where('estado.id', $estado->id);
				}
			}
			$this->db->join('empresas', 'empresas.id = vagas.empresa');
			$this->db->join('areas', 'areas.id = vagas.area');
			$this->db->join('cidade', 'cidade.id = empresas.cidade');
			$this->db->join('estado', 'estado.id = cidade.estado');
			$this->db->select('vagas.id, , empresas.imagem, vagas.cargo, vagas.descricao, empresas.nome as empresa, areas.descricao as area, cidade.nome as cidade, estado.uf as uf');
			$this->db->from('vagas');
			$this->db->where('vagas.bloqueado', 0);
			$this->db->where('vagas.desativado', 0);
			return $this->db->get();
		}

		function retorna_empresas($pesquisa){
			$novo = array("");
			$novo2 = array("");
			$array = explode(" ", $pesquisa);
			$palavras = sizeof($array);

			for($i = 0; $i < $palavras; $i++){
			   for($j = 0; $j < $palavras - $i; $j++){
			      $novo[$i] .= $array[$j]." ";
			   }
			   for($k = $i; $k < $palavras; $k++){
			      $novo2[$i] .= $array[$k]." ";
			   }
			}
			$pesquisa = array_merge($novo, $novo2);
			$this->db->join('cidade', 'cidade.id = empresas.cidade');
			$this->db->join('estado', 'estado.id = cidade.estado');
			$this->db->select('empresas.id, empresas.nome, empresas.imagem, cidade.nome as cidade, estado.uf as uf');
			$this->db->from('empresas');
			$this->db->where('empresas.bloqueado', 0);
			$this->db->where('empresas.confirmado', 1);
			foreach($pesquisa as $p){
				$p = trim($p); //retirando os espa�os a mais da string, colocados acima
				$nome_empresa = $this->db->query("SELECT * FROM empresas WHERE nome LIKE '%$p%'");
				if($nome_empresa->num_rows()>0){
					$nome_empresa = $nome_empresa->result();
					$nome_empresa = $nome_empresa[0];
					$this->db->like("empresas.nome", $p);
				}

				$cidade = $this->db->query("SELECT id FROM cidade WHERE nome LIKE '$p'");
				if($cidade->num_rows()>0){
					$cidade = $cidade->result();
					$cidade = $cidade[0];
					$this->db->where('cidade', $cidade->id);
				}

				$estado = $this->db->query("SELECT id FROM estado WHERE uf LIKE '$p'");
				if($estado->num_rows()>0){
					$estado = $estado->result();
					$estado = $estado[0];
					$this->db->where('estado.id', $estado->id);
				}
			}
			return $this->db->get();
		}

		function retorna_curriculos_all($pesquisa){

			$pesquisa = str_replace(",", "", $pesquisa);

			$novo = array("");
			$novo2 = array("");
			$array = explode(" ", $pesquisa);
			$palavras = sizeof($array);

			for($i = 0; $i < $palavras; $i++){
			   for($j = 0; $j < $palavras - $i; $j++){
			      $novo[$i] .= $array[$j]." ";
			   }
			   for($k = $i; $k < $palavras; $k++){
			      $novo2[$i] .= $array[$k]." ";
			   }
			}
			$pesquisa = array_merge($novo, $novo2);
			foreach($pesquisa as $p){
				$p = trim($p); //retirando os espa�os a mais da string, colocados acima
				$nome_usuario = $this->db->query("SELECT * FROM usuarios WHERE nome LIKE '%$p%'");
				if($nome_usuario->num_rows()>0){
					$nome_usuario = $nome_usuario->result();
					$nome_usuario = $nome_usuario[0];
					$this->db->like("usuarios.nome", $p);
				}

				$sobrenome_usuario = $this->db->query("SELECT * FROM usuarios WHERE sobrenome LIKE '%$p%'");
				if($sobrenome_usuario->num_rows()>0){
					$sobrenome_usuario = $sobrenome_usuario->result();
					$sobrenome_usuario = $sobrenome_usuario[0];
					$this->db->like("usuarios.sobrenome", $p);
				}

				$cidade = $this->db->query("SELECT id FROM cidade WHERE nome LIKE '$p'");
				if($cidade->num_rows()>0){
					$cidade = $cidade->result();
					$cidade = $cidade[0];
					$this->db->where('cidade', $cidade->id);
				}

				$estado = $this->db->query("SELECT id FROM estado WHERE uf LIKE '$p'");
				if($estado->num_rows()>0){
					$estado = $estado->result();
					$estado = $estado[0];
					$this->db->where('estado.id', $estado->id);
				}
			}
			$this->db->join('cidade', 'cidade.id = usuarios.cidade');
			$this->db->join('estado', 'estado.id = cidade.estado');
			$this->db->select('usuarios.foto, usuarios.id, usuarios.sobrenome, usuarios.nome, cidade.nome as cidade, estado.uf as uf');
			$this->db->from('usuarios');

			//$this->db->where('usuarios.desativado', 0);
			$this->db->where('usuarios.bloqueado', 0);
			return $this->db->get();
		}

		function retorna_curriculos($pesquisa, $maximo, $inicio){
			
			$cidade == false;
			$estado == false;
			$pesquisa = str_replace(",", "", $pesquisa);

			$novo = array("");
			$novo2 = array("");
			$array = explode(" ", $pesquisa);
			$palavras = sizeof($array);

			for($i = 0; $i < $palavras; $i++){
			   for($j = 0; $j < $palavras - $i; $j++){
			      $novo[$i] .= $array[$j]." ";
			   }
			   for($k = $i; $k < $palavras; $k++){
			      $novo2[$i] .= $array[$k]." ";
			   }
			}
			$pesquisa = array_merge($novo2, $novo);

			foreach($pesquisa as $p){
				$p = trim($p); //retirando os espa�os a mais da string, colocados acima
				$nome_usuario = $this->db->query("SELECT * FROM usuarios WHERE nome LIKE '$p'");
				if($nome_usuario->num_rows()>0){
					$nome_usuario = $nome_usuario->result();
					$nome_usuario = $nome_usuario[0];
					$this->db->like("usuarios.nome", $p);
				}

				$sobrenome_usuario = $this->db->query("SELECT * FROM usuarios WHERE sobrenome LIKE '$p%'");
				if($sobrenome_usuario->num_rows()>0){
					$sobrenome_usuario = $sobrenome_usuario->result();
					$sobrenome_usuario = $sobrenome_usuario[0];
					$this->db->like("usuarios.sobrenome", $p);
				}

				if(!$cidade){
					$cidade = $this->db->query("SELECT id FROM cidade WHERE nome LIKE '$p'");
					if($cidade->num_rows()>0){
						$cidade = $cidade->result();
						$cidade = $cidade[0];
						$this->db->where('cidade', $cidade->id);
						$cidade = true;
					}
				}

				if(!$estado){
					$estado = $this->db->query("SELECT id FROM estado WHERE uf LIKE '$p'");
					if($estado->num_rows()>0){
						$estado = $estado->result();
						$estado = $estado[0];
						$this->db->where('estado.id', $estado->id);
						$estado = true;
					}
				}
			}
			$this->db->join('cidade', 'cidade.id = usuarios.cidade');
			$this->db->join('estado', 'estado.id = cidade.estado');
			$this->db->select('usuarios.foto, usuarios.id, usuarios.sobrenome, usuarios.nome, cidade.nome as cidade, estado.uf as uf');
			$this->db->from('usuarios');
			//$this->db->where('usuarios.desativado', 0);
			$this->db->where('usuarios.bloqueado', 0);
			$this->db->order_by("id", "desc"); 
			//$this->db->group_by('cidade.nome');
			$this->db->limit($maximo, $inicio);
			return $this->db->get();
		}
	}