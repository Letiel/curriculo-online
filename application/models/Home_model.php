<?php
	if (!defined('BASEPATH'))
	    exit('No direct script access allowed');

	class Home_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}

		function carrega_ultimos_curriculos(){
			return $this->db->query("SELECT usuarios.id as id, usuarios.nome, usuarios.sobrenome, usuarios.dt_nascimento, usuarios.sexo, usuarios.foto, estado.nome as estado, estado.uf as uf, cidade.nome as cidade FROM usuarios JOIN cidade ON (cidade.id = usuarios.cidade) JOIN estado ON (estado.id = cidade.estado) WHERE confirmado = 1 AND bloqueado = 0 AND desativado = 0 ORDER BY usuarios.id DESC LIMIT 6");
		}

		function carrega_ultimas_vagas(){
			return $this->db->query("SELECT vagas.cargo, vagas.id, empresas.nome as empresa, areas.descricao as area, vagas.descricao as descricao, empresas.imagem, cidade.nome as cidade, estado.uf, vagas_especiais.titulo as titulo_especial, vagas_especiais.nome_empresa, vagas_especiais.imagem as imagem_especial, cidade_especial.nome as cidade_especial_nome, estado_especial.uf as uf_estado_especial, vagas.especial FROM vagas LEFT JOIN empresas ON (empresas.id = vagas.empresa) LEFT JOIN areas ON (areas.id = vagas.area) LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) LEFT JOIN vagas_especiais ON (vagas_especiais.vaga_id = vagas.id) LEFT JOIN cidade as cidade_especial ON (cidade_especial.id = vagas_especiais.cidade) LEFT JOIN estado as estado_especial ON (estado_especial.id = cidade_especial.estado) WHERE vagas.bloqueado = 0 AND vagas.desativado = 0 ORDER BY vagas.id DESC LIMIT 6");
		}

		function carrega_ultimas_empresas(){
			return $this->db->query("SELECT empresas.id, empresas.nome, cidade.nome as cidade, estado.uf, empresas.imagem FROM empresas LEFT JOIN cidade ON (cidade.id = empresas.cidade) LEFT JOIN estado ON (estado.id = cidade.estado) WHERE empresas.bloqueado = 0 AND (empresas.confirmado = 1 OR empresas.pre_cadastrado = 1) ORDER BY empresas.id DESC LIMIT 6");
		}
	}